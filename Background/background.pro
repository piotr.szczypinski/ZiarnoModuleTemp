TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
#CONFIG += c++11

DESTDIR         = ../Executables

SOURCES +=  \
    ../../qmazda/MzGenerator/featureio.cpp \
    background.cpp


INCLUDEPATH += ../../qmazda/SharedImage/

macx{
}
else:unix{
    LIBS += -lpthread
    LIBS += -L$$PWD/../Executables/ -lziarnoint
    LIBS += -L$$PWD/../Executables/ -lziarnolib
}
else:win32{
    LIBS += ../Executables/ziarnolib.lib
    LIBS += ../Executables/ziarnoint.lib
}

unix{
    LIBS += -lopencv_core
    LIBS += -lopencv_imgproc
    LIBS += -lopencv_highgui
    LIBS += -lopencv_legacy
}
else:win32{
    include(../../qmazda/Pri/opencv.pri)
}
include(../../qmazda/Pri/config.pri)
include(../../qmazda/Pri/itk.pri)
#include(../../qmazda/Pri/opencv.pri)
#include(../../qmazda/Pri/tiff.pri)
