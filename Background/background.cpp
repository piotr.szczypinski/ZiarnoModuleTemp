/*
 * main.cpp - program do testowania modułu analizy ziaren jęczmienia
 *
 * Autor:  (2016) Piotr M. Szczypiński, Politechnika Łódzka
 *
 * Opracowano w ramach grantu: NCBR PBS3/A8/38/2015
 * Opracowanie przemysłowej metody automatycznej oceny parametrów
 * technologicznych i klasyfikacji ziarna z zastosowaniem analizy
 * obrazów.
 */

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui_c.h>
#include <opencv2/imgproc/imgproc_c.h>
//#include <unistd.h>
#include "../../qmazda/MzShared/getoption.h"
#include "../../qmazda/MzGenerator/featureio.h"
#include "../ZiarnoLib/lib_ziarno.h"
#include "../ZiarnoLib/int_ziarno.h"

extern void GetBackgroundImages(std::vector<cv::Mat*>* background);

int group = 1;
int tosave = 0;
char* input = NULL;
char* output = NULL;
bool isprinthelp = false;

int scan_parameters_local(int argc, char* argv[])
{
    int argi = 1;
    while(argi < argc)
    {
        GET_STRING_OPTION("-i", "--input", input)
        else GET_STRING_OPTION("-o", "--output", output)
        else GET_INT_OPTION("-g", "--group", group)
        else GET_INT_OPTION("-s", "--to-save", tosave)
        else GET_NOARG_OPTION("/?", "--help", isprinthelp, true)
        else return argi;
        argi++;
    }
    return 0;
}
void printhelp_local(char* name)
{
    //------------------------------------------------------------------------------------------v
    printf("Usage: %s [OPTION]...\n", name);
    printf("Computes image attributes (features) in regions.\n");
    printf("2017 by Piotr M. Szczypinski\n");
    printf("Compilation date and time: %s %s\n", __DATE__, __TIME__);
    printf("Options:\n");
    printf("  -i, --input <file>     Text file with image filenemes.\n");
    printf("  -o, --output <file>    Background image file to save\n");
    printf("  -g, --group <int>      Number of images showing single kernel.\n");
    printf("  -s, --to-save <int>    Number of background to save.\n");
    printf("  /?, --help             Display this help and exit\n\n");
}


int main(int argc, char* argv[])
{
    int ret = scan_parameters_local(argc, argv);
    if(isprinthelp || argc <= 1)
    {
        printhelp_local(argv[0]);
        return 1;
    }
    if(ret != 0)
    {
        if(ret < argc) fprintf(stderr, "Incorrect operand: %s\n", argv[ret]);
        fprintf(stderr, "Try --help for more information.\n");
        return ret;
    }

    if(input == NULL) {fprintf(stderr, "Unspecified list file (-i)\n"); return -3;}
    if(output == NULL) {fprintf(stderr, "Unspecified output file (-o)\n"); return -4;}

    std::vector<std::string> lista = loadFeatureList(input);
    int count = 0;




    Image** itab = new Image*[lista.size()];
    for(unsigned int i = 0; i < lista.size(); i++)
    {
        itab[count] = cvLoadImage(lista[i].c_str(), CV_LOAD_IMAGE_UNCHANGED);
        if(itab[count] == NULL)
        {
            fprintf(stderr, "Cannot open %i: %s\n", i, lista[i].c_str());
        }
        if(itab[count] != NULL) count++;
    }
    int out = SetDiskImages(itab, group, count/group);
    if(out >= 0)
    {
        Image* im = GetBackgroundImage(tosave);
        if(im != NULL)
        {
            cvSaveImage(output, im);
        }
    }
    for(int i = 0; i < count; i++)
    {
        cvReleaseImage(&itab[i]);
    }

/*
    cv::Mat* itab  = new cv::Mat[lista.size()];
    for(unsigned int i = 0; i < lista.size(); i++)
    {
        itab[count] = cv::imread(lista[i].c_str(), CV_LOAD_IMAGE_UNCHANGED);
        if(!itab[count].empty()) count++;
    }
    int out = ComputeBackgroundImages(itab, count/group, group);
    delete[] itab;
    if(out < 0)
    {
        fprintf(stderr, "Computation error\n");
        return -5;
    }

    cv::imwrite(output, BackgroundImages[tosave]);
*/

    if(out < 0)
        printf("Computation error %i\n", -out);
    else
        printf("WearOf = %i\n", out);

    return 0;
}

