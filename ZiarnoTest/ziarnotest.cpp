/*
 * main.cpp - program do testowania modułu analizy ziaren jęczmienia
 *
 * Autor:  (2016) Piotr M. Szczypiński, Politechnika Łódzka
 *
 * Opracowano w ramach grantu: NCBR PBS3/A8/38/2015
 * Opracowanie przemysłowej metody automatycznej oceny parametrów
 * technologicznych i klasyfikacji ziarna z zastosowaniem analizy
 * obrazów.
 */

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>
#include <opencv2/highgui/highgui_c.h>
#include <opencv2/imgproc/imgproc_c.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "../../qmazda/MzShared/getoption.h"
#include "../../qmazda/MzGenerator/featureio.h"
#include "../ZiarnoLib/lib_ziarno.h"
#include "../ZiarnoLib/int_ziarno.h"
#include <time.h>

char* lista = NULL;
char* config = NULL;
char* top_back = NULL;
char* bottom_back = NULL;
char* lista_back = NULL;
char* top_image = NULL;
char* bottom_image = NULL;
char* side_image = NULL;


int priority = 0;
int delay = -1;
int debug_log_level = DEBUG_LOG_ERRORS;
bool isprinthelp = false;
// Okresla jak podzielic obrazek na dwa
//bool horizontal = true;//false;//

//#define DEBUG_LEVEL DEBUG_NONE
//#define DEBUG_LEVEL DEBUG_LOG_ERRORS
//#define DEBUG_LEVEL DEBUG_LOG_INFO
//#define DEBUG_LEVEL (DEBUG_LOG_ERRORS | DEBUG_LOG_WARNINGS | DEBUG_LOG_INFO | DEBUG_DUMP_IMAGES)

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
#include <windows.h>
void delayms(int del)
{
    Sleep(del);
}
#else
#include <unistd.h>
void delayms(int del)
{
    int ds = del/1000;
    int du = 1000*(del%1000);
    if(ds > 0) sleep(ds);
    usleep(du);
}
#endif

int lastResult = 0;


unsigned int classCount[256];
int maxClass = -1;

//-----------------------------------------------------------------------------
// ZASOBNIK OBRAZOW WEJSCIOWYCH
//-----------------------------------------------------------------------------
struct SubImages
{
    IplImage* images[3];
};
std::vector< SubImages > imageTable;


void printResults()
{
    std::cout << "Count =";
    for(int i = 0; i < maxClass+1; i++)
    {
        std::cout << " " << classCount[i];
    }
    std::cout << std::endl;
}


//-----------------------------------------------------------------------------
// FUNKCJE CALLBACK
//-----------------------------------------------------------------------------
void FinishRunImpl(int id, EntryResult *tabResult, int numResult)
{
    if(numResult > 0)
        lastResult = tabResult[0].kernelClass;

    if(id >= 0) // Dotyczy ziarniaka
    {
        std::cout<< "FinishRun: " << id;
        for(int i = 0; i < numResult; i++)
        {
            std::cout << " class = " << tabResult[i].kernelClass;
        }
        std::cout << std::endl;

        if(numResult > 0)
        {
            if((unsigned int)tabResult[0].kernelClass < 256)
            {
                classCount[(unsigned int)tabResult[0].kernelClass]++;
                if(tabResult[0].kernelClass > maxClass)
                    maxClass = (unsigned int)tabResult[0].kernelClass;
            }
        }
    }
}
void DebugImageCallbackImpl(int id, int idInImg, const char* imgName, const Image *debugImg)
{
    static unsigned int indx = 0;
    std::cout<< "DebugImageCallback: OBJ#" << id << " SUB#" << idInImg << "Name:" << imgName << std::endl;
    std::stringstream ss;
    ss << "Dbg" << indx << "_"<< id << "_" << idInImg << "_" << imgName << ".bmp";
    cvSaveImage(ss.str().c_str(), debugImg);
    indx++;
}

void SystemLogCallbackImpl(int id, int idInImg, int logId, const char* message)
{
    std::cout<< "SystemLogCallback: ";
    switch(logId)
    {
    case ID_LOG_INFO: std::cout<< "INFO "; break;
    case ID_LOG_WARNING: std::cout<< "WARNING "; break;
    case ID_LOG_ERROR: std::cout<< "ERROR "; break;
    default: std::cout<< "UNKNOWN ";
    }
    std::cout << " OBJ#" << id << " SUB#" << idInImg << " : " << message << std::endl;
}

//-----------------------------------------------------------------------------
// TEST
//-----------------------------------------------------------------------------
//void loadAndRunMulti(const char* fileName)
//{
//   Image* src = cvLoadImage(fileName, CV_LOAD_IMAGE_COLOR);
//   if(src == NULL)
//   {
//       std::cout<< "Nie mozna zaladowac obrazu." << std::endl;
//       return;
//   }
////Rozdziela obrazek z pliku na dwa. Docelowo obrazki powinny byc oddzielne
//   int splv, splh;
//   int stav, stah;
//   if(horizontal)
//   {
//       stav = src->width/2;
//       stah = 0;
//       splv = src->width/2;
//       splh = src->height;
//   }
//   else
//   {
//       stav = 0;
//       stah = src->height/2;
//       splv = src->width;
//       splh = src->height/2;
//   }
//   cvSetImageROI(src, cvRect(0, 0, splv, splh));
//   Image *upper = cvCreateImage(cvSize(splv, splh), src->depth, src->nChannels);
//   cvCopy(src, upper, NULL);
//   cvResetImageROI(src);
//   cvSetImageROI(src, cvRect(stav, stah, splv, splh));
//   Image *lower = cvCreateImage(cvSize(splv, splh), src->depth, src->nChannels);
//   cvCopy(src, lower, NULL);
//   cvResetImageROI(src);
//   cvReleaseImage(& src);

//   SubImages subs;
//   subs.images[0] = upper;
//   subs.images[1] = lower;
//   imageTable.push_back(subs);
//   RunAsync(imageTable.size()-1, DEBUG_LEVEL, imageTable[imageTable.size()-1].images, 2);
//}



//void loadAndRunSingle(const char* fileName, void* setup)
//{
//   Image* src = cvLoadImage(fileName, CV_LOAD_IMAGE_COLOR);
//   if(src == NULL)
//   {
//       std::cout<< "Nie mozna zaladowac obrazu." << std::endl;
//       return;
//   }

////Rozdziela obrazek z pliku na dwa. Docelowo obrazki powinny byc oddzielne
//   int splv, splh;
//   int stav, stah;
//   if(horizontal)
//   {
//       stav = src->width/2;
//       stah = 0;
//       splv = src->width/2;
//       splh = src->height;
//   }
//   else
//   {
//       stav = 0;
//       stah = src->height/2;
//       splv = src->width;
//       splh = src->height/2;
//   }
//   cvSetImageROI(src, cvRect(0, 0, splv, splh));
//   Image *upper = cvCreateImage(cvSize(splv, splh), src->depth, src->nChannels);
//   cvCopy(src, upper, NULL);
//   cvResetImageROI(src);
//   cvSetImageROI(src, cvRect(stav, stah, splv, splh));
//   Image *lower = cvCreateImage(cvSize(splv, splh), src->depth, src->nChannels);
//   cvCopy(src, lower, NULL);
//   cvResetImageROI(src);
//   cvReleaseImage(& src);

//   std::vector <Image *> subs;
//   subs.push_back(upper);
//   subs.push_back(lower);
//   ZiarnoWorkerInput input;
//   input.debuglevel = DEBUG_LEVEL;
//   input.qmazdaSetup = setup;
//   input.id = 0;
//   input.tabInImg = subs;

//   qmazdaRun(&input);

//   cvReleaseImage(& upper);
//   cvReleaseImage(& lower);
//}



void loadAndRunMulti(const char* file1, const char* file2, const char* file3)
{
    Image* src1 = cvLoadImage(file1, CV_LOAD_IMAGE_COLOR);
    if(src1 == NULL)
    {
        fprintf(stderr, "Abort: Cannot load T: %s\n", file1);
        return;
    }
    Image* src2 = cvLoadImage(file2, CV_LOAD_IMAGE_COLOR);
    if(src2 == NULL)
    {
        cvReleaseImage(&src1);
        fprintf(stderr, "Abort: Cannot load T: %s\n", file1);
        return;
    }
    Image* src3 = cvLoadImage(file3, CV_LOAD_IMAGE_GRAYSCALE);
    if(src3 == NULL)
    {
        fprintf(stderr, "Warning: Cannot load T: %s\n", file1);
    }

    SubImages subs;
    subs.images[0] = src1;
    subs.images[1] = src2;
    subs.images[2] = src3;
    imageTable.push_back(subs);
    RunAsync(imageTable.size()-1, debug_log_level, imageTable[imageTable.size()-1].images, 3);
}


void loadAndRunSingle(const char* file1, const char* file2, const char* file3, void* setup)
{
    Image* src1 = cvLoadImage(file1, CV_LOAD_IMAGE_COLOR);
    if(src1 == NULL)
    {
        fprintf(stderr, "Abort: Cannot load T: %s\n", file1);
        return;
    }
    Image* src2 = cvLoadImage(file2, CV_LOAD_IMAGE_COLOR);
    if(src2 == NULL)
    {   cvReleaseImage(&src1);
        fprintf(stderr, "Abort: Cannot load B: %s\n", file2);
        return;
    }
    Image* src3 = cvLoadImage(file3, CV_LOAD_IMAGE_GRAYSCALE);
    if(src3 == NULL)
    {
        fprintf(stderr, "Warning: Cannot load S: %s\n", file3);
    }
    std::vector <Image *> subs;
    subs.push_back(src1);
    subs.push_back(src2);
    if(src3 != NULL)
        subs.push_back(src3);

    ZiarnoWorkerInput input;
    input.debuglevel = debug_log_level;
    input.qmazdaSetup = setup;
    input.id = 0;
    input.tabInImg = subs;

    qmazdaRun(&input);

    if(src1 != NULL) cvReleaseImage(& src1);
    if(src2 != NULL) cvReleaseImage(& src2);
    if(src3 != NULL) cvReleaseImage(& src3);
}

void mainMulti(void)
{
    if(!Init(config, priority))
    {
        std::cout<< "Warning: Cannot initialize, strainer might work" << std::endl;
    }
    std::ifstream file;
    file.open(lista);
    if (!file.is_open())
    {
        std::cout<< "Abort: Cannot load image list" << std::endl;
        return;
    }
    if (!file.good())
    {
        std::cout<< "Abort: Cannot load image list" << std::endl;
        return;
    }

    int64 ticks = cv::getTickCount();

    do
    {
        std::string image1;
        std::string image2;
        std::string image3;
        std::getline(file, image1);
        std::getline(file, image2);
        std::getline(file, image3);
        loadAndRunMulti(image1.c_str(), image2.c_str(), image3.c_str());
        delayms(delay);
    }
    while(!file.eof());
    End(); //Czeka na zakończenie wątków

    ticks = (cv::getTickCount() - ticks);
    double time = (double) ticks/cv::getTickFrequency();
    std::cout << "Time elapsed = " << time << "s" << std::endl;

    file.close();
    for(unsigned int i = 0; i < imageTable.size(); i++)
    {
        for(int d = 0; d < 3; d++)
            if(imageTable[i].images[d] != NULL)
            {
                cvReleaseImage(& (imageTable[i].images[d]));
                imageTable[i].images[d] = NULL;
            }
    }
    imageTable.clear();
}

void mainSingle(void* qmazdaData)
{
    std::ifstream file;
    file.open(lista);
    if (!file.is_open())
    {
        std::cout<< "Abort: Cannot load image list" << std::endl;
        return;
    }
    if (!file.good())
    {
        std::cout<< "Abort: Cannot load image list" << std::endl;
        return;
    }

    int64 ticks = cv::getTickCount();

    do
    {
        std::string image1;
        std::string image2;
        std::string image3;
        std::getline(file, image1);
        std::getline(file, image2);
        std::getline(file, image3);
        std::cout << image1 << " " << image2 << " " << image3 << std::endl;

        loadAndRunSingle(image1.c_str(), image2.c_str(), image3.c_str(), qmazdaData);
    }
    while(!file.eof());

    ticks = (cv::getTickCount() - ticks);
    double time = (double) ticks/cv::getTickFrequency();
    std::cout << "Time elapsed = " << time << "s" << std::endl;

    file.close();
}

//-----------------------------------------------------------------------------

int scan_parameters_local(int argc, char* argv[])
{
    int argi = 1;
    while(argi < argc)
    {
        GET_STRING_OPTION("-c", "--config", config)
        else GET_STRING_OPTION("-T", "--back-top", top_back)
        else GET_STRING_OPTION("-B", "--back-bottom", bottom_back)
        else GET_STRING_OPTION("-L", "--back-list", lista_back)
        else GET_STRING_OPTION("-t", "--input-top", top_image)
        else GET_STRING_OPTION("-b", "--input-bottom", bottom_image)
        else GET_STRING_OPTION("-s", "--input-side", side_image)
        else GET_STRING_OPTION("-l", "--input-list", lista)
        else GET_INT_OPTION("-d", "--debug-log", debug_log_level)
        else GET_INT_OPTION("-m", "--multi", delay)
        else GET_INT_OPTION("-p", "--thread-pri", priority)
        else GET_NOARG_OPTION("/?", "--help", isprinthelp, true)
        else return argi;
        argi++;
    }
    return 0;
}
void printhelp_local(char* name)
{
    //------------------------------------------------------------------------------------------v
    printf("Usage: %s [OPTION]...\n", name);
    printf("Predicts class of grain.\n");
    printf("2017.12.02 by Piotr M. Szczypinski\n");
    printf("Compilation date and time: %s %s\n", __DATE__, __TIME__);
    printf("Options:\n");
    printf("  -c, --config <file>        Classification rules file.\n");
    printf("  -t, --input-top <file>     Load top camera image from <file>.\n");
    printf("  -b, --input-bottom <file>  Load bottom camera image from <file>.\n");
    printf("  -s, --input-side <file>    Load side camera image from <file>.\n");
    printf("  -l, --input-list <file>    Load text file list of image filenames.\n");
    printf("  -d, --debug-log <int>      Errors +1, Warnings+2, Info +4, Images +8\n");
    printf("  -m, --multi <int>          Set multithreading delay in miliseconds.\n");
    printf("  -p, --thread-pri <int>     Thread priority (WinAPI:SetThreadPriority()).\n");

    printf("  -T, --back-top <file>      Load top camera background from <file>.\n");
    printf("  -B, --back-bottom <file>   Load bottom camera background from <file>.\n");
    printf("  -L, --back-list <file>     Text file with image filenemes.\n");

    printf("  /?, --help                 Display this help and exit\n\n");
}

//-----------------------------------------------------------------------------
// MAIN
//-----------------------------------------------------------------------------
int main(int argc, char* argv[])
{
    int ret = scan_parameters_local(argc, argv);
    if(isprinthelp || argc <= 1)
    {
        printhelp_local(argv[0]);
        return 0;
    }
    if(ret != 0)
    {
        if(ret < argc) fprintf(stderr, "Incorrect operand: %s\n", argv[ret]);
        fprintf(stderr, "Try --help for more information.\n");
        return 0;
    }

    for(unsigned int i = 0; i < 256; i++) classCount[i] = 0;

    setDebugLevelForInitialization(debug_log_level);
    setSystemLogCallback(SystemLogCallbackImpl);
    setFinishRun(FinishRunImpl);
    setDebugImageCallback(DebugImageCallbackImpl);

    if(config == NULL)
    {
        fprintf(stderr, "Abort: Unspecified config file (-c)\n");
    }
    if(top_back != NULL && bottom_back == NULL)
    {
        fprintf(stderr, "Warning: Unspecified background file (-B)\n");
    }
    if(top_back == NULL && bottom_back != NULL)
    {
        fprintf(stderr, "Warning: Unspecified background file (-T)\n");
    }

/*
    cv::Mat top_background;
    cv::Mat bottom_background;
    if(top_back != NULL && bottom_back != NULL)
    {

        std::vector<cv::Mat*> background;
        top_background = cv::imread(top_back, CV_LOAD_IMAGE_COLOR);
        bottom_background = cv::imread(bottom_back, CV_LOAD_IMAGE_COLOR);
        if(!(top_background.empty() || bottom_background.empty()))
        {
            background.push_back(& top_background);
            background.push_back(& bottom_background);
            SetBackgroundImages(& background);
        }
        else {fprintf(stderr, "Warning: Missing background files\n");}
    }
    else {fprintf(stderr, "Warning: Unspecified background files (-T -B)\n");}
*/

    Image* top_background;
    Image* bottom_background;
    if(top_back != NULL && bottom_back != NULL)
    {
        top_background = cvLoadImage(top_back, CV_LOAD_IMAGE_COLOR);
        bottom_background = cvLoadImage(bottom_back, CV_LOAD_IMAGE_COLOR);
        if(!(top_background == NULL || bottom_background == NULL))
        {
            SetBackgroundImage(top_background, 0);
            SetBackgroundImage(bottom_background, 1);
        }
        else {fprintf(stderr, "Warning: Missing background files\n");}
    }
    else if(lista_back != NULL)
    {
        std::vector<std::string> lista = loadFeatureList(lista_back);
        int count = 0;
        Image** itab = new Image*[lista.size()];
        for(unsigned int i = 0; i < lista.size(); i++)
        {
            itab[count] = cvLoadImage(lista[i].c_str(), CV_LOAD_IMAGE_UNCHANGED);
            if(itab[count] == NULL)
            {
                fprintf(stderr, "Cannot open %i: %s\n", i, lista[i].c_str());
            }
            if(itab[count] != NULL) count++;
        }
        int out = SetDiskImages(itab, 3, count/3);
        if(out < 0)
            printf("Background computation error %i\n", -out);
        else
            printf("WearOf = %i\n", out);
    }
    else
    {
        fprintf(stderr, "Warning: Unspecified background files (-T -B -L)\n");
    }


    if(top_image != NULL && bottom_image != NULL)
    {
        void* qmazdaData;
        qmazdaData = qmazdaInit(config);
        if(qmazdaData == NULL)
        {
            fprintf(stderr, "Abort: Cannnot initialize\n");
            return 0;
        }
        int64 ticks = cv::getTickCount();

        loadAndRunSingle(top_image, bottom_image, side_image, qmazdaData);

        ticks = (cv::getTickCount() - ticks);
        double time = (double) ticks/cv::getTickFrequency();
        std::cout << "Time elapsed = " << time << "s" << std::endl;
        qmazdaEnd();
        return lastResult;
    }

    if(lista == NULL)
    {
        fprintf(stderr, "Abort: Unspecified image list file (-l)\n");
        return 0;
    }

    if(delay >= 0)
    {
        mainMulti(); //Obliczenia w wielu watkach
        printResults();
        return 0;
    }
    else
    {
        void* qmazdaData;
        qmazdaData = qmazdaInit(config);
        if(qmazdaData == NULL)
        {
            fprintf(stderr, "Abort: Cannnot initialize\n");
            return 0;
        }
        mainSingle(qmazdaData); //Obliczenia w jednym watku po kolei
        qmazdaEnd();
        printResults();
        return 0;
    }
    return lastResult;
}

