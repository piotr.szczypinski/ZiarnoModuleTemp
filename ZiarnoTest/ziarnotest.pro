TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
#CONFIG += c++11

DESTDIR         = ../Executables

SOURCES +=  \
    ziarnotest.cpp \
    ../../qmazda/MzGenerator/featureio.cpp

HEADERS += \
    ../ZiarnoLib/int_ziarno.h \
    ../ZiarnoLib/lib_ziarno.h


macx{
}
else:unix{
    LIBS += -L$$PWD/../Executables/ -lziarnoint
    LIBS += -L$$PWD/../Executables/ -lziarnolib
    LIBS += -lpthread
#    LIBS += -ldl

}
else:win32{
    LIBS += ../Executables/ziarnolib.lib
    LIBS += ../Executables/ziarnoint.lib
}

unix{
    LIBS += -lopencv_core
    LIBS += -lopencv_imgproc
    LIBS += -lopencv_highgui
    LIBS += -lopencv_imgcodecs
#    LIBS += -lopencv_legacy
}
else:win32{
    include(../../qmazda/Pri/opencv.pri)
}
include(../../qmazda/Pri/config.pri)
# include(../../qmazda/Pri/opencv.pri)
#include(../../qmazda/Pri/tiff.pri)



#else:win32{

##Ustawienia zależne od miejsca zainstalowania OpenCV i wersji biblioteki
#    INCLUDEPATH += C:/Programowanie/usr/include
#    LIBS += vfw32.lib
#    LIBS += comctl32.lib
#    LIBS += setupapi.lib
#    LIBS += ws2_32.lib
#    LIBS += C:/Programowanie/usr/opencv3/lib/ippicvmt.lib
#    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_stitching310.lib
#    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_superres310.lib
#    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_ts310.lib
#    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_video310.lib
#    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_videoio310.lib
#    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_videostab310.lib
#    LIBS += C:/Programowanie/usr/opencv3/lib/zlib.lib
#    LIBS += C:/Programowanie/usr/opencv3/lib/IlmImf.lib
#    LIBS += C:/Programowanie/usr/opencv3/lib/libjasper.lib
#    LIBS += C:/Programowanie/usr/opencv3/lib/libjpeg.lib
#    LIBS += C:/Programowanie/usr/opencv3/lib/libpng.lib
#    LIBS += C:/Programowanie/usr/opencv3/lib/libtiff.lib
#    LIBS += C:/Programowanie/usr/opencv3/lib/libwebp.lib
#    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_calib3d310.lib
#    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_core310.lib
#    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_features2d310.lib
#    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_flann310.lib
#    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_highgui310.lib
#    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_imgcodecs310.lib
#    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_imgproc310.lib
#    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_ml310.lib
#    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_objdetect310.lib
#    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_photo310.lib
#    LIBS += C:/Programowanie/usr/opencv3/lib/opencv_shape310.lib
#}
