#include "segment.h"

#include <opencv2/highgui/highgui_c.h>


#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>
using namespace cv;

int greyLevelThreshold = -1;

Image* segmentBackgroundImagesIn[maxNumberOfBackgrounds] = {NULL, NULL};
Image* segmentBackgroundImagesTh[maxNumberOfBackgrounds] = {NULL, NULL};
int* minmaxBallElement = NULL;
int minmaxBallElementArea = 0;


const unsigned char binmask_median_in = 32;
const unsigned char binmask_median_out = 128;

const unsigned char binmask_roi_in = 128;

const unsigned char binmask_gauss_in = 128;
const unsigned char binmask_gauss_out = 64;

const unsigned char binmask_gradient_in = 64;

const unsigned int minimum_area_of_object = 1024;

//Sigma = 3
/*
const int gradientKernelSize = 20;
const double gradientKernel[20] =
{
    -0.0009631898,
    -0.0023213792,
    -0.0049419677,
    -0.0092563591,
    -0.0151620574,
    -0.0215109786,
    -0.0259868833,
    -0.0258257603,
    -0.0193117407,
    -0.0071863509,
    0.0071863509,
    0.0193117407,
    0.0258257603,
    0.0259868833,
    0.0215109786,
    0.0151620574,
    0.0092563591,
    0.0049419677,
    0.0023213792,
    0.0009631898
};
*/


//Sigma = 2
/*
const int gradientKernelSize = 12;
const double gradientKernel[12] =
{
    -0.006548226,
    -0.018231333,
    -0.0377633146,
    -0.0562265644,
    -0.0550473011,
    -0.0234384768,
    0.0234384768,
    0.0550473011,
    0.0562265644,
    0.0377633146,
    0.018231333,
    0.006548226
};
*/

//Sigma = 1

const int gradientKernelSize = 8;
const double gradientKernel[8] =
{
    -0.0042980182,
    -0.0495591181,
    -0.187979758,
    -0.1569715559,
    0.1569715559,
    0.187979758,
    0.0495591181,
    0.0042980182
};


//Sigma = 0.5
/*
const int gradientKernelSize = 4;
const double gradientKernel[4] =
{
    -0.1077142726,
    -0.6899026278,
    0.6899026278,
    0.1077142726
};
*/


/*
//Sigma 2
const int gaussKernelSize = 12;
const double gaussKernel[12] =
{
0.0045467813
0.0158698259
0.0431386594
0.0913245427
0.1505687161
0.1933340584
0.1933340584
0.1505687161
0.0913245427
0.0431386594
0.0158698259
0.0045467813
};
*/

//Sigma = 3
/*
const int gaussKernelSize = 16;
const double gaussKernel[16] =
{
    0.0058427668,
    0.0127175412,
    0.0247703879,
    0.0431725319,
    0.0673328952,
    0.0939706251,
    0.1173551089,
    0.131146572,
    0.131146572,
    0.1173551089,
    0.0939706251,
    0.0673328952,
    0.0431725319,
    0.0247703879,
    0.0127175412,
    0.0058427668
};
*/
//Sigma = 6
const int gaussKernelSize = 24;
const double gaussKernel[24] =
{
    0.0105936178,
    0.0143795531,
    0.0189837824,
    0.024375659,
    0.0304415142,
    0.0369753591,
    0.0436812255,
    0.050189572,
    0.0560878038,
    0.0609620611,
    0.0644446861,
    0.066259911,
    0.066259911,
    0.0644446861,
    0.0609620611,
    0.0560878038,
    0.050189572,
    0.0436812255,
    0.0369753591,
    0.0304415142,
    0.024375659,
    0.0189837824,
    0.0143795531,
    0.0105936178
};


const int x_index = 0;
const int y_index = 1;
const int width_index = 2;
const char* featureNamesFromContour[featureNamesFromContourCount] =
{
    "x",
    "y",
    "width",
    "height",
    "angle",
    "conarea",
    "eqivdiam",
    "conperim",
    "conround",
    "elangle",
    "elheight",
    "elwidth",
    "elelong",
    "momtheta",
    "momelong",
    "humom1",
    "humom2",
    "humom3",
    "humom4",
    "humom5",
    "humom6",
    "humom7"
};


const char* featureConvexNamesFromContour[featureConvexNamesFromContourCount] =
{
    "chrectwidth",
    "chrectheight",
    "chrectarea",
    "charea",
    "chperim",
    "chminferet",
    "chmaxferet"
};

const char* featureDistanceNamesFromContour[featureDistanceNamesFromContourCount] =
{
    "raddist",
    "raddistsqrd",
    "raddistdev",
    "radbb"
};


int segmentComputeBackgroundImages(Image** imagetab, int cameras, int objects)
{
    if (objects < 1)
        return -1;
    if (cameras < 1)
        return -2;


    int realNumberOfBackgrounds = maxNumberOfBackgrounds;
    if(realNumberOfBackgrounds > cameras) realNumberOfBackgrounds = cameras;

    unsigned char minVal;

// Weryfikacja czy wszystkie obrazy sa takie same
    for(int c = 0; c < realNumberOfBackgrounds; c++)
    {
        int maxX = imagetab[c]->width;
        int maxY = imagetab[c]->height;
        int type = imagetab[c]->depth;
        int chan = imagetab[c]->nChannels;
        int step = imagetab[c]->widthStep;

        for(int i = 1; i < objects; i++)
        {
            if (imagetab[i*cameras+c]->widthStep != step)
                return -101;
            if (imagetab[i*cameras+c]->width != maxX)
                return -102;
            if (imagetab[i*cameras+c]->height != maxY)
                return -103;
            if (imagetab[i*cameras+c]->depth != type)
                return -104;
            if (imagetab[i*cameras+c]->nChannels != chan)
                return -105;
            if (imagetab[i*cameras+c]->dataOrder != 0)
                return -106;
        }

    }

//Obliczanie tla jako minimum z serii obrazów
    for(int c = 0; c < realNumberOfBackgrounds; c++)
    {
        int maxX = imagetab[c]->width;
        int maxY = imagetab[c]->height;
        int type = imagetab[c]->depth;
        int chan = imagetab[c]->nChannels;
        int step = imagetab[c]->widthStep;

        if(segmentBackgroundImagesTh[c] != NULL)
            cvReleaseImage(&segmentBackgroundImagesTh[c]);
        segmentBackgroundImagesIn[c] = cvCreateImage(cvSize(maxX, maxY), type, chan);
        cvSetZero(segmentBackgroundImagesIn[c]);
        for(int y = 0; y < maxY; y++)
        {
            unsigned char* back = ((unsigned char *)segmentBackgroundImagesIn[c]->imageData + y * step);
            for(int x = 0; x < maxX*chan; x++)
            {
                minVal = 255;
                for(int i = 0; i < objects; i++)
                {
                    unsigned char pix = *((unsigned char *)imagetab[i*cameras+c]->imageData + y * step + x);
                    if(minVal > pix)
                        minVal = pix;
                }
                *back = minVal;
                back++;
            }
        }
    }

//Estymacja zuzycia plyty
    unsigned long sumPixelsBad = 0;
    unsigned long sumPixelsCount = 0;

    for(int c = 0; c < realNumberOfBackgrounds; c++)
    {
        int maxX = imagetab[c]->width;
        int maxY = imagetab[c]->height;
        int chan = imagetab[c]->nChannels;
        int step = imagetab[c]->widthStep;

        sumPixelsCount += ((unsigned long)maxX*maxY*chan);

        for(int i = 0; i < objects; i++)
        {
            for(int y = 0; y < maxY; y++)
            {
                unsigned char* ppix = ((unsigned char *)imagetab[i*cameras+c]->imageData + (size_t)y * step);
                unsigned char* back = ((unsigned char *)segmentBackgroundImagesIn[c]->imageData + (size_t)y * step);
                unsigned int maxXchan = maxX*chan;
                for(unsigned int x = 0; x < maxXchan; x++)
                {
                    unsigned int dif = abs(*ppix - *back);
                    dif /= qualityDividerForBackgroundWearof;
                    sumPixelsBad += dif;
                    ppix++;
                    back++;
                }
            }
        }
    }
    segmentInitialize();
    double wareof = sumPixelsBad;
    wareof /= sumPixelsCount;
    wareof = 1.0 - 0.025/(wareof+0.025);
    wareof *= 1000;
    return (int)(wareof);
}

void createBallElement(void)
{
    if(minmaxBallElement != NULL) return;

    int radius_int = medianFilterRadius;
    int diam_int = 2*radius_int+1;
    minmaxBallElement = new int[diam_int*2];
    minmaxBallElementArea = 0;

    for(int y = -radius_int; y <= radius_int; y++)
    {
        minmaxBallElement[2*(y+radius_int)] = -radius_int;
        minmaxBallElement[2*(y+radius_int)+1] = radius_int+1;

        int x;
        for(x = -radius_int; x <= radius_int; x++)
        {
            if(x*x+y*y < (medianFilterRadius+0.5)*(medianFilterRadius+0.5))
            {
                minmaxBallElement[2*(y+radius_int)] = x;
                break;
            }
        }
        for(; x <= radius_int; x++)
        {
            if(x*x+y*y >= (medianFilterRadius+0.5)*(medianFilterRadius+0.5))
            {
                minmaxBallElement[2*(y+radius_int)+1] = x;
                break;
            }
        }
        minmaxBallElementArea += (minmaxBallElement[2*(y+radius_int)+1] - minmaxBallElement[2*(y+radius_int)]);
    }
}

/**
 * @brief segmentInitialize
 * @return
 */
void segmentInitialize(void)
{
    if(greyLevelThreshold < 0) return;
    createBallElement();

    for(int c = 0; c < maxNumberOfBackgrounds; c++)
    {
        if(segmentBackgroundImagesIn[c] != NULL)
        {
            int maxX = segmentBackgroundImagesIn[c]->width;
            int maxY = segmentBackgroundImagesIn[c]->height;
            int chan = segmentBackgroundImagesIn[c]->nChannels;
            int step = segmentBackgroundImagesIn[c]->widthStep;
            int type = segmentBackgroundImagesIn[c]->depth;

            if(segmentBackgroundImagesTh[c] != NULL)
                cvReleaseImage(&segmentBackgroundImagesTh[c]);
            segmentBackgroundImagesTh[c] = cvCreateImage(cvSize(maxX, maxY), type, chan);
            int stept = segmentBackgroundImagesTh[c]->widthStep;
            for(int y = 0; y < maxY; y++)
            {
                unsigned char* back = ((unsigned char *)segmentBackgroundImagesIn[c]->imageData + (size_t)y * step);
                unsigned char* backt = ((unsigned char *)segmentBackgroundImagesTh[c]->imageData + (size_t)y * stept);
                unsigned char* maxBack = back + (size_t)maxX*chan;
                for(; back < maxBack; back++, backt++)
                {
                    *backt = *back + greyLevelThreshold;
                }
            }
        }
        else
        {
            if(segmentBackgroundImagesTh[c] != NULL)
                cvReleaseImage(&segmentBackgroundImagesTh[c]);
            segmentBackgroundImagesTh[c] = NULL;
        }

    }
}

void segmentBinaryMedianFilter(Image* dst, Image* src)
{
    int radius_int = medianFilterRadius;
    int lmaxX = src->width-radius_int;
    int maxX = src->width-2*radius_int;
    int maxY = src->height-radius_int;
    int step = src->widthStep;
    memset(dst->imageData, 0, step*radius_int);
    memset(dst->imageData+step*maxY, 0, step*radius_int);
    for(int yy = radius_int; yy < maxY; yy++)
    {
        unsigned char* ds = ((unsigned char *)dst->imageData + (size_t)yy * step);
        memset(ds, 0, radius_int);
        memset(ds+lmaxX, 0, radius_int);
    }
    for(int yy = radius_int; yy < maxY; yy++)
    {
        unsigned char* sr = ((unsigned char *)src->imageData + (size_t)yy * step + radius_int);
        unsigned char* ds = ((unsigned char *)dst->imageData + (size_t)yy * step + radius_int);
        unsigned char* maxDs = ds + (size_t)maxX;
        for(; ds < maxDs; ds++, sr++)
        {
            int count = 0;
            for(int y = -radius_int; y < radius_int; y++)
            {
                minmaxBallElement[2*(y+radius_int)] = -radius_int;
                minmaxBallElement[2*(y+radius_int)+1] = radius_int+1;
                int maxX = minmaxBallElement[2*(y+radius_int)+1];
                for(int x = minmaxBallElement[2*(y+radius_int)]; x < maxX; x++)
                {
                    if(*(sr + step*y + x) > 0) count++;
                }
            }
            if(count > minmaxBallElementArea/2) *ds = 127;
            else *ds = 0;
        }
    }
}

void segmentBinaryMedianFilterMask(Image* src)
{
    int radius_int = medianFilterRadius;
    int maxX = src->width;
    int maxY = src->height;
    int step = src->widthStep;
    for(int yy = 0; yy < maxY; yy++)
    {
        int miny = yy;
        if(miny > radius_int) miny = radius_int;
        int maxy = maxY-yy-1;
        if(maxy > radius_int) maxy = radius_int;

        for(int xx = 0; xx < maxX; xx++)
        {
            int minx = xx;
            if(minx > radius_int) minx = radius_int;
            int maxx = maxX-xx-1;
            if(maxx > radius_int) maxx = radius_int;

            unsigned char* sr = ((unsigned char *)src->imageData + (size_t)yy * step + xx);

            int count = 0;
            for(int y = -miny; y <= maxy; y++)
            {
                unsigned char* srr = sr + (size_t)y * step;

                for(int x = -minx; x <= maxx; x++)
                {
                    if((*(srr + x)) & binmask_median_in) count++;
                }
            }
            if(count > minmaxBallElementArea/2) *sr |= binmask_median_out;
        }
    }
}


void segmentBinarizeImage(Image* dst, Image* buf, Image* src, Image* backg)
{
    if(backg != NULL)
    {
        int maxX = src->width;
        int maxY = src->height;
        int chan = src->nChannels;
        int step = src->widthStep;

        int bstep = buf->widthStep;
        int bgstep = backg->widthStep;

        int green;
        green = chan > 1 ? 1 : 0;

        for(int y = 0; y < maxY; y++)
        {
            unsigned char* bg = ((unsigned char *)backg->imageData + (size_t)y * bgstep + green);
            unsigned char* sr = ((unsigned char *)src->imageData + (size_t)y * step + green);
            unsigned char* ds = ((unsigned char *)buf->imageData + (size_t)y * bstep);
            unsigned char* maxDs = ds + (size_t)maxX;
            for(; ds < maxDs; ds++, sr+=chan, bg+=chan)
            {
                if(*sr > *bg) *ds = 255;
                else *ds = 0;
            }
        }
    }
    else
    {
        int maxX = src->width;
        int maxY = src->height;
        int chan = src->nChannels;
        int step = src->widthStep;
        int bstep = buf->widthStep;

        int green;
        green = chan > 1 ? 1 : 0;

        for(int y = 0; y < maxY; y++)
        {
            unsigned char* sr = ((unsigned char *)src->imageData + (size_t)y * step + green);
            unsigned char* ds = ((unsigned char *)buf->imageData + (size_t)y * bstep);
            unsigned char* maxDs = ds + (size_t)maxX;
            for(; ds < maxDs; ds++, sr+=chan)
            {
                if(*sr > greyLevelThreshold) *ds = 255;
                else *ds = 0;
            }
        }
    }
    segmentBinaryMedianFilter(dst, buf);
}

void segmentVerticalBlur(Image* dst, Image* src, Image* mask)
{
    int maxX = mask->width;
    int maxY = mask->height-gaussKernelSize;
    int chan = src->nChannels;
    int step = src->widthStep;
    int dstep = dst->widthStep;
    int mstep = mask->widthStep;
    int green;
    green = chan > 1 ? 1 : 0;
    for(int y = 0; y < maxY; y++)
    {
        unsigned char* sr = ((unsigned char *)src->imageData + (size_t)y * step + green);
        unsigned char* ds = ((unsigned char *)dst->imageData + (size_t)y * dstep);
        unsigned char* ms = ((unsigned char *)mask->imageData + (size_t)y * mstep);
        unsigned char* maxMs = ms + (size_t)maxX;
        for(; ms < maxMs; ms++, sr+=chan, ds++)
        {
            int k;
            double v = 0;
            unsigned char* srk = sr;
            unsigned char* dsk = ds;
            unsigned char* msk = ms;
            for(k = 0; k < gaussKernelSize; k++, msk+=mstep, srk+=step, dsk+=dstep)
            {
                if(*msk & binmask_gauss_in)
                {
                    v += (gaussKernel[k]**srk);
                }
                else
                {
                    break;
                }
            }
            if(k >= gaussKernelSize)
            {
                *ms |= binmask_gauss_out;
                *ds = v;
            }
        }
    }
}

double segmentHorizontalGradientsAmount(Image* src, Image* mask)
{
    double value = 0.0;
    unsigned int count = 0;
    int maxX = mask->width;
    int maxY = mask->height;
    int chan = src->nChannels;
    int step = src->widthStep;
    int mstep = mask->widthStep;
    int green;
    green = chan > 1 ? 1 : 0;
    for(int y = 0; y < maxY; y++)
    {
        unsigned char* sr = ((unsigned char *)src->imageData + (size_t)y * step + green);
        unsigned char* ms = ((unsigned char *)mask->imageData + (size_t)y * mstep);
        unsigned char* maxMs = ms + (size_t)maxX - gradientKernelSize;
        for(unsigned int x = 0; ms < maxMs; x++, ms++, sr+=chan)
        {
            int k;
            double v = 0;
            unsigned char* srk = sr;
            unsigned char* msk = ms;
            for(k = 0; k < gradientKernelSize; k++, msk++, srk+=chan)
            {
                if(*msk & binmask_gradient_in)
                {
                    v += (gradientKernel[k]**srk);
                }
                else
                {
                    break;
                }
            }
            if(k >= gradientKernelSize)
            {
                count ++;
                //value += (v*v);
                //value += fabs(v);
                double weight = gaussKernel[gaussKernelSize*x/maxX]
                              * gaussKernel[gaussKernelSize*y/maxY];
                //value += (weight*v*v);
                value += fabs(weight*v);
            }
        }
    }
    return value/count;
}

double segmentAngleFromContour(CvSeq* contour, double boxin[4], double boxout[4], double oldangle)
{
    double angle;
    if(oldangle < -1000)
    {
        CvBox2D box = cvFitEllipse2(contour);
        angle = box.angle*M_PI/180.0;
        if(angle > M_PI)
            angle -= 2.0*M_PI;
    }
    else
    {
        angle = 2.0*M_PI-oldangle;
        if(angle > M_PI)
            angle -= 2.0*M_PI;
    }

    double cs = cos(angle);
    double sn = sin(angle);

    CvMoments ms;
    int imax = contour->total;
    if(imax <= 0) return 0;
    int i = 0;

    CvPoint* ppoint = (CvPoint*) cvGetSeqElem(contour, i);
    if(ppoint == NULL) return 0;
    double x = cs * ppoint->x + sn * ppoint->y;
    double y = cs * ppoint->y - sn * ppoint->x;
    boxin[0] = boxin[2] = ppoint->x;
    boxin[1] = boxin[3] = ppoint->y;
    boxout[0] = boxout[2] = x;
    boxout[1] = boxout[3] = y;
    ppoint->x = round(x);
    ppoint->y = round(y);
    i++;
    ppoint = (CvPoint*) cvGetSeqElem(contour, i);

    while(ppoint != NULL && i < imax)
    {
        x = cs * ppoint->x + sn * ppoint->y;
        y = cs * ppoint->y - sn * ppoint->x;
        if(boxin[0] > ppoint->x) boxin[0] = ppoint->x;
        if(boxin[2] < ppoint->x) boxin[2] = ppoint->x;
        if(boxin[1] > ppoint->y) boxin[1] = ppoint->y;
        if(boxin[3] < ppoint->y) boxin[3] = ppoint->y;
        if(boxout[0] > x) boxout[0] = x;
        if(boxout[2] < x) boxout[2] = x;
        if(boxout[1] > y) boxout[1] = y;
        if(boxout[3] < y) boxout[3] = y;
        ppoint->x = round(x);
        ppoint->y = round(y);
        i++;
        ppoint = (CvPoint*) cvGetSeqElem(contour, i);
    }

    if(oldangle < -1000)
    {
        cvMoments(contour, &ms);
        if(ms.mu03 > 0)
        {
            angle += M_PI;
            if(angle > M_PI)
                angle -= 2.0*M_PI;
            i = 0;
            ppoint = (CvPoint*) cvGetSeqElem(contour, i);
            while(ppoint != NULL && i < imax)
            {
                ppoint->x = -ppoint->x;
                ppoint->y = -ppoint->y;
                i++;
                ppoint = (CvPoint*) cvGetSeqElem(contour, i);
            }
            cs = -cs;
            sn = -sn;
            double bb = -boxout[0];
            boxout[0] = -boxout[2];
            boxout[2] = bb;
            bb = -boxout[1];
            boxout[1] = -boxout[3];
            boxout[3] = bb;
        }
    }
    return angle;
    //http://raphael.candelier.fr/?blog=Image%20Moments
}

void segmentExtractDistanceFeaturesFromContour(CvSeq* contour, double* features, double centerx, double centery)
{
    unsigned int i = 0;
    int imax = contour->total;
    if(imax <= 0) return;
    double area = 0.0;
    double dist = 0.0;
    double dist2 = 0.0;
    for(int i = 0; i < imax; i++)
    {
        unsigned int j = (i+1) % imax;
        CvPoint* ppi = (CvPoint*) cvGetSeqElem(contour, i);
        CvPoint* ppj = (CvPoint*) cvGetSeqElem(contour, j);

        double x1 = ppi->x - centerx;
        double x2 = ppj->x - centerx;
        double y1 = ppi->y - centery;
        double y2 = ppj->y - centery;
        double tarea2_t = (x1*y2 -x2*y1);
        double dist2_t = sqrt(x1*x1+y1*y1);
        dist2_t += sqrt(x2*x2+y2*y2);
        area += tarea2_t;
        dist += tarea2_t*dist2_t;
        dist2 += tarea2_t*dist2_t*dist2_t;
    }
    area /= 2.0;
    dist /= 4.0;
    dist2 /= 8.0;
    dist /= area;
    dist2 /= area;

    features[i] = dist; i++;
    features[i] = dist2; i++;
    features[i] = sqrt(dist2)-dist; i++;
    features[i] = area / sqrt(dist2); i++;
}

void segmentExtractConvexFeaturesFromContour(CvSeq* contour, double* features)
{
    unsigned int i = 0;
    CvMemStorage* hullStorage = cvCreateMemStorage(0);
    CvSeq *hcontour = cvConvexHull2(contour, hullStorage, CV_CLOCKWISE, 1);

    double hullarea = cvContourArea(hcontour);
    double hullperim = cvContourPerimeter(hcontour);

    CvBox2D box = cvMinAreaRect2(hcontour);
    if(box.size.width<=box.size.height)
    {
        features[i] = box.size.width; i++;
        features[i] = box.size.height; i++;
    }
    else
    {
        features[i] = box.size.height; i++;
        features[i] = box.size.width; i++;
    }
    features[i] = box.size.height*box.size.width; i++;

    int imax = hcontour->total;
    if(imax <= 0) return;
    double minfer = -1;
    double maxfer = -1;
    for(int i = 0; i < imax; i++)
    {
        double maxnn = 0;
        unsigned int j = (i+1) % imax;
        CvPoint* ppi = (CvPoint*) cvGetSeqElem(hcontour, i);
        CvPoint* ppj = (CvPoint*) cvGetSeqElem(hcontour, j);

        double xn = ppi->y - ppj->y;
        double yn = ppj->x - ppi->x;
        double nn = sqrt(xn*xn + yn*yn);
        xn /= nn;
        yn /= nn;

        for(int k = 0; k < imax; k++)
        {
            CvPoint* ppk = (CvPoint*) cvGetSeqElem(contour, k);
            nn = xn*(ppk->x - ppi->x) + yn*(ppk->y - ppi->y);
            nn = fabs(nn);
            if(maxnn < nn) maxnn = nn;
        }
        if(minfer < 0 || minfer > maxnn) minfer = maxnn;
        if(maxfer < maxnn) maxfer = maxnn;
    }
    cvReleaseMemStorage(&hullStorage);

    features[i] = hullarea; i++;
    features[i] = hullperim; i++;
    features[i] = minfer; i++;
    features[i] = maxfer; i++;
}


void segmentExtractFeaturesFromContour(CvSeq* contour, double* features)
{
    CvBox2D box = cvFitEllipse2(contour);
    double conarea = cvContourArea(contour);
    double conperim = cvContourPerimeter(contour);
    double conround = (4 * M_PI *conarea)/(conperim*conperim);
    double ellangle = box.angle*M_PI/180.0;
    double ellheight = box.size.height;
    double ellwidth = box.size.width;
    double ellelong;
    if(ellheight < ellwidth)
    {
        ellelong = ellwidth;
        ellwidth = ellheight;
        ellheight = ellelong;
    }
    ellelong = ellheight/ellwidth;

    CvMoments ms;
    CvHuMoments hms;
    cvMoments(contour, &ms);
    cvGetHuMoments(&ms, &hms);
    double momtheta = atan2(ms.mu11, ms.mu20 - ms.mu02)/2.0;
    double elx = ms.mu20 + ms.mu02;
    double ely = sqrt(4.0 * ms.mu11 * ms.mu11 + (ms.mu20 - ms.mu02)*(ms.mu20 - ms.mu02));
    double momelong = (elx+ely)/(elx-ely);
    unsigned int i = 0;

    features[i] = ms.m10/ms.m00; i++;
    features[i] = ms.m01/ms.m00; i++;
    features[i] = 0; i++;
    features[i] = 0; i++;
    features[i] = 0; i++;
    features[i] = conarea; i++;
    features[i] = 2.0*sqrt(conarea/3.1415927); i++;
    features[i] = conperim; i++;
    features[i] = conround; i++;
    features[i] = ellangle; i++;
    features[i] = ellheight; i++;
    features[i] = ellwidth; i++;
    features[i] = ellelong; i++;
    features[i] = momtheta; i++;
    features[i] = momelong; i++;
    features[i] = hms.hu1; i++;
    features[i] = hms.hu2; i++;
    features[i] = hms.hu3; i++;
    features[i] = hms.hu4; i++;
    features[i] = hms.hu5; i++;
    features[i] = hms.hu6; i++;
    features[i] = hms.hu7; //i++;
    //http://www.empix.com/NE%20HELP/functions/glossary/morphometric_param.htm
}


//unsigned int segmentExtractFeaturesFromContourCount(void)
//{
//    return featureNamesFromContourCount;
//}
//const char* segmentExtractFeaturesFromContourNames(unsigned int index)
//{
//    return featureNamesFromContour[index];
//}

void segmentRotateImage(Image* src, Image* dst, double angle, double box[4])
{
    CvMat* mat = cvCreateMat(2, 3, CV_64FC1);
    double cs = cos(angle);
    double sn = sin(angle);
    double ax = (box[2]+box[0])/2.0;
    double ay = (box[3]+box[1])/2.0;
    double bx = cs * ax - sn * ay;
    double by = cs * ay + sn * ax;
    cvSet2D(mat, 0, 0, cvScalar(cs));
    cvSet2D(mat, 0, 1, cvScalar(-sn));
    cvSet2D(mat, 1, 0, cvScalar(sn));
    cvSet2D(mat, 1, 1, cvScalar(cs));
    cvSet2D(mat, 0, 2, cvScalar(round(bx)));
    cvSet2D(mat, 1, 2, cvScalar(round(by)));
    cvGetQuadrangleSubPix(src, dst, mat);
    cvReleaseMat(&mat);
}

double segmentGetMaxAreaContour(CvSeq* contours, CvSeq** contour, unsigned int* concount)
{
    double maxArea = -1;
    *contour = contours;
    CvSeq* cr = contours;
    while(cr != NULL)
    {
        double area = cvContourArea(cr);
        if(area >= minimum_area_of_object) (*concount)++;
        if(area > maxArea)
        {
            *contour = cr;
            maxArea = area;
        }
        cr = cr->h_next;
    }
    return maxArea;
}

void segmentMakeRoi(MR2DType** roi, Image* src)
{
    int begin[MR2DType::Dimensions];
    int end[MR2DType::Dimensions];
    begin[0] = 0;
    begin[1] = 0;
    end[0] = src->width-1;
    end[1] = src->height-1;
    *roi = new MR2DType(begin, end);
    MazdaRoiIterator<MR2DType> iterator(*roi);
    for(int y = 0; y < src->height; y++)
    {
        unsigned char* pm = ((unsigned char *)src->imageData + (size_t)y * src->widthStep);
        unsigned char* max = pm + (size_t)src->width;
        for(; pm < max; pm++)
        {
            if((*pm) && binmask_roi_in)
            {
                iterator.SetPixel();
            }
            ++iterator;
        }
    }
}




double segmentSingleImage(MR2DType** roi, MIType2DRGB** image, Image* src, unsigned int background, double* features, double* angle, int moreFeatures, unsigned int* warning)
{
    Image* maska = NULL;
    Image grain;
    CvMemStorage* storage = NULL;
    CvSeq* contours = NULL;
    CvSeq* contour = NULL;
    double boxin[4];
    double boxout[4];

    Image* bf0 = cvCreateImage(cvSize(src->width, src->height), IPL_DEPTH_8U, 1);
    Image* bf1 = cvCreateImage(cvSize(src->width, src->height), IPL_DEPTH_8U, 1);

    //segmentInitialize();
    segmentBinarizeImage(bf0, bf1, src, segmentBackgroundImagesTh[background]);



    storage = cvCreateMemStorage(0);
    cvFindContours(bf0, storage, &contours, sizeof(CvContour), CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cvPoint(0,0));

    if(contours == NULL)
    {
        cvReleaseMemStorage(&storage);
        cvReleaseImage(&bf1);
        cvReleaseImage(&bf0);
        return -1;
    }

    unsigned int concount = 0;
    double area = segmentGetMaxAreaContour(contours, &contour, &concount);

    if(area < 1 || contour->total < 6)
    {
        cvReleaseMemStorage(&storage);
        cvReleaseImage(&bf1);
        cvReleaseImage(&bf0);
        return -1;
    }

    if(concount > 1)
        (*warning) |= segmentation_warning_multiple;
    if(concount < 1)
        (*warning) |= segmentation_warning_noobject;

    if(features != NULL)
    {
        segmentExtractFeaturesFromContour(contour, features);
        if(moreFeatures)
        {
            segmentExtractConvexFeaturesFromContour(contour, features+featureNamesFromContourCount);
            segmentExtractDistanceFeaturesFromContour(contour, features+featureNamesFromContourCount+featureConvexNamesFromContourCount, features[x_index], features[y_index]);
        }
    }
    double ang;

    if(angle == NULL)
    {
         ang = segmentAngleFromContour(contour, boxin, boxout, -100000);
    }
    else
    {
        if(*angle < -1000)
            ang = segmentAngleFromContour(contour, boxin, boxout, -100000);
        else
            ang = segmentAngleFromContour(contour, boxin, boxout, *angle);
        *angle = ang;
    }

    if(boxin[0] <= 2 || boxin[1] <= 2 || (boxin[2]+4) > src->width || (boxin[3]+4) > src->height)
        (*warning) |= segmentation_warning_outframe;

    maska = cvCreateImage(cvSize(round(boxout[2])-round(boxout[0])+1, round(boxout[3])-round(boxout[1])+1), IPL_DEPTH_8U, 1);
    cvSetZero(maska);

//    cvDrawContours(maska, contour, cvScalar(binmaskin+binmaskout), cvScalar(binmaskin+binmaskout), 0, -1, CV_AA, cvPoint(-round(boxout[0]), -round(boxout[1])));

    cvDrawContours(maska, contour, cvScalar(binmask_median_in), cvScalar(binmask_median_in), 0, -1, CV_AA, cvPoint(-round(boxout[0]), -round(boxout[1])));
    segmentBinaryMedianFilterMask(maska);

    features[2] = maska->width;
    features[3] = maska->height;
    features[4] = ang;

    unsigned int size[MIType2DRGB::Dimensions];
    double spacing[MIType2DRGB::Dimensions];
    size[0] = round(boxout[2])-round(boxout[0])+1;
    size[1] = round(boxout[3])-round(boxout[1])+1;
    spacing[0] = spacing[1] = 1.0;

    *image = new MIType2DRGB(size, spacing);
    cvInitImageHeader( &grain, cvSize(size[0], size[1]), 8, 3, IPL_ORIGIN_BL, 4 );
    grain.widthStep = size[0]*3;
    cvSetData( &grain, (*image)->GetDataPointer(), grain.widthStep);
    segmentRotateImage(src, &grain, ang, boxout);


    Image* bfm = cvCreateImage(cvSize(maska->width, maska->height), IPL_DEPTH_8U, 1);


    //////////////////////
//    cvSetZero(bfm);


    segmentVerticalBlur(bfm, &grain, maska);


    ///////////////////
//    char nnn[128];
//    static int i = 0;
//    i++;
//    sprintf(nnn, "./gauss_test%i.png", i);
//    cvSaveImage(nnn, bfm);
//    sprintf(nnn, "./mask_test%i.png", i);
//    cvSaveImage(nnn, maska);


    double grad = segmentHorizontalGradientsAmount(bfm, maska);
    cvReleaseImage(&bfm);
//    cvSaveImage("./grain_test.png", &grain);
//    cvSaveImage("./maska_test.png", maska);

    segmentMakeRoi(roi, maska);


    cvReleaseImage(&maska);
    cvReleaseMemStorage(&storage);
    cvReleaseImage(&bf1);
    cvReleaseImage(&bf0);

    return grad;
}





















///////////////////////////////////////////////////////////////////
// Nieuzywane funkcje
// Zamienione przez segmentAngleMomentsFromContour()
///////////////////////////////////////////////////////////////////


//double segmentBGOrientationAnalysis(Image* mask)
//{
//    double value = 0.0;
//    unsigned int count = 0;
//    int maxX = mask->width;
//    int maxY = mask->height;
//    int step = mask->widthStep;

//    for(int y = 0; y < maxY; y++)
//    {
//        unsigned int locount = 0;
//        unsigned char* ms = ((unsigned char *)mask->imageData + (size_t)y * step);
//        unsigned char* maxMs = ms + (size_t)maxX;

//        for(; ms < maxMs; ms++)
//        {
//                if(*ms > 0)
//                    locount++;
//        }
//        value += gradientKernel[y*gradientKernelSize/maxY]*locount;
//        count += locount;
//    }
//    return value/count;
//}

//double segmentAngleFromContour(CvSeq* contour)
//{
//    CvBox2D box = cvFitEllipse2(contour);
//    return box.angle*M_PI/180.0;
//}

//void segmentGetBoxes(double boxin[4], double boxout[4], CvPoint* poly, double angle, CvSeq* contour)
//{
//    int imax = contour->total;
//    int i = 0;
//    double sn = cos(angle);
//    double cs = -sin(angle);
//    CvPoint* ppoint = (CvPoint*) cvGetSeqElem(contour, i);
//    if(ppoint == NULL || i >= imax) return;
//    double x = cs * ppoint->x + sn * ppoint->y;
//    double y = cs * ppoint->y - sn * ppoint->x;

//    boxin[0] = boxin[2] = ppoint->x;
//    boxin[1] = boxin[3] = ppoint->y;
//    boxout[0] = boxout[2] = x;
//    boxout[1] = boxout[3] = y;

//    poly[i].x = round(x);
//    poly[i].y = round(y);
//    i++;
//    ppoint = (CvPoint*) cvGetSeqElem(contour, i);

//    while(ppoint != NULL && i < imax)
//    {
//        x = cs * ppoint->x + sn * ppoint->y;
//        y = cs * ppoint->y - sn * ppoint->x;
//        if(boxin[0] > ppoint->x) boxin[0] = ppoint->x;
//        if(boxin[2] < ppoint->x) boxin[2] = ppoint->x;
//        if(boxin[1] > ppoint->y) boxin[1] = ppoint->y;
//        if(boxin[3] < ppoint->y) boxin[3] = ppoint->y;
//        if(boxout[0] > x) boxout[0] = x;
//        if(boxout[2] < x) boxout[2] = x;
//        if(boxout[1] > y) boxout[1] = y;
//        if(boxout[3] < y) boxout[3] = y;

//        poly[i].x = round(x);
//        poly[i].y = round(y);
//        i++;
//        ppoint = (CvPoint*) cvGetSeqElem(contour, i);
//    }
//}


//void segmentBinarizeAndRotateImage(Image** dsti, Image** dstm, Image* buf1, Image* buf2, Image* src, Image* backg)
//{
//    segmentBinarizeImage(buf1, buf2, src, backg);

//    CvMemStorage* storage;
//    CvSeq* contours;
//    storage = cvCreateMemStorage(0);
//    contours = 0;

//    cvFindContours(buf1, storage, &contours, sizeof(CvContour), CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cvPoint(0,0));
//    CvSeq* contour = contours;

//    double maxArea = 0;
//    CvSeq* maxContour = NULL;
//    while(contour != NULL)
//    {
//        double area = cvContourArea(contour);
//        if(area > maxArea)
//        {
//            maxContour = contour;
//            maxArea = area;
//        }
//        contour = contour->h_next;
//    }
//    double box[4];
//    double angle = segmentAngleFromContour(maxContour);
//}


//int segmentSegmentacja(const std::vector<Image *> *input, std::vector<Image *> *output, std::vector<MR2DType *> *outroi, std::vector<TransformacjaZiarna>* transf, int* orientation_dv)
//{
//}
