/*
 * ziarno.h - implementacja metod modułu analizy ziaren jęczmienia
 *
 * Autor:  (2016-2017) Piotr M. Szczypiński, Politechnika Łódzka
 *
 * Opracowano w ramach grantu: NCBR PBS3/A8/38/2015
 * Opracowanie przemysłowej metody automatycznej oceny parametrów
 * technologicznych i klasyfikacji ziarna z zastosowaniem analizy
 * obrazów.
 */

#ifndef ZIARNO_H
#define ZIARNO_H

#include "int_ziarno.h"
#include <string>
#include <vector>

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
#if defined ZIARNOLIB_SHARED_COMPILATION
    #define API_ZIARNOLIB __declspec(dllexport)
#else
    #define API_ZIARNOLIB __declspec(dllimport)
#endif
#else
    #define API_ZIARNOLIB
#endif

extern int defaultDebugLevel;

extern "C"
{
	typedef struct
	{
		int id;
		int debuglevel;
        std::vector <Image *> tabInImg;
        void* qmazdaSetup;
    }ZiarnoWorkerInput;
}

extern API_ZIARNOLIB FinishRun finishRun;
extern API_ZIARNOLIB DebugImageCallback debugImage;
extern API_ZIARNOLIB SystemLogCallback systemLog;

extern API_ZIARNOLIB std::vector <std::string> qmazdaNames(void);
extern API_ZIARNOLIB bool qmazdaGen(ZiarnoWorkerInput* input, double* feature_values);
extern API_ZIARNOLIB void qmazdaRun(ZiarnoWorkerInput *input);
extern API_ZIARNOLIB void *qmazdaInit(const char *configuration_filename);
extern API_ZIARNOLIB void qmazdaEnd(void);
extern API_ZIARNOLIB int initializeBackgrounds(Image** imagetab, int cameras, int objects);

extern API_ZIARNOLIB void GetBackgroundImages(std::vector<cv::Mat*>* background);
extern API_ZIARNOLIB bool SetBackgroundImages(std::vector<cv::Mat*>* background);
extern API_ZIARNOLIB void qmazdaGenerate(const char *configuration_filename, std::vector <Image *>* tabInImg, std::vector <std::string>* featureNamesOut, double** featureValuesOut);

extern API_ZIARNOLIB void qmazdaSetDebugLevel(int debugLevel);

#endif // ZIARNO_H


