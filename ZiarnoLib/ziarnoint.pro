TEMPLATE = lib

CONFIG += staticlib
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
#CONFIG += c++11

DESTDIR         = ../Executables


SOURCES +=  \
    int_ziarno.cpp

HEADERS += \
    int_ziarno.h \
    lib_ziarno.h

# DEFINES += ZIARNOINT_SHARED_COMPILATION
DEFINES += DLL_EXPORTS

macx{
}
else:unix{
    LIBS += -lpthread
    LIBS += -L$$PWD/../Executables/ -lziarnolib
}
else:win32{
        QMAKE_CXXFLAGS -= /MD
        QMAKE_CXXFLAGS -= /MDd
        QMAKE_CXXFLAGS_DEBUG -= /MDd
        QMAKE_CXXFLAGS_RELEASE -= /MD
        QMAKE_CFLAGS -= /MD
        QMAKE_CFLAGS -= /MDd
        QMAKE_CFLAGS_DEBUG -= /MDd
        QMAKE_CFLAGS_RELEASE -= /MD

        QMAKE_CXXFLAGS += /MT /O2
        QMAKE_CXXFLAGS_DEBUG += /MT /O2
        QMAKE_CXXFLAGS_RELEASE += /MT /O2
        QMAKE_CFLAGS += /MT /O2
        QMAKE_CFLAGS_DEBUG += /MT /O2
        QMAKE_CFLAGS_RELEASE += /MT /O2

    DEFINES += _USE_MATH_DEFINES
    INCLUDEPATH += C:/Programowanie/usr/include

    LIBS += ../Executables/ziarnolib.lib

}

include(../../qmazda/Pri/opencv.pri)
