/*
 * int_ziarno.h - interfejs modułu analizy ziaren jęczmienia
 *
 * Autorzy:  (2016) Mariusz Mrzygłód, Politechnika Wrocławska
 *           (2016) Piotr M. Szczypiński, Politechnika Łódzka
 *
 * Opracowano w ramach grantu: NCBR PBS3/A8/38/2015
 * Opracowanie przemysłowej metody automatycznej oceny parametrów
 * technologicznych i klasyfikacji ziarna z zastosowaniem analizy
 * obrazów.
 */


#ifndef INTERFACE_ZIARNO_H
#define INTERFACE_ZIARNO_H

// Dekalracja IplImage
#include <opencv2/imgproc/imgproc.hpp>

#ifndef NULL
    #define NULL 0
#endif

// Poziomy debugowania mozna laczyc sumujac wartosci
#define DEBUG_NONE          0
#define DEBUG_LOG_ERRORS    1
#define DEBUG_LOG_WARNINGS  2
#define DEBUG_LOG_INFO      4
#define DEBUG_DUMP_IMAGES   8
#define DEBUG_SAVE_IMAGES   16 //In generate mode only

// Rodzaj komunikatu slownego, podawany jako trzeci argument funkcji systemLog()
#define ID_LOG_INFO 0
#define ID_LOG_WARNING 1
#define ID_LOG_ERROR 2
#define ID_LOG_ALARM ID_LOG_ERROR

extern int defaultDebugLevel;

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)

#if defined DLL_EXPORTS && defined(_MSC_VER)
	#define API_ZIARNO __declspec(dllexport)
#else
    #define API_ZIARNO __declspec(dllexport)
#endif
#else
	#define API_ZIARNO 
#endif

extern "C"
{
    typedef IplImage Image;

	typedef struct 
	{
        int kernelClass;
	} EntryResult;

// FinishRun: id-numer obiektu, tabResult-tablica wartości wynikowych, numResult-liczba elementów w tablicy
    typedef void (*FinishRun)(int id, EntryResult *tabResult, int numResult);
// DebugImageCallback: id-numer obiektu, idInImg-numer obrazu, imgName-nazwa/identyfikator obrazu, debugImg-wskaźnik do pojedynczego obrazka
    typedef void (*DebugImageCallback)(int id, int idInImg, const char* imgName, const Image *debugImg);
// DebugResultCallback: id-numer obiektu, tabResult-tablica wartości wynikowych, numResult-wielkość tablicy
//    typedef void (*DebugResultCallback)(int id, int idInImg, const EntryResult *tabResult, int numResult);
// GetParamCallback: zwraca wartość parametru o identyfikatorze idParam. Jeśli NAN to należy przyjąć wartości domyślne.
    typedef double (*GetParamCallback)(int idParam);
// SystemLogCallback: id-numer obiektu (jesli -1 to dotyczy raportu zbiorczego), logId-rodzaj komunikatu, message-komunikat tekstowy UTF-8
    typedef void (*SystemLogCallback)(int id, int idInImg, int logId, const char* message);

// Komunikaty i informacje zwrotne, wskaźniki wszystkich callback muszą być ustawione przed wywołaniem funkcji Init()
    /**
     * @brief setSystemLogCallback ustawienie wskaźnika funkcji wywoływanej w celu przekazania komunikatu.
     * @param _systemLog wskaźnik funkcji
     */
	API_ZIARNO void setSystemLogCallback(SystemLogCallback _systemLog);

    /**
     * @brief setDebugImageCallback ustawienie wskaźników funkcji wywoływanych w celu przekazania obrazu (wyniki pośrednie).
     * @param _debugImage wskaźnik funkcji dotyczącej obiektu
     * @param _debugImageSub wskaźnik funkcji dotyczacej obrazu obiektu
     */
	API_ZIARNO void setDebugImageCallback(DebugImageCallback _debugImage);

//    /**
//     * @brief setDebugResultCallback ustawienie callback dla przekazywania tablicy wyników
//     * @param _debugResultReport wywołanie dotyczy raportu, serii ziarniaków
//     * @param _debugResultObject wywołanie dotyczy obiektu/ziarniaka
//     * @param _debugResultSub wywołanie dotyczy obrazu ziarniaka (jenego z numInImg)
//     */
//	API_ZIARNO void setDebugResultCallback(DebugResultCallback _debugResult);

    /**
     * @brief setParamCallback ustawienie callback dla odczytu parametrów typu double z repozytorium parametrów
     * @param _paramCallback wskaźnik funkcji
     */
    API_ZIARNO void setParamCallback(GetParamCallback _paramCallback);

// Sterowanie
    /**
     * @brief setFinishRun ustawienie wskaźnika funkcji wywoływanej po zakończeniu analizy obiektu. Zasoby alokowane dla analizy ziarniaka są zwalniane po powrocie z funkcji FinishRun.
     * @param _onFinish wskaźnik funkcji
     */
	API_ZIARNO void setFinishRun(FinishRun _onFinish);

    /**
     * @brief RunAsync funkcja tworzy wątek analizy i niezwłocznie kończy działanie. W wątku analizy wywoływane są funkcje callback.
     * @param id identyfikator obiektu (ziarniaka)
     * @param debuglevel poziom analizy i raportowania
     * @param tabInImg wskaźnik tablicy obrazów
     * @param numInImg liczba obrazów w tablicy
     * @return true gdy jest fajnie, false jesli nie udało się utworzyć wątku roboczego
     */
	API_ZIARNO bool RunAsync(int id, int debuglevel, Image **tabInImg, int numInImg);

    /**
     * @brief setDebugLevelForInitialization
     * @param debugLevel dla funkcji Init() i SetDiskImages()
     */
    API_ZIARNO void setDebugLevelForInitialization(int debugLevel);

    /**
     * @brief Init inicjuje analizę serii obiektów/ziaren. Musi być wywołana przez program główny w celu uaktualnienia parametrów oraz rozpoczęcia analizy serii ziarniaków.
     * @param configfile to sciezka i nazwa pliku konfiguracyjnego. Jesli NULL to wykorzystuje domyslny plik o nazwie "ZiarnoKonfig.txt"
     * @return
     */
    API_ZIARNO bool Init(const char* configfile, int thread_priority);

    /**
     * @brief SetDiskImages przyjmuje serie obrazow samego tla bez podawania ziarniakow
     * @param imagetab tablica wskaznikow obrazow
     * @param cameras liczba kamer/obrazow dla pojedynczego ziarna
     * @param objects liczba akwizycji
     * @return zwraca wartosc zalezna od stopnia zdegenerowania/zabrudzenia dysku szklanego.
     */
    API_ZIARNO int SetDiskImages(Image** imagetab, int cameras, int objects);

    /**
     * @brief End zakończenie analizy serii obiektów/ziaren. Jeśli analiza jest w toku to powinna być przerwana.
     * @param _wait false:anuluje/przerywa wątki, true:czeka na zakończenie wątków
     */
	API_ZIARNO void End(bool _wait = true);
}

#endif // INTERFACE_ZIARNO_H
