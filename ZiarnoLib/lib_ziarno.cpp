/*
* ziarno.cpp - implementacja metod modulu analizy ziaren jeczmienia
* Opcjonalna obsluga watkow w Windows API i POSIX
*
* Autor:  (2016-2017) Piotr M. Szczypinski, Politechnika Lodzka
*
* Opracowano w ramach grantu: NCBR PBS3/A8/38/2015
* Opracowanie przemyslowej metody automatycznej oceny parametrow
* technologicznych i klasyfikacji ziarna z zastosowaniem analizy
* obrazow.
*/
#include <stdio.h>
#include "lib_ziarno.h"
#include <iostream>
#include <opencv2/highgui/highgui_c.h>
#include <opencv2/imgproc/imgproc_c.h>
#include <list>

#include "../../qmazda/SharedImage/mazdaimage.h"
#include "../../qmazda/SharedImage/mazdaimageio.h"
#include "../../qmazda/MzGenerator/featureio.h"
#include "dectree.h"
#include "segment.h"
#include "sideheight.h"


#ifdef USE_DEBUG_LOG
std::ofstream debugfile;
#endif


int defaultDebugLevel = DEBUG_LOG_ERRORS;
FinishRun finishRun = NULL;
DebugImageCallback debugImage = NULL;
SystemLogCallback systemLog = NULL;
std::string A_ = "A_";
std::string B_ = "B_";

void notify(void){} //Required by the generator module

struct QmazdaSetup
{
    DecisionTree* kaskadaCreaseUp;
    DecisionTree* kaskadaCreaseDn;
    std::vector <std::string>* allClassNames;
};
QmazdaSetup qmazdaSetup;

const int commonFeaturesCount = 5;
const int S_orientation_index = 0;
const int S_size_index = 1;
const int S_sizeA_index = 2;
const int S_sizeB_index = 3;
const int S_sizeS_index = 4;
const char* commonFeaturesNames[commonFeaturesCount] =
{
    "S_orientation",
    "S_size",
    "S_sizeA",
    "S_sizeB",
    "S_sizeS"
};

double top_dpmm = mznan;
double bottom_dpmm = mznan;
double side_dpmm = mznan;
double strainer_min = mznan;
double strainer_max = mznan;
double top_pivot_xo = mznan;
double top_pivot_yo = mznan;
double bottom_pivot_xo = mznan;
double bottom_pivot_yo = mznan;
double side_to_pivot = mznan;
double top_to_disk = mznan;
double bottom_to_disk = mznan;

//double dcentertop;
//double dcenterbot;

Image* GetBackgroundImage(unsigned int i)
{
    if(i >= maxNumberOfBackgrounds)
        return NULL;
    return segmentBackgroundImagesIn[i];
}

void SetBackgroundImage(Image* background, unsigned int i)
{
    if(i > maxNumberOfBackgrounds) return;
    if(segmentBackgroundImagesIn[i] != NULL)
        cvReleaseImage(&segmentBackgroundImagesIn[i]);
    segmentBackgroundImagesIn[i] = background;
    segmentInitialize();

    if((defaultDebugLevel & DEBUG_DUMP_IMAGES))
    {
        for(unsigned int c = 0; c < maxNumberOfBackgrounds; c++)
        {
            if(segmentBackgroundImagesTh[c] != NULL)
            {
                std::stringstream ss;
                ss << "Threshold_" << c;
                debugImage(-1, c, ss.str().c_str(), segmentBackgroundImagesTh[c]);
            }
        }
    }
}

bool loadParameters(istream* file)
{
#ifdef USE_DEBUG_LOG
    debugfile.open("./mz_debug_log.txt");
#endif

    char *prevloc = setlocale(LC_ALL, NULL);
    if(prevloc) prevloc = strdup(prevloc);
    setlocale(LC_ALL, "C");
    std::string sline;
    while(std::getline(*file, sline))
    {
        std::stringstream ss(sline);
        std::string name;
        double value;
        name.clear();
        ss >> std::skipws >> name;
        ss >> std::skipws >> value;
        if(name == "%grey_level_threshold") greyLevelThreshold = value;
        else if(name == "%top_dpmm") top_dpmm = value;
        else if(name == "%bottom_dpmm") bottom_dpmm = value;
        else if(name == "%side_dpmm") side_dpmm = value;
        else if(name == "%strainer_min") strainer_min = value;
        else if(name == "%strainer_max") strainer_max = value;
        else if(name == "%top_pivot_xo") top_pivot_xo = value;
        else if(name == "%top_pivot_yo") top_pivot_yo = value;
        else if(name == "%bottom_pivot_xo") bottom_pivot_xo = value;
        else if(name == "%bottom_pivot_yo") bottom_pivot_yo = value;
        else if(name == "%side_to_pivot") side_to_pivot = value;
        else if(name == "%top_to_disk") top_to_disk = value;
        else if(name == "%bottom_to_disk") bottom_to_disk = value;

        else if(name == "%%") break;
    }
    setlocale(LC_ALL, prevloc);
    free(prevloc);

    if(greyLevelThreshold < 0) return false;
    if(!(top_dpmm == top_dpmm)) return false;
    if(!(bottom_dpmm == bottom_dpmm)) return false;
    if(!(side_dpmm == side_dpmm)) return false;
    if(!(strainer_min == strainer_min)) return false;
    if(!(strainer_max == strainer_max)) return false;
    if(!(top_pivot_xo == top_pivot_xo)) return false;
    if(!(top_pivot_yo == top_pivot_yo)) return false;
    if(!(bottom_pivot_xo == bottom_pivot_xo)) return false;
    if(!(bottom_pivot_yo == bottom_pivot_yo)) return false;
    if(!(side_to_pivot == side_to_pivot)) return false;
    if(!(top_to_disk == top_to_disk)) return false;
    if(!(bottom_to_disk == bottom_to_disk)) return false;

    segmentInitialize();
    if((defaultDebugLevel & DEBUG_DUMP_IMAGES))
    {
        for(unsigned int c = 0; c < maxNumberOfBackgrounds; c++)
        {
            if(segmentBackgroundImagesTh[c] != NULL)
            {
                std::stringstream ss;
                ss << "Threshold_" << c;
                debugImage(-1, c, ss.str().c_str(), segmentBackgroundImagesTh[c]);
            }
        }
    }
    return true;
}

bool loadParameters(const char* fileName)
{
    std::ifstream file;
    file.open(fileName);
    if (!file.is_open()) return false;
    if (!file.good()) return false;
    return loadParameters(&file);
}

void qmazdaSetDebugLevel(int debugLevel)
{
    defaultDebugLevel = debugLevel;
}

double qmazdaSideProfileHeight(const std::vector<Image *> *input, int debug, int id)
{
    if((*input).size() < 3) return -1000;
    unsigned int warning = 0;
    double ret = findGrainHeigh((*input)[2], debug, &warning);

    if(debug & DEBUG_DUMP_IMAGES)
        debugImage(id, 2, "Side", (*input)[2]);

    if(debug & DEBUG_LOG_WARNINGS)
    {
        if(warning & segmentation_warning_outframe)
            systemLog(id, 2, ID_LOG_WARNING, "segmentation: Out of frame");
        if(warning & segmentation_warning_multiple)
            systemLog(id, 2, ID_LOG_WARNING, "segmentation: Multiple objects");
        if(warning & segmentation_warning_noobject)
            systemLog(id, 2, ID_LOG_WARNING, "segmentation: Object missing");
    }

    return ret;
}

double qmazdaRecomputeSizes(double* values, const double hside, const unsigned int imageHeight, const unsigned int imageWidth)
{
    unsigned int A_index = commonFeaturesCount;
    unsigned int B_index = commonFeaturesCount+featureNamesFromContourCount;

// Coordinates of kernel's center in top and bottom images refered to pivot
    double xtopmm = values[A_index + x_index]/top_dpmm - top_pivot_xo;
    double ytopmm = values[A_index + y_index]/top_dpmm - top_pivot_yo;
    double xbotmm = values[B_index + x_index]/bottom_dpmm - bottom_pivot_xo;
    double ybotmm = values[B_index + y_index]/bottom_dpmm - bottom_pivot_yo;

    double xx = (top_pivot_xo-imageWidth/(2.0*top_dpmm)); xx *= xx;
    double yy = (top_pivot_yo-imageHeight/(2.0*top_dpmm)); yy *= yy;
    double centerToPivot = sqrt(xx+yy);
// Height of the kernel from the side image
    double SizeS  = (side_to_pivot - sqrt(xtopmm*xtopmm + ytopmm*ytopmm)) / (side_to_pivot-centerToPivot);

    xx = (bottom_pivot_xo-imageWidth/(2.0*bottom_dpmm)); xx *= xx;
    yy = (bottom_pivot_yo-imageHeight/(2.0*bottom_dpmm)); yy *= yy;
    centerToPivot = sqrt(xx+yy);
    SizeS += ((side_to_pivot - sqrt(xbotmm*xbotmm + ybotmm*ybotmm)) / (side_to_pivot-centerToPivot));
    SizeS *= (hside/(2.0*side_dpmm));

// Width of the kernel from top and bottom images
    double SizeT = (values[A_index + width_index]-1) * (top_to_disk - SizeS/2.0) / top_to_disk / top_dpmm;
    double SizeB = (values[B_index + width_index]-1) * (bottom_to_disk + SizeS/2.0) / bottom_to_disk / bottom_dpmm;

    values[S_sizeA_index] = SizeT;
    values[S_sizeB_index] = SizeB;
    values[S_sizeS_index] = SizeS;

    if(SizeS > SizeT) SizeS = SizeT;
    if(SizeS > SizeB) SizeS = SizeB;
    values[S_size_index] = SizeS;
    return SizeS;
}


int qmazdaSegmentacja(const std::vector<Image *> *src, std::vector<MIType2DRGB *> * output, std::vector<MzRoi2D *> *outroi, double* features, int id, int debuglevel)
{
    MR2DType* mask[2];
    MIType2DRGB* image[2];
    int ret = 0;

    unsigned int warning;

    double angle = -100000;
    warning = 0;
    double grad0 = segmentSingleImage(&mask[0], &image[0], (*src)[0], 0, features, &angle, 0, &warning);


    if(debuglevel&DEBUG_LOG_WARNINGS)
    {
        if(warning & segmentation_warning_outframe)
            systemLog(id, 0, ID_LOG_WARNING, "segmentation: Out of frame");
        if(warning & segmentation_warning_multiple)
            systemLog(id, 0, ID_LOG_WARNING, "segmentation: Multiple objects");
        if(warning & segmentation_warning_noobject)
            systemLog(id, 0, ID_LOG_WARNING, "segmentation: Object missing");
    }

    warning = 0;
    double grad1 = segmentSingleImage(&mask[1], &image[1], (*src)[1], 1, features+featureNamesFromContourCount, &angle, 1, &warning);

    if(debuglevel&DEBUG_LOG_WARNINGS)
    {
        if(warning & segmentation_warning_outframe)
            systemLog(id, 1, ID_LOG_WARNING, "segmentation: Out of frame");
        if(warning & segmentation_warning_multiple)
            systemLog(id, 1, ID_LOG_WARNING, "segmentation: Multiple objects");
        if(warning & segmentation_warning_noobject)
            systemLog(id, 1, ID_LOG_WARNING, "segmentation: Object missing");
    }

    if(grad0 <= 0 || grad1 <= 0)
        return -1;

    mask[0]->SetName("A");
    mask[1]->SetName("B");

    if(grad0 > grad1)
    {
        mask[1]->SetColor(0x0000ff00);
        mask[0]->SetColor(0x00ff0000);
        ret = 0;
    }
    else
    {
        mask[0]->SetColor(0x0000ff00);
        mask[1]->SetColor(0x00ff0000);
        ret = 1;
    }
    output->push_back(image[0]);
    output->push_back(image[1]);
    outroi->push_back(mask[0]);
    outroi->push_back(mask[1]);
    return ret;
}

void qmazdaSegmentacjaSaveMaskImage(std::vector<MIType2DRGB *> * output, std::vector<MzRoi2D *> *outroi, int id)
{
    unsigned int ni = output->size();
    if(ni > outroi->size()) ni = outroi->size();

    for(unsigned int i = 0; i < ni; i++)
    {
        unsigned int size[4];
        (*output)[i]->GetSize(size);

        MazdaImageIterator<MIType2DRGB> outiterator((*output)[i]);
        Image* img = cvCreateImage(cvSize(size[0], size[1]), IPL_DEPTH_8U, 3);
        for(unsigned int y = 0; y < size[1]; y++)
        {
            unsigned char* ip = (unsigned char*)(img->imageData) + img->widthStep*y;
            for(unsigned int x = 0; x < size[0]; x++)
            {
                PixelRGBType* p = outiterator.GetPixelPointer();

#ifdef MAZDA_IMAGE_8BIT_PIXEL
                *ip = p->channel[0]; ip++;
                *ip = p->channel[1]; ip++;
                *ip = p->channel[2]; ip++;
#else
                *ip = p->channel[0] >> 8; ip++;
                *ip = p->channel[1] >> 8; ip++;
                *ip = p->channel[2] >> 8; ip++;
#endif
                ++outiterator;
            }
        }
        std::string ts = (*outroi)[i]->GetName() + "_Image";
        debugImage(id, i, ts.c_str(), img);

        int begin[2];
        int end[2];
        unsigned char colors[4];
        unsigned int color = (*outroi)[i]->GetColor();

        colors[0] = (color&0xff);
        colors[1] = ((color>>8)&0xff);
        colors[2] = ((color>>16)&0xff);
        (*outroi)[i]->GetBegin(begin);
        (*outroi)[i]->GetEnd(end);

        MazdaRoiIterator<MzRoi2D> rit((*outroi)[i]);
        for(int y = begin[1]; y <= end[1]; y++)
        {
            unsigned char* ip = (unsigned char*)(img->imageData) + img->widthStep*y;
            for(int x = begin[0]; x <= end[0]; x++)
            {
                if(rit.GetPixel())
                {
                    *ip = colors[0]; ip++;
                    *ip = colors[1]; ip++;
                    *ip = colors[2]; ip++;
                }
                else
                {
                    *ip = 0; ip++;
                    *ip = 0; ip++;
                    *ip = 0; ip++;
                }
                ++rit;
            }
        }
        ts = (*outroi)[i]->GetName() + "_Mask";
        debugImage(id, i, ts.c_str(), img);

        cvReleaseImage(&img);
    }
}

void qmazdaSegmentacjaDump(std::vector<MIType2DRGB *> * output, std::vector<MzRoi2D *> *outroi, int id)
{
    unsigned int ni = output->size();
    if(ni > outroi->size()) ni = outroi->size();

    for(unsigned int i = 0; i < ni; i++)
    {
        unsigned int size[4];
        (*output)[i]->GetSize(size);

        MazdaImageIterator<MIType2DRGB> outiterator((*output)[i]);
        Image* img = cvCreateImage(cvSize(size[0], size[1]), IPL_DEPTH_8U, 3);
        for(unsigned int y = 0; y < size[1]; y++)
        {
            unsigned char* ip = (unsigned char*)(img->imageData) + img->widthStep*y;
            for(unsigned int x = 0; x < size[0]; x++)
            {
                PixelRGBType* p = outiterator.GetPixelPointer();

#ifdef MAZDA_IMAGE_8BIT_PIXEL
                *ip = p->channel[0]; ip++;
                *ip = p->channel[1]; ip++;
                *ip = p->channel[2]; ip++;
#else
                *ip = p->channel[0] >> 8; ip++;
                *ip = p->channel[1] >> 8; ip++;
                *ip = p->channel[2] >> 8; ip++;
#endif
                ++outiterator;
            }
        }

        int begin[2];
        int end[2];
        unsigned char colors[4];
        unsigned int color = (*outroi)[i]->GetColor();

        colors[0] = (color&0xff)/2;
        colors[1] = ((color>>8)&0xff)/2;
        colors[2] = ((color>>16)&0xff)/2;
        (*outroi)[i]->GetBegin(begin);
        (*outroi)[i]->GetEnd(end);

        MazdaRoiIterator<MzRoi2D> rit((*outroi)[i]);
        for(int y = begin[1]; y <= end[1]; y++)
        {
            unsigned char* ip = (unsigned char*)(img->imageData) + img->widthStep*y;
            for(int x = begin[0]; x <= end[0]; x++)
            {
                if(rit.GetPixel())
                {
                    *ip = *ip/2 + colors[0]; ip++;
                    *ip = *ip/2 + colors[1]; ip++;
                    *ip = *ip/2 + colors[2]; ip++;
                }
                else
                {
                    ip+=3;
                }
                ++rit;
            }
        }
        std::string ts = (*outroi)[i]->GetName() + "_RotatedCroppedOverlay";
        debugImage(id, i, ts.c_str(), img);
        cvReleaseImage(&img);
    }
}

void qmazdaRun(ZiarnoWorkerInput* input)
{
    QmazdaSetup* setup = (QmazdaSetup*) input->qmazdaSetup;
    double* values;

    unsigned int max_vals_max;
    unsigned int max_vals;
    max_vals = setup->kaskadaCreaseUp->allFeatureNames.size();
    max_vals_max = setup->kaskadaCreaseDn->allFeatureNames.size();
    if(max_vals_max < max_vals) max_vals_max = max_vals;
    values = new double[max_vals_max];
    for(unsigned int i = 0; i < max_vals_max; i++) values[i] = mznan;

    int klasa = 0;

    std::vector<MIType2DRGB*> output;
    std::vector<MzRoi2D*> outroi;

    int orientation = qmazdaSegmentacja(&input->tabInImg, &output, &outroi, values + commonFeaturesCount, input->id, input->debuglevel);
    values[S_orientation_index] = orientation;

    if(orientation < 0 || outroi.size() < 2)
    {
        if(input->debuglevel&DEBUG_LOG_ERRORS) systemLog(input->id, -1, ID_LOG_ERROR, "qmazdaRun: Segmentation failed (class = 0)");
        EntryResult results[1];
        results[0].kernelClass = klasa;
        finishRun(input->id, results, 1);
        return;
    }

    if(input->debuglevel&DEBUG_DUMP_IMAGES)
        qmazdaSegmentacjaDump(&output, &outroi, input->id);

    if(orientation == 0)
        max_vals = setup->kaskadaCreaseUp->allFeatureNames.size();
    else
        max_vals = setup->kaskadaCreaseDn->allFeatureNames.size();

    double height = qmazdaSideProfileHeight(& input->tabInImg, input->debuglevel, input->id);
    if(height <= 0)
    {
        if(input->debuglevel&DEBUG_LOG_WARNINGS)
            systemLog(input->id, -1, ID_LOG_WARNING, "qmazdaRun: Failed to compute side profile");
    }
    height = qmazdaRecomputeSizes(values, height, input->tabInImg[0]->height, input->tabInImg[0]->width);

    if(height < strainer_min)
    {
        klasa = 1;
    }
    else if(height >= strainer_max)
    {
        klasa = 2;
    }
    else
    {
        if(orientation == 0)
            klasa = setup->kaskadaCreaseUp->decisionTreeClassification(&output, &outroi, values);
        else
            klasa = setup->kaskadaCreaseDn->decisionTreeClassification(&output, &outroi, values);
    }

    if((input->debuglevel & DEBUG_LOG_INFO))
    {
        std::stringstream ss;
        ss << "qmazdaRun:Values";
        if(orientation == 0)
            ss << "Up:";
        else
            ss << "Dn:";
        for(unsigned int i = 0; i < max_vals; i++)
             ss << " " << values[i];
        systemLog(input->id, -1, ID_LOG_INFO, ss.str().c_str());
    }

    if(klasa == 0)
        if(input->debuglevel & DEBUG_LOG_ERRORS)
            systemLog(input->id, -1, ID_LOG_ERROR, "qmazdaRun: Decision tree returned class = 0");

    for(unsigned int i = 0; i < output.size(); i++) delete output[i];
    output.clear();
    for(unsigned int i = 0; i < outroi.size(); i++) delete outroi[i];
    outroi.clear();
    delete[] values;

    if((defaultDebugLevel & DEBUG_LOG_INFO))
    {
        std::stringstream ss;
        ss << "qmazdaRun: class = " << klasa;
        if((unsigned int)klasa < setup->allClassNames->size()) ss << ": " << (*setup->allClassNames)[klasa];
        systemLog(input->id, -1, ID_LOG_INFO, ss.str().c_str());
    }

    EntryResult results[1];
    results[0].kernelClass = klasa;
    finishRun(input->id, results, 1);
    if(input->debuglevel&DEBUG_LOG_INFO)
        systemLog(input->id, -1, ID_LOG_INFO, "qmazdaRun: Finished normally");
}

int initializeBackgrounds(Image** imagetab, int cameras, int objects)
{
    int nimages = cameras * objects;
    if(nimages <= 0) return -1000;
    int toret = segmentComputeBackgroundImages(imagetab, cameras, objects);

    if(toret < 0)
    {
        if((defaultDebugLevel & DEBUG_LOG_ERRORS))
        {
            std::stringstream ss;
            ss << "initializeBackgrounds: Error = " << toret;
            systemLog(-1, -1, DEBUG_LOG_ERRORS, ss.str().c_str());
        }
    }
    if((defaultDebugLevel & DEBUG_LOG_INFO))
    {
        std::stringstream ss;
        ss << "initializeBackgrounds: WearOf = " << toret << "/1000";
        if(toret < 0) ss << " (error)";
        else if(toret < 25) ss << " (excelent)";
        else if(toret < 50) ss << " (good)";
        else if(toret < 75) ss << " (clean)";
        else ss << " (replace)";
        systemLog(-1, -1, DEBUG_LOG_INFO, ss.str().c_str());
    }
    if((defaultDebugLevel & DEBUG_DUMP_IMAGES))
    {
        for(unsigned int c = 0; c < maxNumberOfBackgrounds; c++)
        {
            std::stringstream ss;
            ss << "Backgorund_" << c;
            debugImage(-1, c, ss.str().c_str(), segmentBackgroundImagesIn[c]);
        }
//        for(unsigned int c = 0; c < maxNumberOfBackgrounds; c++)
//        {
//            std::stringstream ss;
//            ss << "Threshold_" << c;
//            debugImage(-1, c, ss.str().c_str(), segmentBackgroundImagesTh[c]);
//        }
    }
    return toret;
}

void qmazdaAppendSegmentationFeatureNames(std::vector <std::string>* names)
{
    for(unsigned int i = 0; i < commonFeaturesCount; i++)
    {
        names->push_back(commonFeaturesNames[i]);
    }
    for(unsigned int i = 0; i < featureNamesFromContourCount; i++)
    {
        names->push_back(A_ + featureNamesFromContour[i]);
    }
    for(unsigned int i = 0; i < featureNamesFromContourCount; i++)
    {
        names->push_back(B_ + featureNamesFromContour[i]);
    }


    for(unsigned int i = 0; i < featureConvexNamesFromContourCount; i++)
    {
        names->push_back(B_ + featureConvexNamesFromContour[i]);
    }
    for(unsigned int i = 0; i < featureDistanceNamesFromContourCount; i++)
    {
        names->push_back(B_ + featureDistanceNamesFromContour[i]);
    }
}

void* qmazdaInit(const char *configuration_filename)
{
    itk::MultiThreader::SetGlobalMaximumNumberOfThreads( 1 );
    itk::MultiThreader::SetGlobalDefaultNumberOfThreads( 1 );

    qmazdaEnd();
    std::ifstream file;
    file.open(configuration_filename);
    if (!file.is_open())
    {
        if((defaultDebugLevel & DEBUG_LOG_ERRORS))
        {
            systemLog(-1, -1, ID_LOG_ERROR, "qmazdaInit: Failed to open configuration file:");
            systemLog(-1, -1, ID_LOG_ERROR, configuration_filename);
        }
        return NULL;
    }
    if (!file.good())
    {
        if((defaultDebugLevel & DEBUG_LOG_ERRORS))
        {
            systemLog(-1, -1, ID_LOG_ERROR, "qmazdaInit: Failed to open configuration file:");
            systemLog(-1, -1, ID_LOG_ERROR, configuration_filename);
        }
        return NULL;
    }

    qmazdaSetup.kaskadaCreaseUp = new DecisionTree();
    qmazdaSetup.kaskadaCreaseDn = new DecisionTree();
    qmazdaSetup.allClassNames = new std::vector <std::string>();

    qmazdaSetup.kaskadaCreaseUp->parse(&file, '*');
    qmazdaSetup.kaskadaCreaseDn->parse(&file, '*');

    bool lpok = loadParameters(&file);
    if((defaultDebugLevel & DEBUG_LOG_INFO))
    {
        std::stringstream ss;
        ss << "qmazdaInit: [Parameters]:";
        ss << " grey_level_threshold " << greyLevelThreshold;
        ss << " top_dpmm " << top_dpmm;
        ss << " bottom_dpmm " << bottom_dpmm;
        ss << " side_dpmm " << side_dpmm;
        ss << " strainer_min " << strainer_min;
        ss << " strainer_max " << strainer_max;
        ss << " top_pivot_xo " << top_pivot_xo;
        ss << " top_pivot_yo " << top_pivot_yo;
        ss << " bottom_pivot_xo " << bottom_pivot_xo;
        ss << " bottom_pivot_yo " << bottom_pivot_yo;
        ss << " side_to_pivot " << side_to_pivot;
        ss << " top_to_disk " << top_to_disk;
        ss << " bottom_to_disk " << bottom_to_disk;
        systemLog(-1, -1, ID_LOG_INFO, ss.str().c_str());
    }
    if(!lpok)
    {
        if((defaultDebugLevel & DEBUG_LOG_ERRORS))
        {
            systemLog(-1, -1, ID_LOG_ERROR, "qmazdaInit: Failed to load or incomplete configuration file:");
            systemLog(-1, -1, ID_LOG_ERROR, configuration_filename);
        }
        return NULL;
    }

    qmazdaAppendSegmentationFeatureNames(& qmazdaSetup.kaskadaCreaseUp->allFeatureNames);
    qmazdaAppendSegmentationFeatureNames(& qmazdaSetup.kaskadaCreaseDn->allFeatureNames);

    qmazdaSetup.allClassNames->push_back("%Error%");
    qmazdaSetup.allClassNames->push_back("%Small%");
    qmazdaSetup.allClassNames->push_back("%Large%");

    qmazdaSetup.kaskadaCreaseUp->errorMessage.clear();
    qmazdaSetup.kaskadaCreaseDn->errorMessage.clear();
    if(!qmazdaSetup.kaskadaCreaseUp->loadClassifiers(qmazdaSetup.allClassNames))
    {
        if((defaultDebugLevel & DEBUG_LOG_ERRORS))
        {
            std::stringstream ss;
            ss << "qmazdaInit: Failed to load CreaseUp decision tree: " << qmazdaSetup.kaskadaCreaseUp->errorMessage;
            systemLog(-1, 0, ID_LOG_ERROR, ss.str().c_str());
        }
        return NULL;
    }

    if(!qmazdaSetup.kaskadaCreaseDn->loadClassifiers(qmazdaSetup.allClassNames))
    {
        if((defaultDebugLevel & DEBUG_LOG_ERRORS))
        {
            std::stringstream ss;
            ss << "qmazdaInit: Failed to load CreaseDn decision tree: " << qmazdaSetup.kaskadaCreaseDn->errorMessage;
            systemLog(-1, 0, ID_LOG_ERROR, ss.str().c_str());
        }
        return NULL;
    }

// Notifies about tree structure
    if((defaultDebugLevel & DEBUG_LOG_INFO))
    {
        std::stringstream ss;
        ss << "qmazdaInit:CreaseUp:\n" << qmazdaSetup.kaskadaCreaseUp->serialize('\t');
        systemLog(-1, 0, ID_LOG_INFO, ss.str().c_str());
        ss.str(std::string());
        ss << "qmazdaInit:CreaseDn:\n" << qmazdaSetup.kaskadaCreaseDn->serialize('\t');
        systemLog(-1, 1, ID_LOG_INFO, ss.str().c_str());
    }

    if((defaultDebugLevel & DEBUG_LOG_INFO))
    {
        std::stringstream ss;
        ss << "qmazdaInit:ClassNames:\n";
        for(unsigned int i = 0; i < qmazdaSetup.allClassNames->size(); i++)
            ss << i << ": " << (* qmazdaSetup.allClassNames)[i] <<"\n";
        systemLog(-1, -1, ID_LOG_INFO, ss.str().c_str());
    }

    if((defaultDebugLevel & DEBUG_LOG_INFO))
    {
        std::stringstream ss;
        ss << "qmazdaInit:FeaturesUp:";
        for(unsigned int i = 0; i < qmazdaSetup.kaskadaCreaseUp->allFeatureNames.size(); i++)
             ss << " " << qmazdaSetup.kaskadaCreaseUp->allFeatureNames[i];
        systemLog(-1, 0, ID_LOG_INFO, ss.str().c_str());
        ss.str(std::string());
        ss << "qmazdaInit:FeaturesDn:";
        for(unsigned int i = 0; i < qmazdaSetup.kaskadaCreaseDn->allFeatureNames.size(); i++)
             ss << " " << qmazdaSetup.kaskadaCreaseDn->allFeatureNames[i];
        systemLog(-1, 1, ID_LOG_INFO, ss.str().c_str());
    }

    if(!qmazdaSetup.kaskadaCreaseUp->renumberClasses(qmazdaSetup.allClassNames))
    {
        if((defaultDebugLevel & DEBUG_LOG_ERRORS))
        {
            systemLog(-1, 0, ID_LOG_ERROR, "qmazdaInit: Failed reindexing CreaseUp classes");
        }
    }
    if(!qmazdaSetup.kaskadaCreaseDn->renumberClasses(qmazdaSetup.allClassNames))
    {
        if((defaultDebugLevel & DEBUG_LOG_ERRORS))
        {
            systemLog(-1, 1, ID_LOG_ERROR, "qmazdaInit: Failed reindexing CreaseDn classes");
        }
    }
    segmentInitialize();

    if((defaultDebugLevel & DEBUG_DUMP_IMAGES))
    {
        for(unsigned int c = 0; c < maxNumberOfBackgrounds; c++)
        {
            if(segmentBackgroundImagesTh[c] != NULL)
            {
                std::stringstream ss;
                ss << "Threshold_" << c;
                debugImage(-1, c, ss.str().c_str(), segmentBackgroundImagesTh[c]);
            }
        }
    }
    return (void*)&qmazdaSetup;
}

void qmazdaEnd(void)
{
    if(qmazdaSetup.kaskadaCreaseUp != NULL) delete qmazdaSetup.kaskadaCreaseUp;
    qmazdaSetup.kaskadaCreaseUp = NULL;
    if(qmazdaSetup.kaskadaCreaseDn != NULL) delete qmazdaSetup.kaskadaCreaseDn;
    qmazdaSetup.kaskadaCreaseDn = NULL;
    if(qmazdaSetup.allClassNames != NULL) delete qmazdaSetup.allClassNames;
    qmazdaSetup.allClassNames = NULL;
}

/**
 * @brief qmazdaGenerate tryb gnerowania cech dla uczenia
 * @param configuration_filename
 * @param tabInImg
 * @param featureNamesOut
 * @param featureValuesOut
 */
void qmazdaGenerate(const char *configuration_filename, std::vector <Image *>* tabInImg, std::vector <std::string>* featureNamesOut, double** featureValuesOut)
{
//    std::string A_ = "A_";
//    std::string B_ = "B_";

    itk::MultiThreader::SetGlobalMaximumNumberOfThreads( 1 );
    itk::MultiThreader::SetGlobalDefaultNumberOfThreads( 1 );

    if(featureNamesOut == NULL) return;
    std::vector <std::string> featureNames;

    featureNames = loadFeatureList(configuration_filename);

    std::vector <std::string> featureNamesVerified;
    NameStubTree templateTree;
    StubItemBuilder::CreateTemplate(&templateTree, roiTreeTemplate, sizeof(roiTreeTemplate)/sizeof(ItemTemplate));

    StubItemBuilder* featureComputationObject = new StubItemBuilder(2, &templateTree);
    for(unsigned int i = 0; i < featureNames.size(); i++)
        if(featureComputationObject->addFeature(featureNames[i], featureNamesVerified.size(), NULL))
        {
            featureNamesVerified.push_back(featureNames[i]);
        }
    featureNames.clear();

    if((defaultDebugLevel & DEBUG_LOG_WARNINGS))
    {
        if(featureNamesVerified.size() <= 0)
        {
            systemLog(-1, -1, ID_LOG_WARNING, "Empty feature list, check configuration_filename:");
            systemLog(-1, -1, ID_LOG_WARNING, configuration_filename);
        }
    }

// Cechy z segmentacji
    qmazdaAppendSegmentationFeatureNames(featureNamesOut);
    unsigned int first_real_feature = featureNamesOut->size();

// Cechy z generatora
    for(unsigned int i = 0; i < featureNamesVerified.size(); i++)
    {
        featureNamesOut->push_back(A_ + featureNamesVerified[i]);
    }
    for(unsigned int i = 0; i < featureNamesVerified.size(); i++)
    {
        featureNamesOut->push_back(B_ + featureNamesVerified[i]);
    }
    if(tabInImg == NULL || featureValuesOut == NULL)
    {
        if((defaultDebugLevel & DEBUG_LOG_INFO))
        {
            systemLog(-1, -1, ID_LOG_INFO, "qmazdaGenerate: Finishing with feature names only");
        }
        return;
    }


    bool lpok = loadParameters(configuration_filename);
    if((defaultDebugLevel & DEBUG_LOG_INFO))
    {
        std::stringstream ss;
        ss << "qmazdaGenerate: [Parameters]:";
        ss << " grey_level_threshold " << greyLevelThreshold;
        ss << " top_dpmm " << top_dpmm;
        ss << " bottom_dpmm " << bottom_dpmm;
        ss << " side_dpmm " << side_dpmm;
        ss << " strainer_min " << strainer_min;
        ss << " strainer_max " << strainer_max;
        ss << " top_pivot_xo " << top_pivot_xo;
        ss << " top_pivot_yo " << top_pivot_yo;
        ss << " bottom_pivot_xo " << bottom_pivot_xo;
        ss << " bottom_pivot_yo " << bottom_pivot_yo;
        ss << " side_to_pivot " << side_to_pivot;
        ss << " top_to_disk " << top_to_disk;
        ss << " bottom_to_disk " << bottom_to_disk;
        systemLog(-1, -1, ID_LOG_INFO, ss.str().c_str());
    }
    if(!lpok)
    {
        if((defaultDebugLevel & DEBUG_LOG_ERRORS))
        {
            systemLog(-1, -1, ID_LOG_ERROR, "qmazdaGenerate: Failed to load or incomplete configuration file:");
            systemLog(-1, -1, ID_LOG_ERROR, configuration_filename);
        }
        return;
    }

    std::vector<MIType2DRGB*> output;
    std::vector<MzRoi2D*> outroi;


    double* values = new double[featureNamesOut->size()];
    for(unsigned int i = 0; i < featureNamesOut->size(); i++) values[i] = mznan;

    *featureValuesOut = values;

    int orientation = qmazdaSegmentacja(tabInImg, &output, &outroi, values + commonFeaturesCount, 0, defaultDebugLevel);
    values[S_orientation_index] = orientation;

    if(orientation < 0 || outroi.size() < 2)
    {
        delete[] values;
        return;
    }

    if((defaultDebugLevel & DEBUG_DUMP_IMAGES))
    {
        qmazdaSegmentacjaDump(&output, &outroi, 0);
    }
    if((defaultDebugLevel & DEBUG_SAVE_IMAGES))
    {
        qmazdaSegmentacjaSaveMaskImage(&output, &outroi, 0);
    }

    if(defaultDebugLevel & DEBUG_LOG_INFO)
    {
        std::stringstream ss;
        ss << "qmazdaGen: [Images] ";
        for(unsigned int i = 0; i < output.size(); i++)
        {
            unsigned int size[4];
            output[i]->GetSize(size);
            ss << size[1] << "x" << size[0] << " ";
        }
        ss << "[Rois] ";
        for(unsigned int i = 0; i < outroi.size(); i++)
        {
            int begin[4];
            int end[4];
            outroi[i]->GetBegin(begin);
            outroi[i]->GetEnd(end);
            ss << "("<< end[1] << "-" << begin[1] << ")x(" << end[0] << "-" << begin[0] << ") ";
        }
        systemLog(0, -1, ID_LOG_INFO, ss.str().c_str());
    }

    double height = qmazdaSideProfileHeight(tabInImg, defaultDebugLevel, -1);

    if(height <= 0)
    {
        if(defaultDebugLevel&DEBUG_LOG_WARNINGS)
            systemLog(0, -1, ID_LOG_WARNING, "qmazdaGenerate: Failed to compute side profile");
    }
    height = qmazdaRecomputeSizes(values, height, (*tabInImg)[0]->height, (*tabInImg)[0]->width);

    if(height < strainer_min || height >= strainer_max)  //analysis cancelled, size too small or too big
    {
        return;
    }
    StubItemDataEntryRoi genData;
    genData.image = output[0];
    genData.isRGB = true;
    genData.roi = outroi[0];
    genData.table = values + first_real_feature;
    featureComputationObject->Execute(&genData);

    genData.image = output[1];
    genData.isRGB = true;
    genData.roi = outroi[1];
    genData.table = values + first_real_feature + featureNamesVerified.size();
    featureComputationObject->Execute(&genData);

    for(unsigned int i = 0; i < output.size(); i++) delete output[i];
    output.clear();
    for(unsigned int i = 0; i < outroi.size(); i++) delete outroi[i];
    outroi.clear();
}
