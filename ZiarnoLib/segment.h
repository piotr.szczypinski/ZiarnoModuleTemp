#ifndef SEGMENT_H
#define SEGMENT_H

//#include "mazdaroi.h"
//#include "mazdaroiio.h"
#include "int_ziarno.h"
#include <opencv2/core/core_c.h>
#include <opencv2/imgproc/imgproc_c.h>
#include "../../qmazda/SharedImage/imagedefs.h"
#include "../../qmazda/SharedImage/mazdaroi.h"
#include "../../qmazda/SharedImage/mazdaroiio.h"

typedef MazdaRoi<unsigned int, 2> MR2DType;
typedef MultiChannelPixel <MazdaImagePixelType, 3> PixelRGBType;
typedef MazdaImage<PixelRGBType , 2> MIType2DRGB;

extern const int x_index;
extern const int y_index;
extern const int width_index;
const unsigned int featureNamesFromContourCount = 22;
const unsigned int featureConvexNamesFromContourCount = 7;
const unsigned int featureDistanceNamesFromContourCount = 4;
extern const char* featureNamesFromContour[featureNamesFromContourCount];
extern const char* featureConvexNamesFromContour[featureConvexNamesFromContourCount];
extern const char* featureDistanceNamesFromContour[featureDistanceNamesFromContourCount];

const unsigned int qualityDividerForBackgroundWearof = 16;
const int maxNumberOfBackgrounds = 2;

extern int greyLevelThreshold;
const double medianFilterRadius = 2;
//const int gradientFilterSigma = 4;

const unsigned int segmentation_warning_noobject = 1;
const unsigned int segmentation_warning_multiple = 2;
const unsigned int segmentation_warning_outframe = 4;

extern Image* segmentBackgroundImagesIn[];
extern Image* segmentBackgroundImagesTh[];
//typedef MazdaRoi<unsigned int, 2> MR2DType;

double segmentOrientationAnalysis(double* upsdn, Image* src, Image* mask);
void segmentBinarizeImage(Image* dst, Image* buf, Image* src, Image* backg);
double segmentGetMaxAreaContour(CvSeq* contours, CvSeq** contour, unsigned int *concount);
double segmentAngleFromContour(CvSeq* contour, double boxin[4], double boxout[4], double oldangle);
void segmentRotateImage(Image* src, Image* dst, double angle, double box[4]);
void segmentVerticalBlur(Image* dst, Image* src, Image* mask);
double segmentHorizontalGradientsAmount(Image* src, Image* mask);
//unsigned int segmentExtractFeaturesFromContourCount(void);
//const char *segmentExtractFeaturesFromContourNames(unsigned int index);
//void segmentExtractFeaturesFromContour(CvSeq* contour, double* features);

double segmentSingleImage(MR2DType **mask, MIType2DRGB **image, Image* src, unsigned int background, double *features, double *angle, int moreFeatures, unsigned int *warning);


int segmentComputeBackgroundImages(Image** imagetab, int cameras, int objects);
void createBallElement(void);
void segmentInitialize(void);

//Obsolete
double segmentAngleFromContour(CvSeq* contour);
void segmentGetBoxes(double boxin[4], double boxout[4], CvPoint *poly, double angle, CvSeq* contour);
double segmentBGOrientationAnalysis(Image* mask);

#endif // SEGMENT_H
