/*
* ziarno.cpp - implementacja metod modulu analizy ziaren jeczmienia
* Opcjonalna obsluga watkow w Windows API i POSIX
*
* Autor:  (2016) Piotr M. Szczypinski, Politechnika Lodzka
*
* Opracowano w ramach grantu: NCBR PBS3/A8/38/2015
* Opracowanie przemyslowej metody automatycznej oceny parametrow
* technologicznych i klasyfikacji ziarna z zastosowaniem analizy
* obrazow.
*/

const char *configuration_filename_def = "ZiarnoKonfig.txt";
char configuration_filename[2048];

#include "lib_ziarno.h"
void* qmazdaData;
int threadPriority = 0;

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
#include <windows.h>
#include <thread>
//typedef HANDLE ThreadHandler;
#define WINDOWS_MM
#else
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
typedef pthread_t ThreadHandler;
#endif

#ifndef WINDOWS_MM
std::vector< ThreadHandler > threadZiarnoList;
#endif

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
HANDLE ziarnoSharedDataMutex = NULL;
#define EnterSharedData WaitForSingleObject(ziarnoSharedDataMutex, INFINITE)
#define LeaveSharedData ReleaseMutex(ziarnoSharedDataMutex)
#define InitializeZiarnaMutex ziarnoSharedDataMutex = CreateMutex(NULL,FALSE,NULL)
#define CloseZiarnaMutex CloseHandle(ziarnoSharedDataMutex); ziarnoSharedDataMutex = NULL;
#else
pthread_mutex_t ziarnoSharedDataMutex = PTHREAD_MUTEX_INITIALIZER;
#define EnterSharedData pthread_mutex_lock(&ziarnoSharedDataMutex)
#define LeaveSharedData pthread_mutex_unlock(&ziarnoSharedDataMutex)
#define InitializeZiarnaMutex pthread_mutex_init(&ziarnoSharedDataMutex, NULL)
#define CloseZiarnaMutex pthread_mutex_init(&ziarnoSharedDataMutex, NULL)
#endif

#ifdef WINDOWS_MM
TP_CALLBACK_ENVIRON cbe;
PTP_CLEANUP_GROUP cleanupGroup = NULL;
PTP_POOL pool = NULL;
VOID CALLBACK cleanGroupCallback(PVOID ObjectContext, PVOID CleanupContext);

void closeThreadPool()
{
    if (cleanupGroup != NULL)
    {
        CloseThreadpoolCleanupGroup(cleanupGroup);
        cleanupGroup = NULL;
    }
    if (pool != NULL)
    {
        CloseThreadpool(pool);
        DestroyThreadpoolEnvironment(&cbe);
        pool = NULL;
    }
}

void initThradPool()
{
    if (pool == NULL)
    {
        int hc = std::thread::hardware_concurrency();
        char buf[512];
        sprintf_s(buf, "hardware_concurrency: %d", hc);
        systemLog(-1, -1, ID_LOG_INFO, buf);
        pool = CreateThreadpool(NULL);
        if (pool == NULL)
        {
            char buf[512];
            sprintf_s(buf, "CreateThreadpool failed.LastError: %u", GetLastError());
            systemLog(-1, -1, ID_LOG_ERROR, buf);
        }
        else
        {
            BOOL bClean = FALSE;
            BOOL bRet;
            SetThreadpoolThreadMaximum(pool, hc);
            bRet = SetThreadpoolThreadMinimum(pool, 1);
            if (FALSE == bRet)
            {
                char buf[512];
                sprintf_s(buf, "SetThreadpoolThreadMinimum failed. LastError: %u", GetLastError());
                systemLog(-1, -1, ID_LOG_ERROR, buf);
                bClean = TRUE;
            }
            else
            {
                cleanupGroup = CreateThreadpoolCleanupGroup();
                if (NULL == cleanupGroup)
                {
                    char buf[512];
                    sprintf_s(buf, "CreateThreadpoolCleanupGroup failed. LastError: %u", GetLastError());
                    systemLog(-1, -1, ID_LOG_ERROR, buf);
                    bClean = TRUE;
                }
                else
                {
                    InitializeThreadpoolEnvironment(&cbe);
                    SetThreadpoolCallbackPool(&cbe, pool);
                    SetThreadpoolCallbackCleanupGroup(&cbe, cleanupGroup, cleanGroupCallback);
                }
            }
            if (bClean) closeThreadPool();
        }
    }
}
#endif

//-----------------------------------------------------------------------------
// FUNKCJA OBLICZENIOWA (WORKER) WATKOW I FUNKCJA LACZACA
//-----------------------------------------------------------------------------

// Wywolywane w utworzonym watku roboczym, analizuje jeden obiekt

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
DWORD WINAPI threadWorkerZiarno(LPVOID param)
#else
void* threadWorkerZiarno(void* param)
#endif
{
    ZiarnoWorkerInput* input = (ZiarnoWorkerInput*)param;
    qmazdaRun(input);

//#ifndef WINDOWS_MM
//PMS Tutaj wątek sam zwalnia swoje zasoby wejsciowe kiedy sie konczy
    delete input;
//#endif
    return 0;
}

#ifdef WINDOWS_MM
//MM
VOID CALLBACK ziarnoWorkCallback(PTP_CALLBACK_INSTANCE Instance, PVOID Parameter, PTP_WORK Work)
{
    // Instance, Parameter, and Work not used in this example.
    UNREFERENCED_PARAMETER(Instance);

    if(!SetThreadPriority(GetCurrentThread(), threadPriority))
    {
        ZiarnoWorkerInput* input = (ZiarnoWorkerInput*)Parameter;
        if(input->debuglevel&DEBUG_LOG_WARNINGS)
            systemLog(input->id, -1, ID_LOG_WARNING, "ziarnoWorkCallback: cannot set priority");
    }

    threadWorkerZiarno(Parameter);

//PMS Mozliwe, ze to ta linijka generuje problem w przypadku, w ktorym to watek mialby zwalniac swoje zasoby
//    delete Parameter;
    CloseThreadpoolWork(Work);
    return;
}
#endif


//-----------------------------------------------------------------------------
// IMPLEMENTACJA INTERFEJSU
//-----------------------------------------------------------------------------

API_ZIARNO void setSystemLogCallback(SystemLogCallback _systemLog)
{
    systemLog = _systemLog;
}

API_ZIARNO void setDebugImageCallback(DebugImageCallback _debugImage)
{
    debugImage = _debugImage;
}

API_ZIARNO void setParamCallback(GetParamCallback _paramCallback)
{
//    getParam = _paramCallback;
}

API_ZIARNO void setFinishRun(FinishRun _onFinish)
{
    finishRun = _onFinish;
}
#ifdef WINDOWS_MM
VOID CALLBACK cleanGroupCallback(void* ObjectContext, void* CleanupContext)
{
//    ZiarnoWorkerInput* input = (ZiarnoWorkerInput*)ObjectContext;
//    if (input != NULL)
//    {
//        if (systemLog!=NULL)
//            systemLog(input->id, -1, ID_LOG_ERROR, "Worker canceled.");
//// PMS: W wersji bez thread-pullingu dane sa zwalniane przez watek
//        delete input;
//    }
}
#endif


API_ZIARNO void setDebugLevelForInitialization(int debugLevel)
{
    qmazdaSetDebugLevel(debugLevel);
}

API_ZIARNO bool Init(const char *configfile, int thread_priority)
{
    threadPriority = thread_priority;
    strcpy(configuration_filename, configuration_filename_def);
    if(configfile != NULL)
        if(strlen(configfile) < 2046)
            strcpy(configuration_filename, configfile);

    std::stringstream ss;
    ss << "Init: [filenames] ";
    ss << configuration_filename;
    ss << " [priority] " << threadPriority;
    systemLog(-1, -1, ID_LOG_INFO, ss.str().c_str());
    qmazdaData = qmazdaInit(configuration_filename);

    if(qmazdaData == NULL) return false;
    InitializeZiarnaMutex;
#ifdef WINDOWS_MM
    initThradPool();
#endif
    return true;
}


API_ZIARNO int SetDiskImages(Image** imagetab, int cameras, int objects)
{
    return initializeBackgrounds(imagetab, cameras, objects);
//    if(imagetab == 0)
//    {
//        systemLog(-1, -1, ID_LOG_ERROR, "SetDiskImages: Table null pointer.");
//        return -1;
//    }
//    for(int o = 0; o < objects; o++)
//    {
//        for(int c = 0; c < cameras; c++)
//        {
//            if(imagetab[o*cameras+c] == NULL)
//            {
//                systemLog(-1, -1, ID_LOG_ERROR, "SetDiskImages: Image null pointer.");
//                return -1;
//            }
//            systemLog(-1, -1, ID_LOG_ERROR, "SetDiskImages: Image null pointer.");
//            //if(c == 0) debugImage(o, c, "SetDiskImages", imagetab[o*cameras+c]);
//        }
//        std::stringstream ss;
//        ss << "SetDiskImages: [Sizes&Channels] ";
//        for(int c = 0; c < cameras; c++)
//        {
//            ss << imagetab[o*cameras+c]->width << "x" << imagetab[o*cameras+c]->width << " ";
//            if(imagetab[o*cameras+c]->nChannels >= 3) ss << " K ";
//            else ss << " G ";
//        }
//        systemLog(o, -1, ID_LOG_INFO, ss.str().c_str());
//    }
//    return cameras*objects;
}






API_ZIARNO bool RunAsync(int id, int debuglevel, Image **tabInImg, int numInImg)
{
    if (numInImg < 2)
    {
        if(debuglevel&DEBUG_LOG_ERRORS) systemLog(id, -1, ID_LOG_ERROR, "RunAsync: Incorrect number of images.");
        return false;
    }
// Ustawianie wskaznikow do parametrow i wskaznikow wykorzystywanych przez watki
    ZiarnoWorkerInput* input = new ZiarnoWorkerInput;
    input->debuglevel = debuglevel;
    input->id = id;
    input->qmazdaSetup = qmazdaData; //Wskaznik ustawien zwrocony przez funkcje qmazdaInit()

    for(int z = 0; z < numInImg; z++) input->tabInImg.push_back(tabInImg[z]);
#ifndef WINDOWS_MM
    ThreadHandler thread;
#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
    thread = CreateThread(NULL, 0, threadWorkerZiarno, (LPVOID)input, 0, NULL);
    if (thread == NULL)
#else
    if (pthread_create(&thread, NULL, threadWorkerZiarno, input) != 0)
#endif
    {
        if(debuglevel&DEBUG_LOG_ERRORS) systemLog(id, -1, ID_LOG_ERROR, "RunAsync: Cannot create thread.");
        return false;
    }
//	else
//	{
//	}
    threadZiarnoList.push_back(thread);
//	ziarnoWorkerInputList.push_back(input);
#else
    if (pool != NULL)
    {
        PTP_WORK work = NULL;
        work = CreateThreadpoolWork(ziarnoWorkCallback,(LPVOID)input, &cbe);
        if (work != NULL)
        {
            SubmitThreadpoolWork(work);
        }
        else
        {
// PMS: Dane zwalniane jesli watek nie ruszy i sam tego nie zrobi (OK w obu przypadkach)
            delete input;
            if(debuglevel&DEBUG_LOG_ERRORS) systemLog(id, -1, ID_LOG_ERROR, "RunAsync-Error.");
        }
    }
#endif
    if(debuglevel&DEBUG_LOG_INFO) systemLog(id, -1, ID_LOG_INFO, "RunAsync: Thread up and running.");
    return true;
}

API_ZIARNO void End(bool _wait)
{
#ifdef WINDOWS_MM
    if (pool == NULL) return;
#endif
    if (_wait)
    {
        systemLog(-1, -1, ID_LOG_INFO, "End: Waiting for threads.");
#ifndef WINDOWS_MM
        for (unsigned int i = 0; i < threadZiarnoList.size(); i++)
        {
#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
            if (WaitForSingleObject(threadZiarnoList[i], INFINITE) != WAIT_OBJECT_0)
#else
            if (pthread_join(threadZiarnoList[i], NULL) != 0)
#endif
                systemLog(i, -1, ID_LOG_ERROR, "Thread join error.");
// Zwalnianie zasobow wejsciowych teraz robione w watku
//			else
//			{
//				if (ziarnoWorkerInputList[i] != NULL) delete ziarnoWorkerInputList[i];
//				ziarnoWorkerInputList[i] = NULL;
//			}
        }
#else
        CloseThreadpoolCleanupGroupMembers(cleanupGroup, FALSE, NULL);
#endif
    }
    else
    {
        systemLog(-1, -1, ID_LOG_INFO, "End: Starts canceling threads.");
#ifndef WINDOWS_MM
        for (unsigned int i = 0; i < threadZiarnoList.size(); i++)
        {
#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
            if (TerminateThread(threadZiarnoList[i], 0) != 0)
#else
            if (pthread_cancel(threadZiarnoList[i]) != 0)
#endif
            {
                systemLog(i, -1, ID_LOG_ERROR, "Thread cancel error.");
            }
            else
            {
#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
                CloseHandle(threadZiarnoList[i]);
#endif
// Zwalnianie zasobow wejsciowych teraz robione sa w watku.
// Problem w tym, ze jesli watek zostaje zabity to zasoby moga nie byc zwolnione
// Dlatego nie zaleca sie wywolywania funkcji End z parametrem false: End(false)
//				if (ziarnoWorkerInputList[i] != NULL) delete ziarnoWorkerInputList[i];
//				ziarnoWorkerInputList[i] = NULL;
            }
        }
#else
        CloseThreadpoolCleanupGroupMembers(cleanupGroup, TRUE, NULL);
#endif
    }
    CloseZiarnaMutex;
    systemLog(-1, -1, ID_LOG_INFO, "End: Threads ended.");
#ifndef WINDOWS_MM
    threadZiarnoList.clear();
#else
    closeThreadPool();
#endif
    qmazdaEnd();
    systemLog(-1, -1, ID_LOG_INFO, "End: Job finalized.");
}


