#include "sideheight.h"
#include "segment.h"

const unsigned char threshold = 128;
//const int first_line = 200;
//const int cntr_line  = 530;
//const int last_line  = 800;

bool findGrainTopBottom(Image *sideImage, int minmax[2]);

double findWaistFromContour(CvSeq* incontour, int leftright[2])
{
    CvMemStorage* hullStorage = cvCreateMemStorage(0);
    CvSeq *contour = cvConvexHull2(incontour, hullStorage, CV_CLOCKWISE, 1);

    int imax = contour->total;
    if(imax <= 0) return 0;
    double minnn = -1;

    int minx = 30000;
    int maxx = -1;

    for(int i = 0; i < imax; i++)
    {
        double maxnn = 0;
        unsigned int j = (i+1) % imax;
        CvPoint* ppi = (CvPoint*) cvGetSeqElem(contour, i);
        CvPoint* ppj = (CvPoint*) cvGetSeqElem(contour, j);

        if(minx > ppi->x) minx = ppi->x;
        if(maxx < ppi->x) maxx = ppi->x;

        double xn = ppi->y - ppj->y;
        double yn = ppj->x - ppi->x;
        double nn = sqrt(xn*xn + yn*yn);
        xn /= nn;
        yn /= nn;

        for(int k = 0; k < imax; k++)
        {
            CvPoint* ppk = (CvPoint*) cvGetSeqElem(contour, k);
            nn = xn*(ppk->x - ppi->x) + yn*(ppk->y - ppi->y);
            nn = fabs(nn);
            if(maxnn < nn) maxnn = nn;
        }
        if(minnn < 0 || minnn > maxnn) minnn = maxnn;
    }

    leftright[0] = minx;
    leftright[1] = maxx;
    cvReleaseMemStorage(&hullStorage);
    return minnn;
}

double findGrainHeigh(Image *sideImage, bool debug, unsigned int* warning)
{
    int minmax[3];
    if(! findGrainTopBottom(sideImage, minmax)) return -1;

    Image* bf = cvCreateImage(cvSize(sideImage->width, minmax[1] - minmax[0]), IPL_DEPTH_8U, 1);

    int maxX = bf->width;
    int maxY = bf->height;
    int chan = sideImage->nChannels;
    int step = sideImage->widthStep;
    int bstep = bf->widthStep;

    int green;
    green = chan > 1 ? 1 : 0;

    for(int y = 0; y < maxY; y++)
    {
        unsigned char* sr = ((unsigned char *)sideImage->imageData + (size_t)(y+minmax[0]) * step + green);
        unsigned char* ds = ((unsigned char *)bf->imageData + (size_t)y * bstep);
        unsigned char* maxDs = ds + (size_t)maxX;
        for(; ds < maxDs; ds++, sr+=chan)
        {
            if(*sr > threshold) *ds = 0;
            else *ds = 255;
        }
    }
    CvMemStorage* storage = cvCreateMemStorage(0);
    CvSeq* contours = NULL;
    CvSeq* contour = NULL;
    cvFindContours(bf, storage, &contours, sizeof(CvContour), CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cvPoint(0,0));

    unsigned int concount = 0;
    double area = segmentGetMaxAreaContour(contours, &contour, &concount);
    if(area < 1 || contour->total < 6)
    {
        cvReleaseMemStorage(&storage);
        cvReleaseImage(&bf);
        return -1;
    }

    if(concount > 1)
        (*warning) |= segmentation_warning_multiple;
    if(concount < 1)
        (*warning) |= segmentation_warning_noobject;

    int leftright[2];
    double width = findWaistFromContour(contour, leftright);

    if(leftright[0] <= 2 || (leftright[1]+4) > sideImage->width)
        (*warning) |= segmentation_warning_outframe;

    if(debug)
    {
        cvDrawContours(sideImage, contour, cvScalar(128, 0, 255), cvScalar(128, 0, 255), 1, 3, 8, cvPoint(0, minmax[0]));
        cvDrawLine(sideImage, cvPoint(0, minmax[0]), cvPoint(maxX-1, minmax[0]), cvScalar(0, 0, 255), 3);
        cvDrawLine(sideImage, cvPoint(0, minmax[1]), cvPoint(maxX-1, minmax[1]), cvScalar(128, 64, 255), 3);
        cvDrawLine(sideImage, cvPoint(0, minmax[2]), cvPoint(maxX-1, minmax[2]), cvScalar(255, 255, 0), 3);
    }

    cvReleaseMemStorage(&storage);
    cvReleaseImage(&bf);
    return width;
}

bool findGrainTopBottom(Image *sideImage, int minmax[3])
{
    int blackCount;

    int maxX = sideImage->width;
    int maxY = sideImage->height;
    minmax[0] = 0;
    minmax[1] = maxY;
    minmax[2] = maxY;
    if(maxX < 128 || maxY < 128)
        return false;
    int chan = sideImage->nChannels;
    int step = sideImage->widthStep;
    int frst = (chan > 1 ? 1 : 0);

    int y;
    for(y = 0; y < maxY; y++)
    {
        unsigned char* ptr = ((unsigned char *)sideImage->imageData + (size_t)y * step);
        unsigned char* maxPtr = ptr + (size_t)maxX*chan;
        ptr += frst;
        blackCount = 0;
        for(; ptr < maxPtr; ptr += chan)
        {
            if(*ptr < threshold)
                blackCount++;
        }
        if(blackCount > 1)
        {
            minmax[0] = y;
            break;
        }
    }

    if(y >= maxY)
        return false;

    for(y = maxY-1; y > minmax[0]; y--)
    {
        unsigned char* ptr = ((unsigned char *)sideImage->imageData + (size_t)y * step);
        unsigned char* maxPtr = ptr + (size_t)maxX*chan;
        ptr += frst;
        blackCount = 0;
        for(; ptr < maxPtr; ptr += chan)
        {
            if(*ptr >= threshold)
                blackCount++;
        }
        if(blackCount > 1)
        {
            minmax[2] = y;
            break;
        }
    }

    if(y <= minmax[0])
        return false;

    int blackCountMin = maxX;
    int minYm = (2 * minmax[0] + minmax[2]) / 3;
    int minYb = minmax[2] - 80;
    if(minYb < minYm) minYb = minYm;

    for(y = minmax[2]; y > minYb || (y > minYm && y > 2 * minmax[1] - minmax[2]); y--)
    {
        unsigned char* ptr = ((unsigned char *)sideImage->imageData + (size_t)y * step);
        unsigned char* maxPtr = ptr + (size_t)maxX*chan;
        ptr += frst;
        blackCount = 0;
        for(; ptr < maxPtr; ptr += chan)
        {
            if(*ptr < threshold)
                blackCount++;
        }
        if(blackCount < blackCountMin)
        {
            blackCountMin = blackCount;
            minmax[1] = y;
        }
    }
    return true;
}



////--------------------------------------------------------------------------------------------------
//bool findGrainTopBottom(Image *sideImage, int threshold, int minmax[2])
//{
//    int maxX = sideImage->width;
//    int maxY = sideImage->height;

//    int maxXM1 = maxX-1;

//    unsigned char *wImBGR = (unsigned char*)sideImage->imageData;
//    int *BlacLineLengths = new int[maxY];
//    int firstLine = -1;
//    int firstBlackPixelOnEdgePosL = -1;
//    int firstBlackPixelOnEdgePosR = -1;
//    bool firstLineFound = false;

//    if(sideImage->nChannels >= 3)
//    {
//        for (int y = 0; y < maxY; y++)
//        {
//            int sumLineBlackPixels = 0;
//            for (int x = 0; x < maxX; x++)
//            {
//                float brightness ;
//                brightness = (int)*wImBGR * 114;
//                wImBGR++;
//                brightness += (int)*wImBGR * 587;
//                wImBGR++;
//                brightness += (int)*wImBGR * 299;
//                wImBGR++;
//                brightness /=1000;
//                if(brightness <= threshold)
//                {
//                    sumLineBlackPixels ++;
//                    if(x == 0 && firstBlackPixelOnEdgePosL < y)
//                        firstBlackPixelOnEdgePosL = y;
//                    if(x == maxXM1 && firstBlackPixelOnEdgePosR < y)
//                        firstBlackPixelOnEdgePosR = y;
//                }
//            }
//            BlacLineLengths[y] = sumLineBlackPixels;
//            if(sumLineBlackPixels && !firstLineFound)
//            {
//                firstLine = y;
//                firstLineFound = true;
//            }
//            if(firstBlackPixelOnEdgePosL >= 0 && firstBlackPixelOnEdgePosR >= 0)
//            {
//                break;
//            }
//        }
//    }
//    else
//    {
//        for (int y = 0; y < maxY; y++)
//        {
//            int sumLineBlackPixels = 0;
//            for (int x = 0; x < maxX; x++)
//            {
//                if(*wImBGR <= threshold)
//                {
//                    sumLineBlackPixels ++;
//                    if(x == 0 && firstBlackPixelOnEdgePosL < y)
//                        firstBlackPixelOnEdgePosL = y;
//                    if(x == maxXM1 && firstBlackPixelOnEdgePosR < y)
//                        firstBlackPixelOnEdgePosR = y;
//                }
//                wImBGR++;
//            }
//            BlacLineLengths[y] = sumLineBlackPixels;
//            if(sumLineBlackPixels && !firstLineFound)
//            {
//                firstLine = y;
//                firstLineFound = true;
//            }
//            if(firstBlackPixelOnEdgePosL >= 0 && firstBlackPixelOnEdgePosR >= 0)
//            {
//                break;
//            }
//        }
//    }
//    int firstBlackPixelOnEdgePos = firstBlackPixelOnEdgePosL;
//    if(firstBlackPixelOnEdgePos < firstBlackPixelOnEdgePosR)
//        firstBlackPixelOnEdgePos = firstBlackPixelOnEdgePosR;

//    if(!firstLineFound)
//        return false;
//    if(firstBlackPixelOnEdgePos == -1)
//        return false;
//    int start = firstBlackPixelOnEdgePos - (firstBlackPixelOnEdgePos - firstLine) * 2 / 3;
//    int minLineLength = maxX;
//    int minLinePosition = 0;
//    for(int y = start; y<firstBlackPixelOnEdgePos; y++)
//    {
//        if(minLineLength > BlacLineLengths[y])
//        {
//            minLineLength = BlacLineLengths[y];
//            minLinePosition = y;
//        }
//    }
//    delete[] BlacLineLengths;

//    minmax[0] = firstLine;
//    minmax[1] = minLinePosition;
//    return true;
//}
