#include "dectree.h"
#include "../../qmazda/LdaPlugin/ldaselection.h"
#include "../../qmazda/VschPlugin/vschselection.h"
#include "../../qmazda/SvmPlugin/svmselection.h"
#include "../../qmazda/MzGenerator/featureio.h"

DecisionTree::DecisionTree():ParameterTree <DecisionTreeElement>()
{
}

int DecisionTree::decisionTreeClassificationFollow(ParameterTreeItem<DecisionTreeElement>* item, std::vector<MIType2DRGB*>* output, std::vector<MzRoi2D*>* outroi, double* values)
{
    unsigned int c;
    DecisionTreeElement* ek = & item->data;
    if(ek->classifier == NULL || ek->classLut == NULL)
        return -1;

    for(c = 0; c < maxNumberOfTrees; c++)
    {
        if(ek->featureComputationObject[c] == NULL)
            continue;
        StubItemDataEntryRoi genData;
        genData.image = (*output)[c];
        genData.isRGB = true;
        genData.roi = (*outroi)[c];
        genData.table = values;
        ek->featureComputationObject[c]->Execute(&genData);
    }


//    {
//        std::cout << "Vals:";
//        for(unsigned int i = 9; i < 15; i++)
//             std::cout << " " << values[i];
//        std::cout << "\n";
//    }

    unsigned int klasa = ek->classifier->classifyFeatureVector(values);



//    std::cout << ek->fileName << " klasa = " << klasa << "\n\n";
//    fflush(stdout);


    if(klasa < item->children.size())
    {
        int nk = decisionTreeClassificationFollow(item->children[klasa], output, outroi, values);
        if(nk >= 0) return nk;
    }

    if(klasa >= ek->maxClassLut) klasa = 0;
    else klasa = ek->classLut[klasa];

    return (int)klasa;
}

int DecisionTree::decisionTreeClassification(std::vector<MIType2DRGB*>* output, std::vector<MzRoi2D*>* outroi, double* values)
{
    if(output->size() < maxNumberOfTrees) return 0;
    if(outroi->size() < maxNumberOfTrees) return 0;

    int klasa = decisionTreeClassificationFollow(getRoot()->children[0], output, outroi, values);
    if(klasa < 0) return 0;
    return klasa;
}

ClassifierAccessInterface* DecisionTree::loadSingleClassifier(ClassifierAccessInterface* object, const char* fileName)
{
    if(object->loadClassifierFromFile(fileName))
    {
        return object;
    }
    delete object;
    return NULL;
}

void DecisionTree::loadClassifierFeatures(DecisionTreeElement* ek, std::vector<std::string>* alreadyComputedFeatureNames)
{
    std::vector <std::string> featureNames = loadFeatureList(ek->fileName.c_str());
    std::vector <std::string> featureNamesSplit[maxNumberOfTrees];
    NameStubTree templateTree;
    StubItemBuilder::CreateTemplate(&templateTree, roiTreeTemplate, sizeof(roiTreeTemplate)/sizeof(ItemTemplate));
    char prefix[4];
    prefix[1] = '_';
    prefix[2] = 0;
    for(unsigned int i = 0; i < featureNames.size(); i++)
    {
        for(unsigned int c = 0; c < maxNumberOfTrees; c++)
        {
            prefix[0] = ('A' + c);
            if(featureNames[i].substr(0, 2) == prefix)
            {
                featureNamesSplit[c].push_back(featureNames[i].substr(2));
            }
        }
    }
    featureNames.clear();

    for(unsigned int c = 0; c < maxNumberOfTrees; c++)
    {
        prefix[0] = ('A' + c);
        ek->featureComputationObject[c] = new StubItemBuilder(2, &templateTree);

        unsigned int required = 0;
        for(unsigned int i = 0; i < featureNamesSplit[c].size(); i++)
        {
            std::string* shortName = & featureNamesSplit[c][i];
            std::string fullName = std::string(prefix)+featureNamesSplit[c][i];

            unsigned int nac = alreadyComputedFeatureNames->size();
            unsigned int ac;
            for(ac = 0; ac < nac; ac++)
            {
                if(fullName == (*alreadyComputedFeatureNames)[ac]) break;
            }
            if(ac < nac) continue; //cecha fullName na pewno powinna byc juz obliczona

            nac = allFeatureNames.size();
            for(ac = 0; ac < nac; ac++)
            {
                if(fullName == allFeatureNames[ac]) break;
            }
            if(ek->featureComputationObject[c]->addFeature(*shortName, ac, NULL))
            {
                alreadyComputedFeatureNames->push_back(fullName);
#ifdef STORE_FEATURE_NAMES
                ek->computationObjectFeature[c].push_back(fullName);
#endif
                if(ac >= nac) allFeatureNames.push_back(fullName);
                required++;
            }
        }
        if(required <= 0)
        {
            delete ek->featureComputationObject[c];
            ek->featureComputationObject[c] = NULL;
        }
        featureNamesSplit[c].clear();
    }
}

int DecisionTree::loadClassifiersRecursive(ParameterTreeItem<DecisionTreeElement>* item,
                              std::vector<std::string>* alreadyComputedFeatureNames,
                              std::vector <std::string>* allClassNames)
{


    DecisionTreeElement* ek = & item->data;
    if(ek->classifier != NULL) delete ek->classifier;
    ek->classifier = NULL;
    if(ek->classLut != NULL) delete[] ek->classLut;
    ek->classLut = NULL;

    if(ek->fileName.empty())
    {
        return 0;
    }
    if(ek->fileName == "!")
        return 0;

//    std::cout << "ClassesIN:\n";
//    for(int i = 0; i < allClassNames.size(); i++)
//        std::cout << allClassNames[i] <<" ";
//    std::cout << "\n\n";

    if(ek->classifier == NULL)
        ek->classifier = loadSingleClassifier(dynamic_cast<ClassifierAccessInterface*>(new LdaSelectionReduction()), ek->fileName.c_str());
    if(ek->classifier == NULL)
        ek->classifier = loadSingleClassifier(dynamic_cast<ClassifierAccessInterface*>(new VschSelectionReduction()), ek->fileName.c_str());
    if(ek->classifier == NULL)
        ek->classifier = loadSingleClassifier(dynamic_cast<ClassifierAccessInterface*>(new SvmSelectionReduction()), ek->fileName.c_str());

    if(ek->classifier == NULL)
    {
        errorMessage = ek->fileName.c_str();
        return -1;
    }


//    std::cout << ek->fileName << "\n";
    loadClassifierFeatures(ek, alreadyComputedFeatureNames);
    unsigned int nac = alreadyComputedFeatureNames->size();

//    std::cout << "\n\n";


//        std::cout << ek->fileName << " All: ";
//        for(int i = 0; i < allFeatureNames.size(); i++)
//            std::cout << allFeatureNames[i] <<" ";
//        std::cout << " ";

//        std::cout << "Loc: ";
//        for(int i = 0; i < alreadyComputedFeatureNames->size(); i++)
//            std::cout << (*alreadyComputedFeatureNames)[i] <<" ";
//        std::cout << "\n";

    if(! ek->classifier->configureForClassification(&allFeatureNames))
    {
        unsigned int ii, i;
        std::vector <std::string> aa = ek->classifier->getFeatureNames();
        for(i = 0; i < aa.size(); i++)
        {
            for(ii = 0; ii < allFeatureNames.size(); ii++)
            {
                if(allFeatureNames[ii] == aa[i])
                    break;
            }
            if(ii >= allFeatureNames.size())
                errorMessage = std::string("Unrecognized ") + aa[i];
        }

//        std::cout << "Classifier: ";
//        std::vector <std::string> aa = ek->classifier->getFeatureNames();
//        for(int i = 0; i < aa.size(); i++)
//            std::cout << aa[i] <<" ";
//        std::cout << "\n";

//        std::cout << "allFeatureNames: ";
//        for(int i = 0; i < allFeatureNames.size(); i++)
//            std::cout << allFeatureNames[i] <<" ";
//        std::cout << "\n";

//        fflush(stdout);

        return -2;
    }

    std::vector<std::string> newClassNames = ek->classifier->getClassNames();
    unsigned int nc = newClassNames.size();

    ek->classLut = new unsigned int[nc];
    ek->maxClassLut = nc;


    unsigned int nb = item->children.size();
    unsigned int ncb = nc;
    if(nb < nc) ncb = nb;

    int ret = 0;
    unsigned int c = 0;

    for(; c < ncb ; c++)
    {
        unsigned int na = allClassNames->size();
        unsigned int a;

        for(a = 0; a < na; a++)
        {
            if((*allClassNames)[a] == newClassNames[c]) break;
        }

        if(a >= na)
        {
            allClassNames->push_back(newClassNames[c]);

        }
        ret++;

        int added_leafs = loadClassifiersRecursive(item->children[c], alreadyComputedFeatureNames, allClassNames);
        if(added_leafs < 0)
        {
            return added_leafs;
        }

        alreadyComputedFeatureNames->resize(nac);

        if(a >= na && (allClassNames->size() > na+1 || added_leafs > 0))
            allClassNames->erase(allClassNames->begin()+na);
    }

    for(; c < nc ; c++)
    {
        unsigned int na = allClassNames->size();
        unsigned int a;
        for(a = 0; a < na; a++)
        {
            if((*allClassNames)[a] == newClassNames[c]) break;
        }
        if(a >= na)
        {
            allClassNames->push_back(newClassNames[c]);
        }
        ret++;
    }
//    std::cout << "ClassesOUT:\n";
//    for(int i = 0; i < allClassNames.size(); i++)
//        std::cout << allClassNames[i] <<" ";
//    std::cout << "\n\n";

//    for(; c < nb; c++)
//    {
//        if(! loadClassifiersRecursive(item->children[c], alreadyComputedFeatureNames, allClassNames))
//            ret = false;
//        alreadyComputedFeatureNames->resize(nac);
//    }

    if(c < nb)
    {
        errorMessage = item->children[c]->data.fileName;
    }

    return ret;
}


bool DecisionTree::loadClassifiers(std::vector <std::string>* allClassNames)
{
    unsigned int ni = getRoot()->children.size();
    std::vector<std::string> alreadyComputedFeatureNames;

    bool ret = true;
    for(unsigned int i = 0; i < ni; i++)
    {
        int r = loadClassifiersRecursive(getRoot()->children[i], &alreadyComputedFeatureNames, allClassNames);
        if(r < 0)
            ret = false;
        alreadyComputedFeatureNames.clear();
    }

    for(std::vector<std::string>::iterator it = allClassNames->begin() ; it != allClassNames->end(); ++it)
    {
        if(*it == "!")
        {
            allClassNames->erase(it);
            break;
        }
    }

//    for(unsigned int c = 0; c < allClassNames->size(); c++)
//    {
//        if((*allClassNames)[c] == "!")
//            allClassNames->erase(allClassNames->begin() + c);
//    }

    return ret;
}

bool DecisionTree::renumberClassesRecursive(ParameterTreeItem<DecisionTreeElement>* item, std::vector <std::string>* allClassNames)
{
    DecisionTreeElement* ek = & item->data;
    if(ek->classifier == NULL || ek->classLut == NULL) return true;

//    if(! ek->classifier->configureForClassification(&allFeatureNames))
//        return false;

    std::vector<std::string> newClassNames = ek->classifier->getClassNames();
    unsigned int nc = newClassNames.size();
    if(nc > ek->maxClassLut) nc = ek->maxClassLut;

    unsigned int c;

    for(c = 0; c < nc ; c++)
    {
        unsigned int na = allClassNames->size();
        unsigned int a;
        ek->classLut[c] = 0;
        for(a = 0; a < na; a++)
        {
            if((*allClassNames)[a] == newClassNames[c]) break;
        }
        if(a < na)
        {
            ek->classLut[c] = a;
        }
    }

    bool ret = true;
    unsigned int nb = item->children.size();

    for(c = 0; c < nb; c++)
    {
        if(! renumberClassesRecursive(item->children[c], allClassNames))
            ret = false;
    }
    return ret;
}


bool DecisionTree::renumberClasses(std::vector <std::string>* allClassNames)
{
    unsigned int ni = getRoot()->children.size();
    bool ret = true;
    for(unsigned int i = 0; i < ni; i++)
    {
        if(! renumberClassesRecursive(getRoot()->children[i], allClassNames)) ret = false;
    }
    return ret;
}
