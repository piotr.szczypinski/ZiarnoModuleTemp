#ifndef SIDEHRIGHT_H
#define SIDEHRIGHT_H

#include <opencv2/core/core_c.h>
#include "int_ziarno.h"

double findGrainHeigh(Image* sideImage, bool debug, unsigned int *warning);

#endif // SIDEHRIGHT_H
