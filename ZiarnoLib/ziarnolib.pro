TEMPLATE = lib

CONFIG += staticlib
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
#CONFIG += c++11

DESTDIR         = ../Executables



DEFINES += MAZDA_IMAGE_8BIT_PIXEL
DEFINES += MAZDA_CONVERT_BGR_IMAGE
# DEFINES += USE_DEBUG_LOG
INCLUDEPATH += ../../qmazda/SharedImage/
# DEFINES += ZIARNOLIB_SHARED_COMPILATION

SOURCES +=  \
    lib_ziarno.cpp \
    ../../qmazda/LdaPlugin/ldaselection.cpp \
    ../../qmazda/VschPlugin/vschselection.cpp \
    ../../qmazda/MzGenerator/templates.cpp \
    ../../qmazda/MzGenerator/stubitemwindow.cpp \
    ../../qmazda/MzGenerator/stubitemshape.cpp \
    ../../qmazda/MzGenerator/stubitemnormalization.cpp \
    ../../qmazda/MzGenerator/stubitemhistogram.cpp \
    ../../qmazda/MzGenerator/stubitemgrlm.cpp \
    ../../qmazda/MzGenerator/stubitemgradient.cpp \
    ../../qmazda/MzGenerator/stubitemglcm.cpp \
    ../../qmazda/MzGenerator/stubitemgabor.cpp \
    ../../qmazda/MzGenerator/stubitemcolortransform.cpp \
    ../../qmazda/MzGenerator/stubitemarm.cpp \
    ../../qmazda/MzGenerator/stubitem.cpp \
    ../../qmazda/MzGenerator/featureio.cpp \
    ../../qmazda/MzGenerator/stubitemshapemz.cpp \
    ../../qmazda/MzGenerator/stubitemshapecv.cpp \
    ../../qmazda/MzGenerator/stubitemlbp.cpp \
    ../../qmazda/MzGenerator/stubitemhog.cpp \
    ../../qmazda/MzGenerator/stubitemhaar.cpp \
    ../../qmazda/MzShared/classifierio.cpp \
    ../../qmazda/MzShared/parametertreetemplate.cpp \
    ../../qmazda/MzShared/multidimselection.cpp \
    ../../qmazda/MzShared/dataforconsole.cpp \
    ../../qmazda/SvmPlugin/svmselection.cpp \
    ../../qmazda/SvmPlugin/libsvm/svm.cpp \
    ../../qmazda/MzShared/csvio.cpp \
    dectree.cpp \
    segment.cpp \
    sideheight.cpp

HEADERS += \
    int_ziarno.h \
    lib_ziarno.h \
    ../../qmazda/LdaPlugin/ldaselection.h \
    ../../qmazda/VschPlugin/vschselection.h \
    ../../qmazda/MzGenerator/templates.h \
    ../../qmazda/MzGenerator/stubitemwindow.h \
    ../../qmazda/MzGenerator/stubitemshape.h \
    ../../qmazda/MzGenerator/stubitemshapemz.h \
    ../../qmazda/MzGenerator/stubitemnormalization.h \
    ../../qmazda/MzGenerator/stubitemhistogram.h \
    ../../qmazda/MzGenerator/stubitemgrlm.h \
    ../../qmazda/MzGenerator/stubitemgradient.h \
    ../../qmazda/MzGenerator/stubitemglcm.h \
    ../../qmazda/MzGenerator/stubitemgabor.h \
    ../../qmazda/MzGenerator/stubitemcolortransform.h \
    ../../qmazda/MzGenerator/stubitemarm.h \
    ../../qmazda/MzGenerator/stubitem.h \
    ../../qmazda/MzGenerator/featureio.h \
    ../../qmazda/SvmPlugin/svmselection.h \
    ../../qmazda/SvmPlugin/libsvm/svm.h \
    dectree.h \
    segment.h \
    sideheight.h


#unix{
#    LIBS += -lopencv_core
#    LIBS += -lopencv_imgproc
#    LIBS += -lopencv_highgui
#    LIBS += -lopencv_legacy
#}
#else:win32{
#    include(../../qmazda/Pri/opencv.pri)
#}

include(../../qmazda/Pri/config.pri)
include(../../qmazda/Pri/alglib.pri)
include(../../qmazda/Pri/qhull.pri)
include(../../qmazda/Pri/itk.pri)
include(../../qmazda/Pri/opencv.pri)
#include(../../qmazda/Pri/tiff.pri)
