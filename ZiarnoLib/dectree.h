#ifndef DECTREE_H
#define DECTREE_H

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include "../MzShared/classifierio.h"
#include "../../qmazda/MzGenerator/templates.h"

#define STORE_FEATURE_NAMES
const int maxNumberOfTrees = 2;

class DecisionTreeElement
{
public:
    DecisionTreeElement()
    {
        maxClassLut = 0;
        classifier = NULL;
        classLut = NULL;
        for(int i = 0; i < maxNumberOfTrees; i++)
            featureComputationObject[i] = NULL;
    }
    ~DecisionTreeElement()
    {
        if(classifier != NULL) delete classifier;
        if(classLut != NULL) delete[] classLut;
        for(int i = 0; i < maxNumberOfTrees; i++)
            if(featureComputationObject[i] != NULL)
                delete featureComputationObject[i];
    }
    std::string toString(void)
    {
        std::stringstream ss;
        ss << fileName << " ";
//        << " " << (featureComputationObject[0]==NULL ? "-" : "A")
//                << (featureComputationObject[1]==NULL ? "-" : "B")<< (classifier==NULL ? "-" : "C")
//                << (classLut==NULL ? "-" : "T") << (classLut==NULL ? 0 : maxClassLut);
        if(featureComputationObject[0] != NULL)
        {
            ss << "A";
#ifdef STORE_FEATURE_NAMES
            ss << ": ";
            for(unsigned int i = 0; i < computationObjectFeature[0].size(); i++)
                ss << computationObjectFeature[0][i] <<" ";
#endif
        }

        if(featureComputationObject[1] != NULL)
        {
            ss << "B";
#ifdef STORE_FEATURE_NAMES
            ss << ": ";
            for(unsigned int i = 0; i < computationObjectFeature[1].size(); i++)
                ss << computationObjectFeature[1][i] <<" ";
#endif
        }

        if(classifier != NULL)
        {
            std::vector<std::string> cnames = classifier->getClassNames();
            ss << "C: ";
            for(unsigned int i = 0; i < cnames.size(); i++)
                ss << cnames[i] <<" ";
        }

        return ss.str();
    }
    void fromString(std::string v)
    {
        fileName = v;
    }
    std::string fileName;

    StubItemBuilder* featureComputationObject[maxNumberOfTrees];

#ifdef STORE_FEATURE_NAMES
    std::vector<std::string> computationObjectFeature[maxNumberOfTrees];
#endif

    ClassifierAccessInterface* classifier;
    unsigned int* classLut;
    unsigned int maxClassLut;

};


class DecisionTree : public ParameterTree <DecisionTreeElement>
{
public:
    DecisionTree();//:ParameterTree <DecisionTreeElement>(){}

    int decisionTreeClassification(std::vector<MIType2DRGB*>* output,
                                   std::vector<MzRoi2D*>* outroi,
                                   double* values);

    bool loadClassifiers(std::vector<string> *allClassNames);

    bool renumberClasses(std::vector<string> *allClassNames);

    std::vector <std::string> allFeatureNames;
//    std::vector <std::string> allClassNames;

    std::string errorMessage;
private:
    int decisionTreeClassificationFollow(ParameterTreeItem<DecisionTreeElement>* item,
                                            std::vector<MIType2DRGB*>* output,
                                            std::vector<MzRoi2D*>* outroi, double* values);

    bool renumberClassesRecursive(ParameterTreeItem<DecisionTreeElement>* item, std::vector<string> *allClassNames);

    int loadClassifiersRecursive(ParameterTreeItem<DecisionTreeElement>* item,
                                  std::vector<std::string>* alreadyComputedFeatureNames, std::vector<string> *allClassNames);

    void loadClassifierFeatures(DecisionTreeElement* ek,
                                std::vector<std::string>* alreadyComputedFeatureNames);

    ClassifierAccessInterface* loadSingleClassifier(ClassifierAccessInterface* object,
                                                    const char* fileName);

};

#endif // DECTREE_H
