/*
* ziarno.cpp - implementacja metod modulu analizy ziaren jeczmienia
* Opcjonalna obsluga watkow w Windows API i POSIX
*
* Autor:  (2016-2017) Piotr M. Szczypinski, Politechnika Lodzka
*
* Opracowano w ramach grantu: NCBR PBS3/A8/38/2015
* Opracowanie przemyslowej metody automatycznej oceny parametrow
* technologicznych i klasyfikacji ziarna z zastosowaniem analizy
* obrazow.
*/
#include <stdio.h>
#include "lib_ziarno_mk.h"
#include <iostream>
#include <opencv2/highgui/highgui_c.h>
#include <opencv2/imgproc/imgproc_c.h>
#include <list>

#include "../../qmazda/SharedImage/mazdaimage.h"
#include "../../qmazda/SharedImage/mazdaimageio.h"
#include "../Segmentacja/SegmentGrainImage.h"
#include "../Segmentacja/heightfromsideimage.h"
#include "../../qmazda/MzGenerator/featureio.h"
#include "dectree.h"


int defaultDebugLevel = DEBUG_LOG_ERRORS;
FinishRun finishRun = NULL;
DebugImageCallback debugImage = NULL;
SystemLogCallback systemLog = NULL;

void notify(void){} //Required by the generator module

struct QmazdaSetup
{
    DecisionTree* kaskadaCreaseUp;
    DecisionTree* kaskadaCreaseDn;
    std::vector <std::string>* allClassNames;
};
QmazdaSetup qmazdaSetup;


double top_dpmm = 61.14;
double bottom_dpmm = 62.88;
double side_dpmm = 75.63;
double strainer_min = 2.2;
double strainer_max = 3.8;

double top_pivot_xo = 51.75;
double top_pivot_yo = -130.08;
double bottom_pivot_xo = -48.91;
double bottom_pivot_yo = -131.18;
double side_to_pivot = 253.8;
double top_to_disk = 80;
double bottom_to_disk = 80;

double dcentertop;
double dcenterbot;



//extern cv::Mat BackgroundImages[];

/**
 * @brief GetBackgroundImages pobiera obrazy tla
 * @param background
 */
void GetBackgroundImages(std::vector<cv::Mat*>* background)
{
    for(int c = 0; c < maxNumberOfBackgrounds; c++)
        background->push_back(segmentBackgroundImages[c]);
}

/**
 * @brief SetBackgroundImages
 * @param background
 * @return
 */
bool SetBackgroundImages(std::vector<cv::Mat*>* background)
{
    if(maxNumberOfBackgrounds != background->size()) return false;
    for(int c = 0; c < maxNumberOfBackgrounds; c++)
    {
        if(segmentBackgroundImages[c] == NULL) segmentBackgroundImages[c] = new cv::Mat;
        *segmentBackgroundImages[c] = *(*background)[c];
    }
    return true;
}




bool loadParameters(istream* file)
{
    while(true)
    {
        std::string sline;
        std::getline(*file, sline);
        std::stringstream ss(sline);
        std::string name;
        double value;

        name.clear();
        ss >> std::skipws >> name;
        ss >> std::skipws >> value;
        if(name == "%top_dpmm") top_dpmm = value;
        else if(name == "%bottom_dpmm") bottom_dpmm = value;
        else if(name == "%side_dpmm") side_dpmm = value;
        else if(name == "%strainer_min") strainer_min = value;
        else if(name == "%strainer_max") strainer_max = value;
        else if(name == "%top_pivot_xo") top_pivot_xo = value;
        else if(name == "%top_pivot_yo") top_pivot_yo = value;
        else if(name == "%bottom_pivot_xo") bottom_pivot_xo = value;
        else if(name == "%bottom_pivot_yo") bottom_pivot_yo = value;
        else if(name == "%side_to_pivot") side_to_pivot = value;
        else if(name == "%top_to_disk") top_to_disk = value;
        else if(name == "%bottom_to_disk") bottom_to_disk = value;
        else if(name == "%%") return true;
        else if(name == "") return true;
        else if(name.empty()) return true;
    }
    return true;
}


bool loadParameters(const char* fileName)
{
    std::ifstream file;
    file.open(fileName);
    if (!file.is_open()) return false;
    if (!file.good()) return false;
    return loadParameters(&file);
}

void qmazdaSetDebugLevel(int debugLevel)
{
    defaultDebugLevel = debugLevel;
}


void qmazdaRecomputeSizes(double* htop, double* hbot, double* hsid, double xtop, double ytop, double xbot, double ybot)
{
    double xtopmm = xtop/top_dpmm - top_pivot_xo;
    double ytopmm = ytop/top_dpmm - top_pivot_yo;
    double xbotmm = xbot/bottom_dpmm - bottom_pivot_xo;
    double ybotmm = ybot/bottom_dpmm - bottom_pivot_yo;
    double d = side_to_pivot - sqrt(xtopmm*xtopmm + ytopmm*ytopmm);
    double hsidt = *hsid * d / dcentertop / side_dpmm;
    d = side_to_pivot - sqrt(xbotmm*xbotmm + ybotmm*ybotmm);
    double hsidb = *hsid * d / dcenterbot / side_dpmm;
    *hsid = (hsidb + hsidt) / 2.0;
    double hsid2 = *hsid / 2.0;
    *htop = *htop * (top_to_disk - hsid2) / top_to_disk / top_dpmm;
    *hbot = *hbot * (bottom_to_disk + hsid2) / bottom_to_disk / bottom_dpmm;
}

int qmazdaSideProfileHeight(const std::vector<Image *> *input)
{
    if((*input).size() < 3) return -1000;
    cv::Mat tmpMat;
    tmpMat = cv::cvarrToMat((*input)[2]);

    int ret = FindGrainHeigh(tmpMat);
    return ret;
}

void qmazdaClearMats(std::vector<cv::Mat*>* ImOutVect)
{
    unsigned int i, ii;
    ii = ImOutVect->size();
    for(i = 0; i < ii; i++)
    {
        delete (*ImOutVect)[i];
    }
    ImOutVect->clear();
}




int qmazdaSegmentacja(const std::vector<Image *> *input, std::vector<cv::Mat*>* ImOutVect, std::vector<MIType2DRGB *> * output, std::vector<MzRoi2D *> *outroi, std::vector<TransformacjaZiarna>* transf, int* orientation_dv)
{
    bool allfastcopy = true;
    unsigned int i;
    cv::Mat tmpMat[2];
    std::vector<cv::Mat*> ImInPVect;


    if((*input).size() < 2)
        return 1;

    for(i = 0; i < 2; i++)
    {
        tmpMat[i] = cv::cvarrToMat((*input)[i]);
        ImInPVect.push_back(&tmpMat[i]);
    }

    for(i = 0; i < 2; i++)
    {
        if(ImInPVect[i]->empty())
            return 2;
    }

    bool to_ret = SegmentGrainImg(&ImInPVect, ImOutVect, outroi, transf, orientation_dv);

    if(!to_ret || ImOutVect->size()!=outroi->size() || outroi->size()!=transf->size() ||  outroi->size()!=2)
    {

        for(i = 0; i < ImOutVect->size(); i++) delete (*ImOutVect)[i];
        ImOutVect->clear();
        for(i = 0; i < (*outroi).size(); i++) delete (*outroi)[i];
        (*outroi).clear();
        if(to_ret) return 3;
        return 4;
    }

    for(i = 0; i < ImOutVect->size(); i++)
    {
        unsigned int size[2];
        double spacing[2];
        size[0] = (*ImOutVect)[i]->cols;
        size[1] = (*ImOutVect)[i]->rows;
        spacing[0] = 1.0;
        spacing[1] = 1.0;
        MIType2DRGB* outim;

        if(sizeof(MazdaImagePixelType) == 1 && (*ImOutVect)[i]->channels() == 3 && (*ImOutVect)[i]->depth() == CV_8U)
        {
            outim = new MIType2DRGB(size, spacing, (PixelRGBType*) (*ImOutVect)[i]->data);
            outim->DeallocateAtDestruction(false);
        }
        else
        {
            allfastcopy = false;
            outim = new MIType2DRGB(size, spacing);
            MazdaImageIterator<MIType2DRGB> outiterator(outim);

            switch((*ImOutVect)[i]->depth())
            {
            case CV_8U:
                switch((*ImOutVect)[i]->channels())
                {
                case 1:
                    {
                        cv::MatIterator_<uchar> it, end;
                        for( it = (*ImOutVect)[i]->begin<uchar>(), end = (*ImOutVect)[i]->end<uchar>(); it != end; ++it, ++outiterator)
                        {
                            PixelRGBType* p = outiterator.GetPixelPointer();
                            p->channel[0] = 0x101 * *it;
                            p->channel[1] = 0x101 * *it;
                            p->channel[2] = 0x101 * *it;
                        }
                        break;
                    }
                case 3:
                    {
                        cv::MatIterator_<cv::Vec3b> it, end;
                        for( it = (*ImOutVect)[i]->begin<cv::Vec3b>(), end = (*ImOutVect)[i]->end<cv::Vec3b>(); it != end; ++it, ++outiterator)
                        {
                            PixelRGBType* p = outiterator.GetPixelPointer();
                            p->channel[0] = 0x101 * (*it)[0];
                            p->channel[1] = 0x101 * (*it)[1];
                            p->channel[2] = 0x101 * (*it)[2];
                        }
                    }
                }
            break;
            default:
                switch((*ImOutVect)[i]->channels())
                {
                case 1:
                    {
                        cv::MatIterator_<uchar> it, end;
                        for( it = (*ImOutVect)[i]->begin<uchar>(), end = (*ImOutVect)[i]->end<uchar>(); it != end; ++it, ++outiterator)
                        {
                            PixelRGBType* p = outiterator.GetPixelPointer();
                            p->channel[0] = *it;
                            p->channel[1] = *it;
                            p->channel[2] = *it;
                        }
                        break;
                    }
                case 3:
                    {
                        cv::MatIterator_<cv::Vec3b> it, end;
                        for( it = (*ImOutVect)[i]->begin<cv::Vec3b>(), end = (*ImOutVect)[i]->end<cv::Vec3b>(); it != end; ++it, ++outiterator)
                        {
                            PixelRGBType* p = outiterator.GetPixelPointer();
                            p->channel[0] = (*it)[0];
                            p->channel[1] = (*it)[1];
                            p->channel[2] = (*it)[2];
                        }
                    }
                }
            }
//            delete (*ImOutVect)[i];
        }
        (*output).push_back(outim);
    }
    if(allfastcopy)
        return 0;
    return -1;
}



void qmazdaSegmentacjaDump(std::vector<MIType2DRGB *> * output, std::vector<MzRoi2D *> *outroi, int id)
{
    unsigned int ni = output->size();
    if(ni > outroi->size()) ni = outroi->size();

    for(unsigned int i = 0; i < ni; i++)
    {
        unsigned int size[4];
        (*output)[i]->GetSize(size);

        MazdaImageIterator<MIType2DRGB> outiterator((*output)[i]);
        Image* img = cvCreateImage(cvSize(size[0], size[1]), IPL_DEPTH_8U, 3);
        for(unsigned int y = 0; y < size[1]; y++)
        {
            unsigned char* ip = (unsigned char*)(img->imageData) + img->widthStep*y;
            for(unsigned int x = 0; x < size[0]; x++)
            {
                PixelRGBType* p = outiterator.GetPixelPointer();

#ifdef MAZDA_IMAGE_8BIT_PIXEL
                *ip = p->channel[0]; ip++;
                *ip = p->channel[1]; ip++;
                *ip = p->channel[2]; ip++;
#else
                *ip = p->channel[0] >> 8; ip++;
                *ip = p->channel[1] >> 8; ip++;
                *ip = p->channel[2] >> 8; ip++;
#endif
                ++outiterator;
            }
        }

        int begin[2];
        int end[2];
        unsigned char colors[4];
        unsigned int color = (*outroi)[i]->GetColor();

        colors[0] = (color&0xff)/2;
        colors[1] = ((color>>8)&0xff)/2;
        colors[2] = ((color>>16)&0xff)/2;
        (*outroi)[i]->GetBegin(begin);
        (*outroi)[i]->GetEnd(end);

        MazdaRoiIterator<MzRoi2D> rit((*outroi)[i]);
        for(int y = begin[1]; y <= end[1]; y++)
        {
            unsigned char* ip = (unsigned char*)(img->imageData) + img->widthStep*y;
            for(int x = begin[0]; x <= end[0]; x++)
            {
                if(rit.GetPixel())
                {
                    *ip = *ip/2 + colors[0]; ip++;
                    *ip = *ip/2 + colors[1]; ip++;
                    *ip = *ip/2 + colors[2]; ip++;
                }
                else
                {
                    ip+=3;
                }
                ++rit;
            }
        }
        std::string ts = (*outroi)[i]->GetName() + "_RotatedCroppedOverlay";
        debugImage(id, i, ts.c_str(), img);
        cvReleaseImage(&img);
    }
}


void qmazdaRun(ZiarnoWorkerInput* input)
{
    std::vector<cv::Mat*> ImOutVect;
    QmazdaSetup* setup = (QmazdaSetup*) input->qmazdaSetup;
    double* values;
    int klasa = 0;

    int orientation;
    std::vector<MIType2DRGB*> output;
    std::vector<MzRoi2D*> outroi;
    std::vector<TransformacjaZiarna> transf;

// Wyznaczanie obszarow zainteresowania, transformacja i przyciecie obrazow

    int segres = qmazdaSegmentacja(&input->tabInImg, &ImOutVect, &output, &outroi, &transf, &orientation);
    if(segres > 0 || outroi.size() < 2)
    {
        if(input->debuglevel&DEBUG_LOG_ERRORS) systemLog(input->id, -1, ID_LOG_ERROR, "qmazdaRun: Segmentation failed (class = 0)");
        EntryResult results[1];
        results[0].kernelClass = klasa;
        finishRun(input->id, results, 1);
        qmazdaClearMats(&ImOutVect);
        return;
    }
    else if(segres < 0)
    {
        if(input->debuglevel&DEBUG_LOG_WARNINGS)
            systemLog(input->id, -1, ID_LOG_WARNING, "qmazdaGen: image format incompatible");
    }

    if(input->debuglevel&DEBUG_DUMP_IMAGES)
        qmazdaSegmentacjaDump(&output, &outroi, input->id);

    unsigned int max_vals;

    if(orientation == 0)
        max_vals = setup->kaskadaCreaseUp->allFeatureNames.size();
    else
        max_vals = setup->kaskadaCreaseDn->allFeatureNames.size();
    values = new double[max_vals];

    double hsid, htop, hbot, height;
    int begin[4];
    int end[4];
    outroi[0]->GetBegin(begin);
    outroi[0]->GetEnd(end);
    htop = (end[0]-begin[0]+1);
    outroi[1]->GetBegin(begin);
    outroi[1]->GetEnd(end);
    hbot = (end[0]-begin[0]+1);

    int hsidi = qmazdaSideProfileHeight(&input->tabInImg);
    if(hsidi <= 0)
    {
        if(input->debuglevel&DEBUG_LOG_WARNINGS)
            systemLog(input->id, -1, ID_LOG_WARNING, "qmazdaRun: Failed to compute side profile");
        EntryResult results[1];
        results[0].kernelClass = 0;
        finishRun(input->id, results, 1);
        qmazdaClearMats(&ImOutVect);
        return;
//        hsid = 0;
//        htop = htop / top_dpmm;
//        hbot = hbot / bottom_dpmm;
//        height = htop;
//        if(hbot < height) height = hbot;
    }
    else
    {
        hsid = hsidi;
        qmazdaRecomputeSizes(&htop, &hbot, &hsid, transf[0].x, transf[0].y, transf[1].x, transf[1].y);
        height = hsid;
        if(htop < height) height = htop;
        if(hbot < height) height = hbot;
    }

    values[0] = orientation;
    values[1] = hsid;
    values[2] = height;
    values[3] = transf[0].x;
    values[4] = transf[0].y;
    values[5] = transf[0].angle;
    values[6] = transf[1].x;
    values[7] = transf[1].y;
    values[8] = transf[1].angle;

    if(height < strainer_min)
    {
        klasa = 1;
    }
    else if(height >= strainer_max)
    {
        klasa = 2;
    }
    else
    {
        for(unsigned int i = 9; i < max_vals; i++) values[i] = mznan;

        if(orientation == 0)
            klasa = setup->kaskadaCreaseUp->decisionTreeClassification(&output, &outroi, values);
        else
            klasa = setup->kaskadaCreaseDn->decisionTreeClassification(&output, &outroi, values);
    }

    if((input->debuglevel & DEBUG_LOG_INFO))
    {
        std::stringstream ss;
        ss << "qmazdaRun:FeatureVals:";
        for(unsigned int i = 0; i < max_vals; i++)
             ss << " " << values[i];
        systemLog(input->id, -1, ID_LOG_INFO, ss.str().c_str());
    }

    if(klasa == 0)
        if(input->debuglevel & DEBUG_LOG_ERRORS)
            systemLog(input->id, -1, ID_LOG_ERROR, "qmazdaRun: Decision tree returned class = 0");

    for(unsigned int i = 0; i < output.size(); i++) delete output[i];
    output.clear();
    for(unsigned int i = 0; i < outroi.size(); i++) delete outroi[i];
    outroi.clear();
    delete[] values;
    qmazdaClearMats(&ImOutVect);

    if((defaultDebugLevel & DEBUG_LOG_INFO))
    {
        std::stringstream ss;
        ss << "qmazdaRun: class = " << klasa;
        if((unsigned int)klasa < setup->allClassNames->size()) ss << ": " << (*setup->allClassNames)[klasa];
        systemLog(input->id, -1, ID_LOG_INFO, ss.str().c_str());
    }

    EntryResult results[1];
    results[0].kernelClass = klasa;
    finishRun(input->id, results, 1);
    if(input->debuglevel&DEBUG_LOG_INFO)
        systemLog(input->id, -1, ID_LOG_INFO, "qmazdaRun: Finished normally");
}


int initializeBackgrounds(Image** imagetab, int cameras, int objects)
{
    int nimages = cameras * objects;
    if(nimages <= 0) return -1000;
    cv::Mat* itab  = new cv::Mat[cameras*objects];
    for(int c = 0; c < nimages; c++)
    {
        itab[c] = cv::cvarrToMat(imagetab[c]);
    }
    int toret = ComputeBackgroundImages(itab, cameras, objects);

    if((defaultDebugLevel & DEBUG_LOG_INFO))
    {
        std::stringstream ss;
        ss << "initializeBackgrounds: WearOf = " << toret << "/1000";
        if(toret < 25) ss << " (excelent)";
        else if(toret < 50) ss << " (good)";
        else if(toret < 75) ss << " (clean)";
        else ss << " (replace)";
        systemLog(-1, -1, DEBUG_LOG_INFO, ss.str().c_str());
    }

    delete[] itab;

    if((defaultDebugLevel & DEBUG_DUMP_IMAGES))
    {
        std::vector<cv::Mat*> tab;
        GetBackgroundImages(&tab);
        for(unsigned int c = 0; c < tab.size(); c++)
        {
            Image img = *(tab[c]);
            std::stringstream ss;
            ss << "Backgorund_" << c;
            debugImage(-1, c, ss.str().c_str(), &img);
        }
    }
    return toret;
}

void* qmazdaInit(const char *configuration_filename)
{
    qmazdaEnd();
    std::ifstream file;
    file.open(configuration_filename);
    if (!file.is_open())
    {
        if((defaultDebugLevel & DEBUG_LOG_ERRORS))
        {
            systemLog(-1, -1, ID_LOG_ERROR, "qmazdaInit: Failed to open configuration file:");
            systemLog(-1, -1, ID_LOG_ERROR, configuration_filename);
        }
        return NULL;
    }
    if (!file.good())
    {
        if((defaultDebugLevel & DEBUG_LOG_ERRORS))
        {
            systemLog(-1, -1, ID_LOG_ERROR, "qmazdaInit: Failed to open configuration file:");
            systemLog(-1, -1, ID_LOG_ERROR, configuration_filename);
        }
        return NULL;
    }

    qmazdaSetup.kaskadaCreaseUp = new DecisionTree();
    qmazdaSetup.kaskadaCreaseDn = new DecisionTree();
    qmazdaSetup.allClassNames = new std::vector <std::string>();

    qmazdaSetup.kaskadaCreaseUp->parse(&file, '*');
    qmazdaSetup.kaskadaCreaseDn->parse(&file, '*');
    if(!loadParameters(&file))
    {
        if((defaultDebugLevel & DEBUG_LOG_ERRORS))
        {
            systemLog(-1, -1, ID_LOG_ERROR, "qmazdaInit: Failed to load configuration file:");
            systemLog(-1, -1, ID_LOG_ERROR, configuration_filename);
        }
        return NULL;
    }

    dcentertop = side_to_pivot - sqrt(top_pivot_xo*top_pivot_xo + top_pivot_yo*top_pivot_yo);
    dcenterbot = side_to_pivot - sqrt(bottom_pivot_xo*bottom_pivot_xo + bottom_pivot_yo*bottom_pivot_yo);

    if((defaultDebugLevel & DEBUG_LOG_INFO))
    {
        std::stringstream ss;
        ss << "qmazdaInit: [Parameters]:";
        ss << " top_dpmm " << top_dpmm;
        ss << " bottom_dpmm " << bottom_dpmm;
        ss << " side_dpmm " << side_dpmm;
        ss << " strainer_min " << strainer_min;
        ss << " strainer_max " << strainer_max;
        ss << " top_pivot_xo " << top_pivot_xo;
        ss << " top_pivot_yo " << top_pivot_yo;
        ss << " bottom_pivot_xo " << bottom_pivot_xo;
        ss << " bottom_pivot_yo " << bottom_pivot_yo;
        ss << " side_to_pivot " << side_to_pivot;
        ss << " top_to_disk " << top_to_disk;
        ss << " bottom_to_disk " << bottom_to_disk;
        systemLog(-1, -1, ID_LOG_INFO, ss.str().c_str());
    }

    qmazdaSetup.kaskadaCreaseUp->allFeatureNames.push_back("S_orientation");
    qmazdaSetup.kaskadaCreaseUp->allFeatureNames.push_back("S_strainer");
    qmazdaSetup.kaskadaCreaseUp->allFeatureNames.push_back("S_height");
    qmazdaSetup.kaskadaCreaseUp->allFeatureNames.push_back("A_x");
    qmazdaSetup.kaskadaCreaseUp->allFeatureNames.push_back("A_y");
    qmazdaSetup.kaskadaCreaseUp->allFeatureNames.push_back("A_angle");
    qmazdaSetup.kaskadaCreaseUp->allFeatureNames.push_back("B_x");
    qmazdaSetup.kaskadaCreaseUp->allFeatureNames.push_back("B_y");
    qmazdaSetup.kaskadaCreaseUp->allFeatureNames.push_back("B_angle");

    qmazdaSetup.kaskadaCreaseDn->allFeatureNames.push_back("S_orientation");
    qmazdaSetup.kaskadaCreaseDn->allFeatureNames.push_back("S_strainer");
    qmazdaSetup.kaskadaCreaseDn->allFeatureNames.push_back("S_height");
    qmazdaSetup.kaskadaCreaseDn->allFeatureNames.push_back("A_x");
    qmazdaSetup.kaskadaCreaseDn->allFeatureNames.push_back("A_y");
    qmazdaSetup.kaskadaCreaseDn->allFeatureNames.push_back("A_angle");
    qmazdaSetup.kaskadaCreaseDn->allFeatureNames.push_back("B_x");
    qmazdaSetup.kaskadaCreaseDn->allFeatureNames.push_back("B_y");
    qmazdaSetup.kaskadaCreaseDn->allFeatureNames.push_back("B_angle");

    qmazdaSetup.allClassNames->push_back("%Error%");
    qmazdaSetup.allClassNames->push_back("%Small%");
    qmazdaSetup.allClassNames->push_back("%Large%");


    qmazdaSetup.kaskadaCreaseUp->errorMessage.clear();
    qmazdaSetup.kaskadaCreaseDn->errorMessage.clear();
    if(!qmazdaSetup.kaskadaCreaseUp->loadClassifiers(qmazdaSetup.allClassNames))
    {
        if((defaultDebugLevel & DEBUG_LOG_ERRORS))
        {
            std::stringstream ss;
            ss << "qmazdaInit: Failed to load CreaseUp decision tree: " << qmazdaSetup.kaskadaCreaseUp->errorMessage;
            systemLog(-1, 0, ID_LOG_ERROR, ss.str().c_str());
        }
        return NULL;
    }

    if(!qmazdaSetup.kaskadaCreaseDn->loadClassifiers(qmazdaSetup.allClassNames))
    {
        if((defaultDebugLevel & DEBUG_LOG_ERRORS))
        {
            std::stringstream ss;
            ss << "qmazdaInit: Failed to load CreaseDn decision tree: " << qmazdaSetup.kaskadaCreaseDn->errorMessage;
            systemLog(-1, 0, ID_LOG_ERROR, ss.str().c_str());
        }
        return NULL;
    }

// Notifies about tree structure
    if((defaultDebugLevel & DEBUG_LOG_INFO))
    {
        std::stringstream ss;
        ss << "qmazdaInit:CreaseUp:\n" << qmazdaSetup.kaskadaCreaseUp->serialize('\t');
        systemLog(-1, 0, ID_LOG_INFO, ss.str().c_str());
        ss.str(std::string());
        ss << "qmazdaInit:CreaseDn:\n" << qmazdaSetup.kaskadaCreaseDn->serialize('\t');
        systemLog(-1, 1, ID_LOG_INFO, ss.str().c_str());
    }

    if((defaultDebugLevel & DEBUG_LOG_INFO))
    {
        std::stringstream ss;
        ss << "qmazdaInit:ClassNames:\n";
        for(unsigned int i = 0; i < qmazdaSetup.allClassNames->size(); i++)
            ss << i << ": " << (* qmazdaSetup.allClassNames)[i] <<"\n";
        systemLog(-1, -1, ID_LOG_INFO, ss.str().c_str());
    }

    if((defaultDebugLevel & DEBUG_LOG_INFO))
    {
        std::stringstream ss;
        ss << "qmazdaInit:FeaturesUp:";
        for(unsigned int i = 0; i < qmazdaSetup.kaskadaCreaseUp->allFeatureNames.size(); i++)
             ss << " " << qmazdaSetup.kaskadaCreaseUp->allFeatureNames[i];
        systemLog(-1, 0, ID_LOG_INFO, ss.str().c_str());
        ss.str(std::string());
        ss << "qmazdaInit:FeaturesDn:";
        for(unsigned int i = 0; i < qmazdaSetup.kaskadaCreaseDn->allFeatureNames.size(); i++)
             ss << " " << qmazdaSetup.kaskadaCreaseDn->allFeatureNames[i];
        systemLog(-1, 1, ID_LOG_INFO, ss.str().c_str());
    }

    if(!qmazdaSetup.kaskadaCreaseUp->renumberClasses(qmazdaSetup.allClassNames))
    {
        if((defaultDebugLevel & DEBUG_LOG_ERRORS))
        {
            systemLog(-1, 0, ID_LOG_ERROR, "qmazdaInit: Failed reindexing CreaseUp classes");
        }
    }
    if(!qmazdaSetup.kaskadaCreaseDn->renumberClasses(qmazdaSetup.allClassNames))
    {
        if((defaultDebugLevel & DEBUG_LOG_ERRORS))
        {
            systemLog(-1, 1, ID_LOG_ERROR, "qmazdaInit: Failed reindexing CreaseDn classes");
        }
    }

    return (void*)&qmazdaSetup;
}

void qmazdaEnd(void)
{
    if(qmazdaSetup.kaskadaCreaseUp != NULL) delete qmazdaSetup.kaskadaCreaseUp;
    qmazdaSetup.kaskadaCreaseUp = NULL;
    if(qmazdaSetup.kaskadaCreaseDn != NULL) delete qmazdaSetup.kaskadaCreaseDn;
    qmazdaSetup.kaskadaCreaseDn = NULL;
    if(qmazdaSetup.allClassNames != NULL) delete qmazdaSetup.allClassNames;
    qmazdaSetup.allClassNames = NULL;
}

void qmazdaGenerate(const char *configuration_filename, std::vector <Image *>* tabInImg, std::vector <std::string>* featureNamesOut, double** featureValuesOut)
{
    std::string A_ = "A_";
    std::string B_ = "B_";

    if(featureNamesOut == NULL) return;
    std::vector <std::string> featureNames;

    featureNames = loadFeatureList(configuration_filename);

    std::vector <std::string> featureNamesVerified;
    NameStubTree templateTree;
    StubItemBuilder::CreateTemplate(&templateTree, roiTreeTemplate, sizeof(roiTreeTemplate)/sizeof(ItemTemplate));

    StubItemBuilder* featureComputationObject = new StubItemBuilder(2, &templateTree);
    for(unsigned int i = 0; i < featureNames.size(); i++)
        if(featureComputationObject->addFeature(featureNames[i], featureNamesVerified.size(), NULL))
        {
            featureNamesVerified.push_back(featureNames[i]);
        }
    featureNames.clear();

    if((defaultDebugLevel & DEBUG_LOG_ERRORS))
    {
        if(featureNamesVerified.size() <= 0)
        {
            systemLog(-1, -1, ID_LOG_ERROR, "Empty feature list, check configuration_filename:");
            systemLog(-1, -1, ID_LOG_ERROR, configuration_filename);
        }
    }

    featureNamesOut->push_back("S_orientation");
    featureNamesOut->push_back("S_strainer");
    featureNamesOut->push_back("S_height");

    featureNamesOut->push_back(A_ + "x");
    featureNamesOut->push_back(A_ + "y");
    featureNamesOut->push_back(A_ + "angle");
    featureNamesOut->push_back(B_ + "x");
    featureNamesOut->push_back(B_ + "y");
    featureNamesOut->push_back(B_ + "angle");
    unsigned int first_real_feature = featureNamesOut->size();

    for(unsigned int i = 0; i < featureNamesVerified.size(); i++)
    {
        featureNamesOut->push_back(A_ + featureNamesVerified[i]);
    }
    for(unsigned int i = 0; i < featureNamesVerified.size(); i++)
    {
        featureNamesOut->push_back(B_ + featureNamesVerified[i]);
    }
    if(tabInImg == NULL || featureValuesOut == NULL)
    {
        if((defaultDebugLevel & DEBUG_LOG_INFO))
        {
            systemLog(-1, -1, ID_LOG_INFO, "qmazdaGenerate: Finishing with feature names only");
        }
        return;
    }

    loadParameters(configuration_filename);

    dcentertop = side_to_pivot - sqrt(top_pivot_xo*top_pivot_xo + top_pivot_yo*top_pivot_yo);
    dcenterbot = side_to_pivot - sqrt(bottom_pivot_xo*bottom_pivot_xo + bottom_pivot_yo*bottom_pivot_yo);

    if((defaultDebugLevel & DEBUG_LOG_INFO))
    {
        std::stringstream ss;
        ss << "qmazdaGenerate: [Parameters]:";
        ss << " top_dpmm " << top_dpmm;
        ss << " bottom_dpmm " << bottom_dpmm;
        ss << " side_dpmm " << side_dpmm;
        ss << " strainer_min " << strainer_min;
        ss << " strainer_max " << strainer_max;
        ss << " top_pivot_xo " << top_pivot_xo;
        ss << " top_pivot_yo " << top_pivot_yo;
        ss << " bottom_pivot_xo " << bottom_pivot_xo;
        ss << " bottom_pivot_yo " << bottom_pivot_yo;
        ss << " side_to_pivot " << side_to_pivot;
        ss << " top_to_disk " << top_to_disk;
        ss << " bottom_to_disk " << bottom_to_disk;
        systemLog(-1, -1, ID_LOG_INFO, ss.str().c_str());
    }

    std::vector<MIType2DRGB*> output;
    std::vector<MzRoi2D*> outroi;
    std::vector<TransformacjaZiarna> transf;

    int orientation_dv=0;
    std::vector<cv::Mat*> ImOutVect;

    int segres = qmazdaSegmentacja(tabInImg, &ImOutVect, &output, &outroi, &transf, &orientation_dv);
    if(segres > 0 || outroi.size() < 2)
    {
        qmazdaClearMats(&ImOutVect);
        return;
    }
    else if(segres < 0)
    {
        if(defaultDebugLevel & DEBUG_LOG_WARNINGS)
            systemLog(0, -1, ID_LOG_WARNING, "qmazdaGen: image format incompatible");
    }

    if((defaultDebugLevel & DEBUG_DUMP_IMAGES))
    {
        qmazdaSegmentacjaDump(&output, &outroi, 0);
    }

    if(defaultDebugLevel & DEBUG_LOG_INFO)
    {
        std::stringstream ss;
        ss << "qmazdaGen: [Images] ";
        for(unsigned int i = 0; i < output.size(); i++)
        {
            unsigned int size[4];
            output[i]->GetSize(size);
            ss << size[1] << "x" << size[0] << " ";
        }
        ss << "[Rois] ";
        for(unsigned int i = 0; i < outroi.size(); i++)
        {
            int begin[4];
            int end[4];
            outroi[i]->GetBegin(begin);
            outroi[i]->GetEnd(end);
            ss << "("<< end[1] << "-" << begin[1] << ")x(" << end[0] << "-" << begin[0] << ") ";
        }
        systemLog(0, -1, ID_LOG_INFO, ss.str().c_str());
    }

    double hsid = 0.0;
    if(tabInImg->size() >= 3)
    {
        cv::Mat tmpMat;
        tmpMat = cv::cvarrToMat((*tabInImg)[2]);
        hsid = FindGrainHeigh(tmpMat);
    }

    int begin[4];
    int end[4];
    outroi[0]->GetBegin(begin);
    outroi[0]->GetEnd(end);
    double htop = (end[0]-begin[0]+1);
    outroi[1]->GetBegin(begin);
    outroi[1]->GetEnd(end);
    double hbot = (end[0]-begin[0]+1);

    double height = hsid;
    if(hsid > 0.0)
    {
        qmazdaRecomputeSizes(&htop, &hbot, &hsid, transf[0].x, transf[0].y, transf[1].x, transf[1].y);
        height = hsid;
        if(htop < height) height = htop;
        if(hbot < height) height = hbot;
    }
    else
    {
        htop = htop / top_dpmm;
        hbot = hbot / bottom_dpmm;
        height = htop;
        if(hbot < height) height = hbot;
    }

    double* feature_values = new double[featureNamesOut->size()];;
    *featureValuesOut = feature_values;
    for(unsigned int i = 0; i < featureNamesOut->size(); i++) feature_values[i] = mznan;

    feature_values[0] = orientation_dv;
    feature_values[1] = hsid;
    feature_values[2] = height;
    feature_values[3] = transf[0].x;
    feature_values[4] = transf[0].y;
    feature_values[5] = transf[0].angle;
    feature_values[6] = transf[1].x;
    feature_values[7] = transf[1].y;
    feature_values[8] = transf[1].angle;


    if(height < strainer_min || height >= strainer_max)  //analysis cancelled, size too small or too big
    {
        qmazdaClearMats(&ImOutVect);
        return;
    }
    StubItemDataEntryRoi genData;
    genData.image = output[0];
    genData.isRGB = true;
    genData.roi = outroi[0];
    genData.table = feature_values + first_real_feature;
    featureComputationObject->Execute(&genData);

    genData.image = output[1];
    genData.isRGB = true;
    genData.roi = outroi[1];
    genData.table = feature_values + first_real_feature + featureNamesVerified.size();
    featureComputationObject->Execute(&genData);

    for(unsigned int i = 0; i < output.size(); i++) delete output[i];
    output.clear();
    for(unsigned int i = 0; i < outroi.size(); i++) delete outroi[i];
    outroi.clear();
    qmazdaClearMats(&ImOutVect);
}
