/*
 * main.cpp - program do testowania modułu analizy ziaren jęczmienia
 *
 * Autor:  (2016) Piotr M. Szczypiński, Politechnika Łódzka
 *
 * Opracowano w ramach grantu: NCBR PBS3/A8/38/2015
 * Opracowanie przemysłowej metody automatycznej oceny parametrów
 * technologicznych i klasyfikacji ziarna z zastosowaniem analizy
 * obrazów.
 */

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui_c.h>
#include <opencv2/imgproc/imgproc_c.h>
#include "../ZiarnoLib/lib_ziarno.h"
//#include <unistd.h>
#include "../../qmazda/MzShared/getoption.h"
#include "../../qmazda/MzShared/dataforconsole.h"

char* top_back = NULL;
char* bottom_back = NULL;
char* top_image = NULL;
char* bottom_image = NULL;
char* side_image = NULL;

char* input_features = NULL;
char* output_data[2] = {NULL, NULL};
char* class_name = (char*)("Unknown");
char* comment = (char*)("");
// bool hexadecimal = false;
int debug_log_level = DEBUG_LOG_ERRORS;
bool isprinthelp = false;
// bool horizontal = false;
const double mznan = std::numeric_limits<double>::quiet_NaN();

int scan_parameters_local(int argc, char* argv[])
{
    int argi = 1;
    while(argi < argc)
    {
        GET_STRING_OPTION("-t", "--input-top", top_image)
        else GET_STRING_OPTION("-b", "--input-bottom", bottom_image)
        else GET_STRING_OPTION("-T", "--back-top", top_back)
        else GET_STRING_OPTION("-B", "--back-bottom", bottom_back)
        else GET_STRING_OPTION("-s", "--input-side", side_image)
        else GET_STRING_OPTION("-f", "--features", input_features)
        else GET_STRING_OPTION("-U", "--output-up", output_data[0])
        else GET_STRING_OPTION("-D", "--output-dn", output_data[1])
        else GET_STRING_OPTION("-c", "--category", class_name)
        else GET_STRING_OPTION("-r", "--comment", comment)
//        else GET_NOARG_OPTION("-s", "--horizontal-split", horizontal, true)
        else GET_INT_OPTION("-d", "--debug-log", debug_log_level)
        //else GET_NOARG_OPTION("-x", "--save-hex", hexadecimal, true)
        else GET_NOARG_OPTION("/?", "--help", isprinthelp, true)
        else return argi;
        argi++;
    }
    return 0;
}
void printhelp_local(char* name)
{
    //------------------------------------------------------------------------------------------v
    printf("Usage: %s [OPTION]...\n", name);
    printf("Computes image attributes (features) in regions.\n");
    printf("2017.11.28 by Piotr M. Szczypinski\n");
    printf("Compilation date and time: %s %s\n", __DATE__, __TIME__);
    printf("Options:\n");
    printf("  -t, --input-top <file>     Load top camera image from <file>.\n");
    printf("  -b, --input-bottom <file>  Load bottom camera image from <file>.\n");
    printf("  -T, --back-top <file>      Load top camera background from <file>.\n");
    printf("  -B, --back-bottom <file>   Load bottom camera background from <file>.\n");
    printf("  -s, --input-side <file>    Load side camera image from <file>.\n");
    printf("  -f, --features <file>      Load feature list from <file>\n");
    printf("  -U, --output-up <file>     Save results to csv or floating-point multipage tiff <file>\n");
    printf("  -D, --output-dn <file>     Save results to csv or floating-point multipage tiff <file>\n");
    printf("  -c, --category             Category name (class, label) for feature vector\n");
    printf("  -r, --comment              Comment\n");
    //printf("  -s, --horizontal-split     Split image horizontaly, default is vertical split\n");
    //printf("  -x, --save-hex          Save double precision results in hexadecimal format\n");
    printf("  -d, --debug-log <int>      Errors +1, Warnings+2, Info +4, Images +8 +16\n");
    printf("  /?, --help                 Display this help and exit\n\n");
}

//-----------------------------------------------------------------------------
// FUNKCJE CALLBACK
//-----------------------------------------------------------------------------
void FinishRunImpl(int id, EntryResult *tabResult, int numResult)
{
    if(id >= 0) // Dotyczy ziarniaka
    {
        std::cout<< "FinishRun: " << id;
        for(int i = 0; i < numResult; i++)
        {
            std::cout << " class = " << tabResult[i].kernelClass;
        }
        std::cout << std::endl;
    }
}
void DebugImageCallbackImpl(int id, int idInImg, const char* imgName, const Image *debugImg)
{
    static int nr0 = 0;
    static int nr1 = 0;
    static int nr2 = 0;
    static int nr3 = 0;
    std::cout<< "DebugImageCallback: OBJ#" << id << " SUB#" << idInImg << "Name:" << imgName << std::endl;
    std::stringstream ss;
//    ss << "Dbg" << id << "_" << idInImg << "_" << imgName << ".tif";
    if(idInImg == 0)
    {
        ss << top_image << "_log"<< nr0 <<".tif";
        nr0++;
    }
    else if(idInImg == 1)
    {
        ss << bottom_image << "_log"<< nr1 <<".tif";
        nr1++;
    }
    else if(idInImg == 2)
    {
        ss << side_image << "_log"<< nr2 <<".tif";
        nr2++;
    }
    else
    {
        ss << "Log" << id << "_" << idInImg << "_" << imgName << nr3 <<".tif";
        nr3++;
    }
    //cvSaveImage(ss.str().c_str(), debugImg);


    cv::Mat im(debugImg->height, debugImg->width, debugImg->nChannels == 4 ?CV_8UC4 :CV_8UC3, (void*)debugImg->imageData, debugImg->widthStep);
    cv::Mat dst;
    cv::flip(im, dst, 0);
    cv::imwrite(ss.str().c_str(), dst);

}

void SystemLogCallbackImpl(int id, int idInImg, int logId, const char* message)
{
    std::cout<< "SystemLogCallback: ";
    switch(logId)
    {
    case ID_LOG_INFO: std::cout<< "INFO "; break;
    case ID_LOG_WARNING: std::cout<< "WARNING "; break;
    case ID_LOG_ERROR: std::cout<< "ERROR "; break;
    default: std::cout<< "UNKNOWN ";
    }
    std::cout << " OBJ#" << id << " SUB#" << idInImg << " : " << message << std::endl;
}

//-----------------------------------------------------------------------------
// TEST
//-----------------------------------------------------------------------------
//bool loadImages(const char* fileName, ZiarnoWorkerInput* data)
//{
//   Image* src = cvLoadImage(fileName, CV_LOAD_IMAGE_COLOR);
//   if(src == NULL)
//       return false;

////Rozdziela obrazek z pliku na dwa. Docelowo obrazki powinny byc oddzielne
//   int splv, splh;
//   int stav, stah;
//   if(horizontal)
//   {
//       stav = src->width/2;
//       stah = 0;
//       splv = src->width/2;
//       splh = src->height;
//   }
//   else
//   {
//       stav = 0;
//       stah = src->height/2;
//       splv = src->width;
//       splh = src->height/2;
//   }
//   cvSetImageROI(src, cvRect(0, 0, splv, splh));
//   Image *upper = cvCreateImage(cvSize(splv, splh), src->depth, src->nChannels);
//   cvCopy(src, upper, NULL);
//   cvResetImageROI(src);
//   cvSetImageROI(src, cvRect(stav, stah, splv, splh));
//   Image *lower = cvCreateImage(cvSize(splv, splh), src->depth, src->nChannels);
//   cvCopy(src, lower, NULL);
//   cvResetImageROI(src);
//   cvReleaseImage(& src);

//   std::vector <Image *> images;
//   images.push_back(upper);
//   images.push_back(lower);

//   data->tabInImg = images;
//   return true;
//}


char* loadImages(std::vector <Image *>* images)
{
    Image* it = NULL;
    Image* ib = NULL;
    Image* is = NULL;
    if(top_image != NULL)
        it = cvLoadImage(top_image, CV_LOAD_IMAGE_COLOR);
    else
        return top_image;

    if(bottom_image != NULL)
        ib = cvLoadImage(bottom_image, CV_LOAD_IMAGE_COLOR);
    else
        return bottom_image;

    if(side_image != NULL)
        is = cvLoadImage(side_image, CV_LOAD_IMAGE_COLOR);

    if(it == NULL)
        return top_image;

    if(ib == NULL)
        return bottom_image;

    images->push_back(it);
    images->push_back(ib);

    if(is != NULL)
        images->push_back(is);

    return NULL;
}




void setSystemLogCallback(SystemLogCallback _systemLog)
{
    systemLog = _systemLog;
}
void setDebugImageCallback(DebugImageCallback _debugImage)
{
    debugImage = _debugImage;
}
void setFinishRun(FinishRun _onFinish)
{
    finishRun = _onFinish;
}

//-----------------------------------------------------------------------------
// MAIN
//-----------------------------------------------------------------------------
int main(int argc, char* argv[])
{
    std::ofstream file;

    int ret = scan_parameters_local(argc, argv);
    if(isprinthelp || argc <= 1)
    {
        printhelp_local(argv[0]);
        return 1;
    }
    if(ret != 0)
    {
        if(ret < argc) fprintf(stderr, "Incorrect operand: %s\n", argv[ret]);
        fprintf(stderr, "Try --help for more information.\n");
        return ret;
    }

    qmazdaSetDebugLevel(debug_log_level);

    setSystemLogCallback(SystemLogCallbackImpl);
    setFinishRun(FinishRunImpl);
    setDebugImageCallback(DebugImageCallbackImpl);


    if(input_features == NULL) {fprintf(stderr, "Unspecified feature list file (-f)\n"); return -3;}
    if(output_data[0] == NULL) {fprintf(stderr, "Unspecified output file (-U)\n"); return -3;}
    if(output_data[1] == NULL) {fprintf(stderr, "Unspecified output file (-D)\n"); return -3;}

    std::vector <std::string> featureNamesOut;
    double* featureValuesOut = NULL;
    if(top_image == NULL && bottom_image == NULL && side_image == NULL)
    {
        bool error = false;
        qmazdaGenerate(input_features, NULL, &featureNamesOut, NULL);
        for(int c = 0; c < 2; c++)
        {
            file.open(output_data[c]);
            if (file.is_open() && file.good())
            {
                std::vector<std::string>::iterator nameit;
                for(nameit = featureNamesOut.begin(); nameit != featureNamesOut.end(); ++nameit)
                    file << *nameit << ",";
                file << "Category,Comment" << std::endl;
                file.close();
            }
            else
            {
                fprintf(stderr, "Output file problem: %s\n", output_data[c]);
                error = true;
            }
        }
        if(error) return -6;
        return 0;
    }

/*
    cv::Mat top_background;
    cv::Mat bottom_background;

    if(top_back != NULL && bottom_back != NULL)
    {
        std::vector<cv::Mat*> background;

        top_background = cv::imread(top_back, CV_LOAD_IMAGE_COLOR);
        bottom_background = cv::imread(bottom_back, CV_LOAD_IMAGE_COLOR);

        if(!(top_background.empty() || bottom_background.empty()))
        {
            background.push_back(& top_background);
            background.push_back(& bottom_background);
            SetBackgroundImages(& background);
        }
        else {fprintf(stderr, "Missing background files\n");}
    }
    else {fprintf(stderr, "Unspecified background files\n");}
*/

    Image* top_background;
    Image* bottom_background;
    if(top_back != NULL && bottom_back != NULL)
    {
        top_background = cvLoadImage(top_back, CV_LOAD_IMAGE_COLOR);
        bottom_background = cvLoadImage(bottom_back, CV_LOAD_IMAGE_COLOR);
        if(!(top_background == NULL || bottom_background == NULL))
        {
            SetBackgroundImage(top_background, 0);
            SetBackgroundImage(bottom_background, 1);
        }
        else {fprintf(stderr, "Warning: Missing background files\n");}
    }
    else {fprintf(stderr, "Warning: Unspecified background files (-T -B)\n");}



    finishRun = FinishRunImpl;
    debugImage = DebugImageCallbackImpl;
    systemLog = SystemLogCallbackImpl;

//    void* qmazdaInitData = qmazdaInit(input_features);
//    if(qmazdaInitData == NULL) {fprintf(stderr, "Initialization failed: %s\n", input_features); return -5;}

//    std::vector <std::string> names = qmazdaNames();


    std::vector <Image *> tabInImg;
    char* failed = loadImages(&tabInImg);
    if(failed != NULL)
    {
        fprintf(stderr, "Image loading problem: %s\n", failed);
        return -7;
    }

    qmazdaGenerate(input_features, &tabInImg, &featureNamesOut, &featureValuesOut);

    if(featureValuesOut == NULL)
    {
        fprintf(stderr, "Configuration loading problem: %s\n", input_features);
        return -8;
    }

    int side = 0;
    if(featureValuesOut[0] > 0.5) side = 1;
    file.open(output_data[side], std::ofstream::out | std::ofstream::app);
    if (!file.is_open())
    {
        fprintf(stderr, "Output file problem: %s\n", output_data[side]);
        return -6;
    }
    if (!file.good())
    {
        fprintf(stderr, "Output file problem: %s\n", output_data[side]);
        return -6;
    }

    for(unsigned int i = 0; i < featureNamesOut.size(); i++)
    {
        file << featureValuesOut[i] << ",";
    }
    file << class_name << "," << comment << std::endl;
    file.close();

    if(featureValuesOut == NULL) delete[] featureValuesOut;
    return 0;
}

