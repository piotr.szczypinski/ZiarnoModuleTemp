#ifndef HEIGHTFROMSIDEIMAGE_H
#define HEIGHTFROMSIDEIMAGE_H


#include <opencv2/core/core.hpp>

int FindGrainHeigh(cv::Mat sideImage, int threshold = 100);

#endif // HEIGHTFROMSIDEIMAGE_H
