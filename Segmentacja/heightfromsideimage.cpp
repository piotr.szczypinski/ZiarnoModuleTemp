#include <opencv2/core/core.hpp>
#include "heightfromsideimage.h"

//--------------------------------------------------------------------------------------------------
int FindGrainHeigh(cv::Mat sideImage, int threshold)
{
    int maxX = sideImage.cols;
    int maxY = sideImage.rows;
    if(maxX == 0 || maxY ==0)
        return -1;
    int maxXM1 = maxX-1;
    if(!sideImage.isContinuous())
        return -2;

    unsigned char *wImBGR = (unsigned char*)sideImage.data;
    int *BlacLineLengths = new int[maxY];
    int firstLine = -1;
    int firstBlackPixelOnEdgePosL = -1;
    int firstBlackPixelOnEdgePosR = -1;//
    bool firstLineFound = false;


    if(sideImage.channels() >= 3)
    {
        for (int y = 0; y < maxY; y++)
        {
            int sumLineBlackPixels = 0;
            for (int x = 0; x < maxX; x++)
            {
                float brightness ;
                brightness = (int)*wImBGR * 114;
                wImBGR++;
                brightness += (int)*wImBGR * 587;
                wImBGR++;
                brightness += (int)*wImBGR * 299;
                wImBGR++;
                brightness /=1000;
                if(brightness <= threshold)
                {
                    sumLineBlackPixels ++;
                    if(x == 0 && firstBlackPixelOnEdgePosL < y)
                        firstBlackPixelOnEdgePosL = y;
                    if(x == maxXM1 && firstBlackPixelOnEdgePosR < y)
                        firstBlackPixelOnEdgePosR = y;
                }
            }
            BlacLineLengths[y] = sumLineBlackPixels;
            if(sumLineBlackPixels && !firstLineFound)
            {
                firstLine = y;
                firstLineFound = true;
            }
            if(firstBlackPixelOnEdgePosL >= 0 && firstBlackPixelOnEdgePosR >= 0)
            {
                break;
            }
        }
    }
    else
    {
        for (int y = 0; y < maxY; y++)
        {
            int sumLineBlackPixels = 0;
            for (int x = 0; x < maxX; x++)
            {
                if(*wImBGR <= threshold)
                {
                    sumLineBlackPixels ++;
                    if(x == 0 && firstBlackPixelOnEdgePosL < y)
                        firstBlackPixelOnEdgePosL = y;
                    if(x == maxXM1 && firstBlackPixelOnEdgePosR < y)
                        firstBlackPixelOnEdgePosR = y;
                }
                wImBGR++;
            }
            BlacLineLengths[y] = sumLineBlackPixels;
            if(sumLineBlackPixels && !firstLineFound)
            {
                firstLine = y;
                firstLineFound = true;
            }
            if(firstBlackPixelOnEdgePosL >= 0 && firstBlackPixelOnEdgePosR >= 0)
            {
                break;
            }
        }
    }


    int firstBlackPixelOnEdgePos = firstBlackPixelOnEdgePosL;
    if(firstBlackPixelOnEdgePos < firstBlackPixelOnEdgePosR)
        firstBlackPixelOnEdgePos = firstBlackPixelOnEdgePosR;

    if(!firstLineFound)
        return -3;
    if(firstBlackPixelOnEdgePos == -1)
        return -4;

    int start = firstBlackPixelOnEdgePos - (firstBlackPixelOnEdgePos - firstLine) * 2 / 3;

    int minLineLength = maxX;
    int minLinePosition = 0;
    for(int y = start; y<firstBlackPixelOnEdgePos; y++)
    {
        if(minLineLength > BlacLineLengths[y])
        {
            minLineLength = BlacLineLengths[y];
            minLinePosition = y;
        }
    }
    delete[] BlacLineLengths;
    return minLinePosition - firstLine;
}
