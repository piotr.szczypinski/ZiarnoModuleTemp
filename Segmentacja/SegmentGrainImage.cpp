//#include <iostream>
//#include <string>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#ifdef TERAZ_DEBUG
// Zabezpieczenie: Nie ma prawa korzystac z funckji highgui poza trybem debug
#include <opencv2/highgui/highgui.hpp>
#endif


#include "gradient.h"
//#include "DispLib.h"

#include "mazdaroi.h"
#include "mazdaroiio.h"
#include "SegmentGrainImage.h"

typedef MazdaRoi<unsigned int, 2> MR2DType;

using namespace std;
using namespace cv;

cv::Mat* segmentBackgroundImages[maxNumberOfBackgrounds] = {NULL, NULL};

//---------------------------------------------------------------------------------------------
//              functions
//--------------------------------------------------------------------------------------------------

#ifdef TERAZ_DEBUG
#include "DispLib.h"
#endif


/**
 * @brief ComputeBackgroundImages oblicza obrazy tla na podstawie tablicy obrazow
 * @param Stack - tablica obrazow
 * @param number_of_images - liczba obrazow w serii dla jednej kamery
 * @param number_of_cameras - liczba kamer
 * @return
 */

int ComputeBackgroundImages(cv::Mat* Stack, int number_of_images, int number_of_cameras)
{
    if (number_of_images < 1)
        return -1;
    if (number_of_cameras < 1)
        return -2;

    int maxX = Stack[0].cols;
    int maxY = Stack[0].rows;
    int type = Stack[0].type();
    int chan = Stack[0].channels();
    unsigned int step = Stack[0].step;

    int realNumberOfBackgrounds = maxNumberOfBackgrounds;
    if(realNumberOfBackgrounds > number_of_cameras) realNumberOfBackgrounds = number_of_cameras;

    unsigned char minVal;

// Weryfikacja czy wszystkie obrazy sa takie same
    for(int c = 0; c < realNumberOfBackgrounds; c++)
    {
        for(int i = 0; i < number_of_images; i++)
            if (Stack[i*number_of_cameras+c].step != step ||
                Stack[i*number_of_cameras+c].cols != maxX ||
                Stack[i*number_of_cameras+c].rows != maxY ||
                Stack[i*number_of_cameras+c].type() != type ||
                Stack[i*number_of_cameras+c].channels() != chan)
                return -3;
    }

//Obliczanie tla
    for(int c = 0; c < realNumberOfBackgrounds; c++)
    {
        if(segmentBackgroundImages[c] == NULL) segmentBackgroundImages[c] = new cv::Mat;

        *segmentBackgroundImages[c] = Mat::zeros(maxY, maxX, type);
        for(int y = 0; y < maxY; y++)
        {
            unsigned char* back = ((unsigned char *)segmentBackgroundImages[c]->data + y * step);
            for(int x = 0; x < maxX*chan; x++)
            {
                minVal = 255;
                for(int i = 0; i < number_of_images; i++)
                {
                    unsigned char pix = *((unsigned char *)Stack[i*number_of_cameras+c].data + y * step + x);
                    if(minVal > pix)
                        minVal = pix;
                }
                *back = minVal;
                back++;
            }
            if(segmentBackgroundImages[c]->empty())
                return -5;
        }
    }

#ifdef TERAZ_DEBUG
    imshow("Background T", BackgroundImages[0]);
    imwrite("C:/Data/Ziarno/BackgroundT.png",BackgroundImages[0]);
    imshow("Background B", BackgroundImages[1]);
    imwrite("C:/Data/Ziarno/BackgroundB.png",BackgroundImages[1]);
#endif

//Estymacja zuzycia plyty
    unsigned long sumPixelsBad = 0;
    unsigned long sumPixelsCount = 0;
    for(int c = 0; c < realNumberOfBackgrounds; c++)
    {
        int channels = segmentBackgroundImages[c]->channels();
        sumPixelsCount += (segmentBackgroundImages[c]->cols * segmentBackgroundImages[c]->rows * channels);
        size_t stride = segmentBackgroundImages[c]->step;

        for(int i = 0; i < number_of_images; i++)
        {
            for(int y = 0; y < maxY; y++)
            {
                for(int x = 0; x < maxX; x++)
                {
                    unsigned char* ppix = ((unsigned char *)Stack[i*number_of_cameras+c].data + y * stride + x * channels);
                    unsigned char* back = ((unsigned char *)segmentBackgroundImages[c]->data + y * stride + x * channels);
                    for(int n = 0; n < channels; n++)
                    {
                        unsigned int dif = abs(*ppix - *back);
                        dif /= qualityDividerForBackgroundWearof;
                        sumPixelsBad += dif;
                        ppix++;
                        back++;
                    }
                }
            }
        }
    }
    double wareof = sumPixelsBad;
    wareof /= sumPixelsCount;
    wareof = 1.0 - 0.025/(wareof+0.025);
    wareof *= 1000;
    return (int)(wareof);
}

#ifdef TERAZ_DEBUG
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//                            Display Functions
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
void ShowImage(bool show, Mat ImIn, string WindowName)
{
    if(show)
        imshow(WindowName ,ImIn);
}
//---------------------------------------------------------------------------------------------
void ShowImageAndMask(bool show, bool contour, Mat ImIn, Mat Mask, string WindowName)
{
    if(show)
    {
        if(contour)
            imshow(WindowName, ShowSolidRegionOnImage(GetContour5(Mask),ImIn));
        else
            imshow(WindowName, ShowSolidRegionOnImage(Mask,ImIn));

    }
}
//---------------------------------------------------------------------------------------------
void ShowFitedRectangle(bool show, Mat ImIn, RotatedRect fittedRect, string WindowName)
{
    if(show)
    {
        Point2f vertices[4];
        Mat ImToShow;
        fittedRect.points(vertices);
        ImIn.copyTo(ImToShow);
        for (int i = 0; i < 4; i++)
            line(ImToShow, vertices[i], vertices[(i+1)%4], Scalar(0,255,0));
        imshow(WindowName, ImToShow);
    }
}
#endif
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//                            Process Functions
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
cv::Mat FindMaskFromGray(cv::Mat ImIn,int thesholdVal)
{
    Mat ImGray;
    Mat ImMask;

//pms
//    if(ImIn.empty())
//        return ImMask;
    cvtColor(ImIn,ImGray,CV_BGR2GRAY);

    threshold(ImGray, ImMask, thesholdVal ,1,THRESH_BINARY);
    ImMask.convertTo(ImMask, CV_16U);
    ImGray.release();
    return ImMask;
}
//--------------------------------------------------------------------------------------------------
void MaskOutsideEllipseMK(cv::Mat Mask)
{
    int maxX = Mask.cols;
    int maxY = Mask.rows;
    if(!maxX || !maxY)
        return;
    if(!Mask.isContinuous())
        return;

    int centerX = maxX/2;
    int centerY = maxY/2;


    unsigned short *wMask = (unsigned short *)Mask.data;
    float denominatorX = (float)(centerX * centerX)*(float).8;
    float denominatorY = (float)(centerY * centerY)*(float).9;
    for (int y = 0;  y < maxY; y++)
    {
        for (int x = 0;  x < maxX; x++)
        {
            float ellipse = (float)((x - centerX) * (x - centerX)) / denominatorX
                          + (float)((y - centerY) * (y - centerY)) / denominatorY;;
            if(ellipse > 1)
                *wMask = 0;
            wMask++;
        }
    }
}
//--------------------------------------------------------------------------------------------------
void MaskOutsideEllipseCV(cv::Mat Mask)
{
    int maxX = Mask.cols;
    int maxY = Mask.rows;
    int centerX = maxX/2;
    int centerY = maxY/2;
    if(!maxX || !maxY)
        return;
    Mat Ellipse = Mat::zeros(maxY,maxX,CV_16U);
    ellipse(Ellipse,Point(centerX,centerY),Size(centerX*8/10,centerY*9/10),0.0, 0.0,360.0,1,-1);
    Mask = Mask.mul(Ellipse);
    Ellipse.release();
}
//--------------------------------------------------------------------------------------------------
void MorphologyOnOneMasksMK(cv::Mat Mask, int erosion1, int dilation2, int erosion3)
{
    switch (erosion1)
    {
    case 1:
        RegionErosion5(Mask);
        break;
    case 2:
        RegionErosion9(Mask);
        break;
    case 3:
        RegionErosion13(Mask);
        break;
    default:
        break;
    }

    switch (dilation2)
    {
    case 1:
        RegionDilation5(Mask);
        break;
    case 2:
        RegionDilation9(Mask);
        break;
    case 3:
        RegionDilation13(Mask);
        break;
    default:
        break;
    }

    switch (erosion3)
    {
    case 1:
        RegionErosion5(Mask);
        break;
    case 2:
        RegionErosion9(Mask);
        break;
    case 3:
        RegionErosion13(Mask);
        break;
    default:
        break;
    }
}
//--------------------------------------------------------------------------------------------------
Mat CreateKernelForMorphology(int kernelShape)
{
    Mat Kernel;

    switch(kernelShape)
    {
    case 4:
       Kernel = (cv::Mat_<uchar>(5,5) << 0,1,1,1,0,
                                         1,1,1,1,1,
                                         1,1,1,1,1,
                                         1,1,1,1,1,
                                         0,1,1,1,0);
       break;

    case 3:
       Kernel = (cv::Mat_<uchar>(5,5) << 0,0,1,0,0,
                                         0,1,1,1,0,
                                         1,1,1,1,1,
                                         0,1,1,1,0,
                                         0,0,1,0,0);
       break;
    case 2:
       Kernel = (cv::Mat_<uchar>(3,3) << 1,1,1,
                                         1,1,1,
                                         1,1,1);
       break;
    default:
       Kernel = (cv::Mat_<uchar>(3,3) << 0,1,0,
                                         1,1,1,
                                         0,1,0);
       break;
    }
    return Kernel;
}
//--------------------------------------------------------------------------------------------------
void MorphologyOnOneMaskCV(cv::Mat Mask, int erosion1, int dilation2, int erosion3)
{
    Mat Kernel;
    if(erosion1)
    {
        erode(Mask,Mask,CreateKernelForMorphology(erosion1));
    }
    if(dilation2)
    {
        dilate(Mask,Mask,CreateKernelForMorphology(dilation2));
    }
    if(erosion3)
    {
        erode(Mask,Mask,CreateKernelForMorphology(erosion3));
    }
    Kernel.release();
}
//--------------------------------------------------------------------------------------------------
void FillHolesOnOneMaskMK(cv::Mat Mask)
{
    FillBorderWithValue(Mask, 0xFFFF);
    OneRegionFill5Fast1(Mask,  0xFFFF);
    FillHoles(Mask);
    DeleteRegionFromImage(Mask, 0xFFFF);
}
//--------------------------------------------------------------------------------------------------
RotatedRect FitEllipseToReg(cv::Mat Mask)
{
    RotatedRect fittedRect;
    fittedRect.angle = 0.0;
    fittedRect.center = Point2f(float(Mask.cols/2),float(Mask.rows/2));
    fittedRect.size = Size2f(100.0,100.0);

    Mat MaskTemp;
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;
    Mat pointsF;

    Mask.convertTo(MaskTemp,CV_8U);
    findContours(MaskTemp,contours,hierarchy,CV_RETR_LIST,CHAIN_APPROX_NONE);
    if(contours.size())
    {
        unsigned int maxSize = 0;
        unsigned int maxContour = 0;
        for(unsigned int i = 0;i<contours.size();i++)
        {
            if(maxSize < contours[i].size())
            {
                maxSize = contours[i].size();
                maxContour = i;
            }
        }
        if(maxSize > 10)
        {
            Mat(contours[maxContour]).convertTo(pointsF, CV_32F);
            fittedRect = fitEllipse(pointsF);
        }
    }

    contours.clear();
    hierarchy.clear();

    MaskTemp.release();
    pointsF.release();

    return fittedRect;
}
//--------------------------------------------------------------------------------------------------
void RotateOneImage(Mat &ImIn, Mat &Mask, RotatedRect fittedRect)
{
    Mat RotationMatrix;

    RotationMatrix = getRotationMatrix2D(fittedRect.center,fittedRect.angle,1.0);

    Mat ImRotated, MaskRotated;

    warpAffine(ImIn,ImRotated,RotationMatrix,Size(ImIn.cols,ImIn.rows));
    warpAffine(Mask,MaskRotated,RotationMatrix,Size(Mask.cols,Mask.rows));

    ImIn.release();
    Mask.release();
    RotationMatrix.release();
    ImIn = ImRotated;
    Mask = MaskRotated;
}
//--------------------------------------------------------------------------------------------------
bool CroppOneImage(Mat &ImIn, Mat &Mask)
{
    unsigned short *wMask;
    bool minSizeReached = true;

    int croppXMin = Mask.cols;
    int croppXMax = 0;

    int croppYMin = Mask.rows;
    int croppYMax = 0;

    int maxX = Mask.cols;
    int maxY = Mask.rows;

    wMask = (unsigned short *)Mask.data;
    for(int y = 0; y < maxY; y++)
    {
        for(int x = 0; x < maxX; x++)
        {
            if(*wMask)
            {
                if(croppXMin > x)
                    croppXMin = x;
                if(croppXMax < x)
                    croppXMax = x;

                if(croppYMin > y)
                    croppYMin = y;
                if(croppYMax < y)
                    croppYMax = y;
            }
            wMask++;
        }
    }
    //Size croppedImageSize;
    int croppWidth = croppXMax - croppXMin + 1;
    int croppHeight = croppYMax - croppYMin + 1;
    int croppX = croppXMin;
    int croppY = croppYMin;

    if (croppHeight <= 200)
    {
        minSizeReached = false;
        croppHeight = 200;
    }
    if (croppWidth <= 50)
    {
        minSizeReached = false;
        croppWidth = 50;  //pms171017
    }
    if(croppX + croppWidth >= Mask.cols)
        croppX = Mask.cols - croppWidth - 1;
    if(croppY + croppHeight >= Mask.rows)
        croppY = Mask.rows - croppHeight - 1;

    Mat ImCropped, MaskCropped;


//PMS Jest cropY1 = -1 co powoduje wywalenie programu w nastepnej linii (np. dla obrazow 01288_?.png)
    if(croppY < 1 || croppX < 1)
        return false;

    ImIn(Rect(croppX,croppY,croppWidth,croppHeight)).copyTo(ImCropped);
    Mask(Rect(croppX,croppY,croppWidth,croppHeight)).copyTo(MaskCropped);

    ImIn.release();
    Mask.release();
    ImIn = ImCropped;
    Mask = MaskCropped;

    return true;
}
//--------------------------------------------------------------------------------------------------
//            segmentation procedure for one image
//--------------------------------------------------------------------------------------------------
#ifdef TERAZ_DEBUG
int SegmentOneGrainImg(cv::Mat *ImIn, cv::Mat *ImOut, cv::Mat *MaskOut, TransformacjaZiarna *transf, int camera, SegmentParams *params)
#else
int SegmentOneGrainImg(cv::Mat *ImIn, cv::Mat *ImOut, cv::Mat *MaskOut, TransformacjaZiarna *transf, int camera)
#endif
{
#ifdef TERAZ_DEBUG
    int params_threshVal = params->threshVal;
    bool params_removeSmallReg = params->removeSmallReg;
    int params_rawMorphErosion1 = params->rawMorphErosion1;
    int params_rawMorphDilation2 = params->rawMorphDilation2;
    bool params_fillHoles = params->fillHoles;
    bool params_divideSeparateReg = params->divideSeparateReg;
    int params_MinRegSize = params->MinRegSize;
    bool params_removeBorderRegion = params->removeBorderRegion;

    bool params_temp = params->temp;

    const bool params_showContour = params->showContour;
    const bool params_showInput = params->showInput;
    const bool params_showThesholded = params->showThesholded;
    const bool params_show1stMorphology = params->show1stMorphology;
    const bool params_showHolesRemoved = params->showHolesRemoved;
    const bool params_showMask = params->showMask;
    const bool params_showFitted = params->showFitted;
    const bool params_showRotated = params->showRotated;
    const bool params_showCropped = params->showCropped;

    string DispName;
    switch(camera)
    {
    case 0:
        DispName = "_T";
        break;
    case 1:
        DispName = "_B";
        break;
    default:
        DispName = "_?";;
    }

#else
    const int params_threshVal = 10;
    const bool params_removeSmallReg = false;
    const int params_rawMorphErosion1 = 3;
    const int params_rawMorphDilation2 = 0;
    const bool params_fillHoles = true;
    const bool params_divideSeparateReg = true;
    const int params_MinRegSize = 10000;
    const bool params_removeBorderRegion = false;

    bool params_temp = false;
#endif

    Mat Im;
    ImIn->copyTo(Im);
    if(!Im.rows || !Im.cols || !Im.isContinuous() || Im.empty())
        return 0;

#ifdef TERAZ_DEBUG
    ShowImage(params_showInput, Im, "Input image" + DispName);
#endif

    Mat Mask;

    bool validBackground;
    // thresholding
    Mat ImToTheshold;
    if(segmentBackgroundImages[camera]->empty())
    {
        ImToTheshold = Im;
        validBackground = false;
    }
    else
    {
        ImToTheshold = Im - *segmentBackgroundImages[camera];
        validBackground = true;
    }

    Mask = FindMaskFromGray(ImToTheshold,params_threshVal);

    ImToTheshold.release();

    if(!Mask.cols || !Mask.rows || !Mask.isContinuous() || Mask.empty())
        return -1;

    if(!validBackground)
        MaskOutsideEllipseMK(Mask);   //MK version faster than CV

#ifdef TERAZ_DEBUG
    ShowImageAndMask(params_showThesholded, params_showContour, Im, Mask, "Thresholded" + DispName);
#endif

    // remove regions of size 1 pix
    if(params_removeSmallReg && !params_rawMorphErosion1)
        Mask = RemovingTinyReg9(Mask);
// !!!!!!!!!!!!!!!!!!!Under Test!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // morphology on thresholded image
    if(params_temp)
        MorphologyOnOneMasksMK(Mask, params_rawMorphErosion1, params_rawMorphDilation2, 0);
    else
        MorphologyOnOneMaskCV(Mask, params_rawMorphErosion1, params_rawMorphDilation2, 0);

#ifdef TERAZ_DEBUG
    ShowImageAndMask(params_show1stMorphology, params_showContour, Im, Mask, "Morphology1" + DispName);
#endif

    // procedure for removing holes inside object, obligatory for gradient thesholded image, usefull in other case
    if(params_fillHoles)
       FillHolesOnOneMaskMK(Mask);

#ifdef TERAZ_DEBUG
    ShowImageAndMask(params_showHolesRemoved, params_showContour, Im, Mask, "Without Holes" + DispName);
#endif

    // labelling
    if(params_divideSeparateReg)
        if(DivideSeparateRegions(Mask, params_MinRegSize) <= 0)
            return -2;

    // removal of regions touchig image border
    if(params_removeBorderRegion && validBackground)
        DeleteRegTouchingBorder(Mask);

#ifdef TERAZ_DEBUG
    ShowImageAndMask(params_showMask, params_showContour, Im, Mask, "Mask" + DispName);
#endif
     //fit ellipse
    RotatedRect fittedRect;
    fittedRect.angle = 0.0;
    fittedRect.center = Point2f((float)(Im.cols/2),(float)(Im.rows/2));
    fittedRect.size = Size2f(100.0,100.0);

    fittedRect = FitEllipseToReg(Mask);

    transf->x = fittedRect.center.x;
    transf->y = fittedRect.center.y;

    if(camera == 0)
    {
        transf->angle = fittedRect.angle;
    }
    else
    {
        fittedRect.angle = -transf->angle;
        transf->angle = -transf->angle;
    }

#ifdef TERAZ_DEBUG
    ShowFitedRectangle(params_showFitted, Im, fittedRect,"Fitted" + DispName);
#endif

    RotateOneImage(Im, Mask, fittedRect);

#ifdef TERAZ_DEBUG
    ShowImageAndMask(params_showRotated, params_showContour, Im, Mask, "Rotated" + DispName);
#endif

    if(!CroppOneImage(Im, Mask))
        return -3;

#ifdef TERAZ_DEBUG
    ShowImageAndMask(params_showCropped, params_showContour, Im, Mask, "Cropped" + DispName);
#endif
    *ImOut = Im;
    *MaskOut = Mask;
    return 1;
}
//--------------------------------------------------------------------------------------------------
void MeasureAreaForGermIdentyfication(Mat &Mask, int *topCount, int *bottomCount)
{
    int maxX = Mask.cols;
    int maxY = Mask.rows;

    int maxPosY = maxY-1;
    int minPosY = 0;

    int linesToCount = 200;
    int pixelCount = linesToCount * maxX;

    int uStartLine = minPosY;

    int uStopLine =  uStartLine + linesToCount;

    if(uStopLine >= maxY)
        uStopLine = maxY - 1;

    int lStopLine  =  maxPosY;
    int lStartLine = lStopLine - linesToCount;
    if(lStartLine < 0)
        lStartLine = 0;

    unsigned short *wMask;

    wMask = (unsigned short*)Mask.data + maxX * uStartLine;//(imCenterY - linesToCount - offsetToCount);
    int uRegPixelCount = 0;
    for(int i = 0; i < pixelCount; i++)
    {
        if(*wMask)
            uRegPixelCount++;
        wMask++;
    }

    wMask = (unsigned short*)Mask.data + maxX * lStartLine;//(imCenterY + offsetToCount);
    int lRegPixelCount = 0;
    for(int i = 0; i< pixelCount; i++)
    {
        if(*wMask)
            lRegPixelCount++;
        wMask++;
    }
    *topCount = uRegPixelCount;
    *bottomCount = lRegPixelCount;
}
//---------------------------------------------------------------------------------------------
float AverageMaskedPixelsF(Mat Reg, Mat ImF)
{
    int maxX = ImF.cols;
    int maxY = ImF.rows;
    int maxXY = maxX * maxY;
    if(!ImF.isContinuous())
        return -1;

    unsigned short *wReg = (unsigned short *)Reg.data;
    float *wImF = (float *)ImF.data;

    //deph


    float sumIm = 0;
    float sumReg = 0;
    for (int i = 0; i < maxXY; i++)
    {
            if(*wReg)
            {
                sumIm  += *wImF;
                sumReg += 1.0;
            }
            wReg++;
            wImF++;
    }
    if(!sumReg)
        sumReg = 1;

    return sumIm/sumReg;
}
//--------------------------------------------------------------------------------------------------
float MeasureSumOfGradientsForValeyIdentyfication(Mat ImIn, int ellipseSizeX, int ellipseSizeY)
{
    Mat Mask = Mat::zeros(ImIn.rows ,ImIn.cols ,CV_16U);

    ellipse(Mask, Point(Mask.cols/2,Mask.rows/2),Size(ellipseSizeX,ellipseSizeY),0,0,360,1,-1);

    // find valey(znajdź bruzdke)
    Mat ImGray;
    cvtColor(ImIn,ImGray,CV_BGR2GRAY);

    Mat ImGradient = HorizontalGradientDown(ImGray);

#ifdef TERAZ_DEBUG
    //ShowImageAndMask(1, 1, ShowImageF32PseudoColor(ImGradient, 0.0, 100.0), Mask, "Gradient");
#endif
    float outputVal = AverageMaskedPixelsF(Mask, ImGradient);
    ImGradient.release();
    ImGray.release();
    Mask.release();

    return outputVal;

}

//--------------------------------------------------------------------------------------------------
MR2DType* PrepareRoiOut(Mat Mask, int color, std::string Name)
{
    // output ROI
        int begin[MR2DType::Dimensions];
        int end[MR2DType::Dimensions];

        //Mask 1
        begin[0] = 0;
        begin[1] = 0;
        end[0] = Mask.cols-1;
        end[1] = Mask.rows-1;

        //MR2DType* roi;
        MR2DType* Roi = new MR2DType(begin, end);
        MazdaRoiIterator<MR2DType> iterator(Roi);

        unsigned short *wMask = (unsigned short*)Mask.data;
        while(! iterator.IsBehind())
        {
            if (*wMask)
                iterator.SetPixel();
            ++iterator;
           wMask++;
        }

        Roi->SetName(Name);
        Roi->SetColor(color);
        return Roi;
}

//--------------------------------------------------------------------------------------------------
//                                    function visible from outside
//--------------------------------------------------------------------------------------------------
#ifdef TERAZ_DEBUG
bool SegmentGrainImg(const std::vector<cv::Mat*> *ImInVect, std::vector<cv::Mat*> *ImOutVect, std::vector <MR2DType*> * OutRoiVect,
                     std::vector<TransformacjaZiarna> *transfVect, int* orientation, SegmentParams *params)
#else
bool SegmentGrainImg(const std::vector<cv::Mat*> *ImInVect, std::vector<cv::Mat*> *ImOutVect, std::vector <MR2DType*> * OutRoiVect,
                     std::vector<TransformacjaZiarna> *transfVect, int* orientation)
#endif
{
    Mat *ImIn1 = ImInVect->at(0);
    Mat *ImIn2 = ImInVect->at(1);

    Mat *ImOut1 = new Mat;
    Mat *Mask1 = new Mat;
    Mat *ImOut2 = new Mat;
    Mat *Mask2 = new Mat;
    TransformacjaZiarna transf1, transf2;

    int exitCode1,exitCode2;
#ifdef TERAZ_DEBUG
    exitCode1 = SegmentOneGrainImg(ImIn1, ImOut1, Mask1, &transf1, 0, params);
    transf2 = transf1;
    exitCode2 = SegmentOneGrainImg(ImIn2, ImOut2, Mask2, &transf2, 1, params);
#else
    exitCode1 = SegmentOneGrainImg(ImIn1, ImOut1, Mask1, &transf1, 0);
    transf2 = transf1;
    exitCode2 = SegmentOneGrainImg(ImIn2, ImOut2, Mask2, &transf2, 1);
#endif

    if(exitCode1 < 1 || exitCode2 < 1)
        return 0;

    // germ brush identyfication
    int topCount1,bottomCount1,topCount2,bottomCount2;
    MeasureAreaForGermIdentyfication(*Mask1, &topCount1, &bottomCount1);
    MeasureAreaForGermIdentyfication(*Mask2, &topCount2, &bottomCount2);
    bool flipped = false;
    if(topCount1 + topCount2 >= bottomCount1 + bottomCount2)
    {
        flip(*ImOut1,*ImOut1, -1);
        flip(*ImOut2,*ImOut2, -1);
        flip(*Mask1,*Mask1, -1);
        flip(*Mask2,*Mask2, -1);
        flipped = true;
    }

#ifdef TERAZ_DEBUG
    ShowImageAndMask(params->showAligned, params->showContour, *ImOut1, *Mask1, "Aligned_T");
    ShowImageAndMask(params->showAligned, params->showContour, *ImOut2, *Mask2, "Aligned_B");
#endif

    //find valey
    int ellipseSizeX = Mask1->cols/4;
    int ellipseSizeY = Mask1->rows/4;

    if(ellipseSizeX > Mask2->cols/4)
        ellipseSizeX = Mask2->cols/4;
    if(ellipseSizeY > Mask2->rows/4)
        ellipseSizeY = Mask2->rows/4;

    bool switchImages;

    float sum1 = MeasureSumOfGradientsForValeyIdentyfication(*ImOut1, ellipseSizeX, ellipseSizeY);
    float sum2 = MeasureSumOfGradientsForValeyIdentyfication(*ImOut2, ellipseSizeX, ellipseSizeY);

    if(sum1 < sum2)
    {
        *Mask1 *= 2;
        switchImages = 1;
    }
    else
    {
        *Mask2 *= 2;
        switchImages = 0;
    }
#ifdef TERAZ_DEBUG
    ShowImageAndMask(params->showOutput, params->showContour, *ImOut1, *Mask1, "Out_T");
    ShowImageAndMask(params->showOutput, params->showContour, *ImOut2, *Mask2, "Out_B");
#endif

    // Prepare outputoutput
    int color1,color2;
    if(switchImages)
    {
        *orientation = 1;
        color1 = 0x0000ff00;
        color2 = 0x00ff0000;
    }
    else
    {
        *orientation = 0;
        color1 = 0x00ff0000;
        color2 = 0x0000ff00;
    }

    MR2DType *Roi1 = PrepareRoiOut(*Mask1, color1, "A");;
    MR2DType *Roi2 = PrepareRoiOut(*Mask2, color2, "B");

    Mask1->release();
    Mask2->release();

    transf1.x -= ImIn1->cols/2.0;
    transf1.y -= ImIn1->rows/2.0;
    transf2.x -= ImIn2->cols/2.0;
    transf2.y -= ImIn2->rows/2.0;

    if(flipped)
    {
        transf1.angle +=  180.0;
        transf2.angle +=  180.0;
    }

    ImOutVect->push_back(ImOut1);
    ImOutVect->push_back(ImOut2);
    OutRoiVect->push_back(Roi1);
    OutRoiVect->push_back(Roi2);
    transfVect->push_back(transf1);
    transfVect->push_back(transf2);
    return 1;
}
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
